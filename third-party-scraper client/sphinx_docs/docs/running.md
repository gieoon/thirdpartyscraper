# Running GroupChat

Github Repository ->

## Client

* `npm start` - Start the create-react-app project on a local server

## Server

* `cd server && node server.js` - Start the NodeJS server locally

## DOCS

* `mkdocs serve` - This will start the docs (May take some time to load)
