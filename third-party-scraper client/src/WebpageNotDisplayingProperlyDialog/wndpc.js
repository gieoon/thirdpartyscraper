
import React from 'react';
import * as DB from '../API/DB.js';
import './wndpc.css';

class WNDPC extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			showing: 3
		}
	}

	showModal = () => {
		console.log("Modal pressed");
		this.setState({
			showing: 1
		});
	}

	wndpcDone = () => {
		console.log('wndpc done');
		this.setState({
			showing: 2,
		});
		var wndpcURL = document.getElementById('wndpc-website-input').value;
		var wndpcIssue = document.getElementById('wndpc-specific-issue-input').value;
		DB.saveWNDPCToDB('guest', wndpcURL, wndpcIssue);
	}

	closeModal = () => {
		this.setState({
			showing: 3
		})
	}

	render(){
		return(
			<div className="g-relative">
				{
					this.state.showing === 1 ? 
						<div className="wndpc-main">
							<div className="wndpc-content" >
								<span className="wndpc-close" onClick={()=>this.closeModal()}>&times;</span>
								{/*<p>Website</p>*/} 
								<span>What's the URL that's giving you issues</span>
								<input id="wndpc-website-input" placeholder="https://www." />
								<span>What's the specific issue? </span>
								<br/>
								<span><i>e.g. Can't click a button, 'Apply' button not displaying properly</i></span>
								<input id="wndpc-specific-issue-input" />
								<button onClick={() => this.wndpcDone(this)}>Done</button>
							</div>
						</div>	
					:
						
							this.state.showing === 2 ?
								<div className="wndpc-thanks" onClick={()=>this.closeModal()}>
									<div className="wndpc-content">
										<span className="wndpc-close" onClick={()=>this.closeModal()}>&times;</span>
										<span>Thank you for your feedback, we'll get back to you as soon as we can</span>
									</div>
								</div>
							:
							<div className="wndpc-container" onClick={() => this.showModal()}>
								<span>page not displaying properly?</span>
							</div>
						
						
				}
			</div>
		);
	}
}

export default WNDPC;
