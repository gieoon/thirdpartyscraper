const CONSTANTS = require('../Constants');

const helpers = {
	//user submitted url for search
	/*searchUrl: async (url, signal) => {
		var urls = [url];
		var response = {};
		await APIRequest(urls , 'searchUrl', signal)
			.then(res => {
				response = res;
				return res;
			})
			.catch(err => console.log(err));
		return response;
	},*/
	//to terminate ongoing asynchronous requests
	getController: () => { 
		return new AbortController(); 
	},

	actionCSSSelector: async (actionType, cssSelector, signal, instanceId, optionalText, fieldName, fieldType) => {
		var response = {};
		await APIActionRequest([cssSelector], actionType, signal, instanceId, optionalText, fieldName, fieldType)
			.then(res => {
				console.log("res: ", res);
				response = res;
			})
			.catch(err => console.log(err));
		return response;
	},

	//http://localhost:5000/api/clickUrl/undefined/|tag:input|arialabel:Log In|id:u_0_2|text:
	action: async (actionType, selector, signal, instanceId) => {
		var data = [];

		data.push('tag:' + selector.tagName);
		var text = getElementDirectInnerHTML(selector);
		console.log('text: ', text);
		if(text.length > 0 && actionType !== CONSTANTS.ACTION_TYPE_GET) data.push('text:' + text);//selector.firstChild.nodeValue//.trim()//textContent//innerText//innerHTML 
		var attributes = selector.attributes;
		console.log('attributes: ', attributes);
		for (var i = 0; i < attributes.length; i++){
			data.push(attributes[i].name + ':' + attributes[i].value);
		};

		var response = {};
		await APIActionRequest(data, actionType, signal, instanceId)
			.then(res => {
				console.log('response: ', res);
				response = res;
				return res;
			})
			.catch(err => console.log(err));
		return response;
	},

	// getData: async (selector, signal, instanceId) => {
	// 	var data = [];
	// 	var text = getElementDirectInnerHTML(selector);
	// 	console.log('text: ', text);
	// 	if(text.length > 0) data.push('text:' + text);
	// 	var attributes = selector.attributes;
	// 	console.log('attributes: ', attributes);
	// 	for(var i = 0; i < attributes.length; i++){
	// 		data.push(attributes[i].name + ':' + attributes[i].value);
	// 	};

	// 	var response = {};
	// 	await APIActionRequest
	// },
	receiveSelectorResponse: (response) => {
		var tempQuery = {};
		tempQuery.url = response.url;
		tempQuery.step = response.step;
		tempQuery.title = response.title;
		//tempQuery.screenshotData = response.screenshot.data;
		tempQuery.tags = response.tags;
		tempQuery.allHTML = response.allHTML; 
		tempQuery.sanitizedContent = response.sanitizedContent;
		return tempQuery;
	},
	receiveSelectorGetDataResponse: (response) => {
		var tempQuery = {};
		tempQuery.step = response.step;
		tempQuery.text = response.results.innerData;
		//tempQuery.tags = response.results.tag;
		return tempQuery;
	},
	directRender: async (url, instanceId, signal) => {
		var urls = [url];
		var response = {};
		await APIRequest( urls, 'directRender', instanceId, signal)
			.then(res => {
				response = res;
				return res;
			})
			.catch(err => console.log(err));
		return response;
	}
}

//gets this element's innerhtml ignoring child component's innerHTML
export function getElementDirectInnerHTML(selector){
	var child = selector.firstChild,
		texts = [];
	while(child){
		if(child.nodeType === 3){ // 3 = Node.TEXT_TYPE
			texts.push(child.data);
		}
		child = child.nextSibling;
	}
	var text = texts.join("");
	text = text.replace(/\r?\n|\r/g, '');
	text = text.trim();
	return text;
}  

export default helpers;

//generic GET API Request
export async function APIRequest(dataArray, endpoint, instanceId, signal){
	var params = processParams(dataArray);

	var url = global.SERVER_PORT + '/api/' + endpoint + '/' + instanceId + '/' + params; 
	console.log('sending url: ', url);
	
	const response = await fetch(url, { mode: "cors", signal: signal });//.then(response => {console.log('response.json(): ', response.json())});
	const body = await response.json();
	if(response.status !== 200) {
		throw Error(body.message);
	}
	return body;
}

export async function APIActionRequest(actionType, endpoint, signal, instanceId, optionalText, fieldName, fieldType){
	var params = processParams(actionType);
	endpoint = endpoint.replace(/[ ]/g, '');
	endpoint = endpoint.toLowerCase();
	var additionalParams = '';
	if(endpoint === CONSTANTS.URL_ACTION_TYPE_TYPE){
		additionalParams = '/' + optionalText;
	} 
	else if(endpoint === CONSTANTS.URL_ACTION_TYPE_GET){
		additionalParams = '/' + (fieldName || 'New Field to Read') + '/' + fieldType; 
	} 
	else if(endpoint === CONSTANTS.URL_ACTION_TYPE_NEXT_URL){
		additionalParams = processParams([fieldName]);
	}
	console.log("endpoint: ", endpoint);
	console.log("added fieldName: ", fieldName);
	console.log("added fieldType: ", fieldType);
	console.log("additional params: ", additionalParams);
	
	var url = global.SERVER_PORT + '/api/' + endpoint + '/' + instanceId + '/' + params + additionalParams; 
	console.log('sending url: ', url);
	const response = await fetch(url, { mode: 'cors', signal: signal});
	const body = await response.json();
	if(response.status !== 200) {
		//re-excute with https://
		throw Error(body.message);
	}
	return body;
}

//encodes strings on URLS to be passed properly.
function processParams(array){
	var params = '';
	//console.log('array received: ', array);
	array.forEach((param, index) => {
		param = param.replace(/\//gi, '{}');
		param = param.replace(/\?/g, '[]');
		param = param.replace(/#/g,'<>');
		param = param.replace(/\./g,'>>');
		param = param.replace(/\\/g,'<<');
		//get rid of hashes, not compatible with URL's and don't want to query off thema s they point to other elments in the DOM that are not helpful for selections
		//get rid of style properties, no point searching on them
		var match = param.match(/style:/g);
		//console.log('match: ', match);
		if(match === null){
			if(index > 0) params += '|' + param;
			else params += param;
		}
	});
	return params;
}


/*
		// Object.keys(selector).map(obj => {
		// 	data.push(obj + ':' + selector[obj]);
		// });

		//console.log('dom object: ', selector.innerHTML);
		//console.log('dom object: ', selector.tagName);
		//console.log('dom object: ', selector.attributes.length);
		//console.log('dom object: ', selector.attributes[0].name);
		//console.log('dom object: ', selector.attributes[0].value);
		//always include these two


return fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json",
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
    .then(response => response.json()); // parses response to JSON
*/