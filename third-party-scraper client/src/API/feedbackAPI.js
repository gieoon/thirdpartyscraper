
//generic GET API Request
export async function APIRequest(name, email, msg){
	var url = 'http://' + global.REACT_SERVER_PORT + '/api/mailfeedback/' + name + '/' + email + '/' + msg; 
	console.log('sending url: ', url);
	
	const response = await fetch(url, { mode: "cors" });//.then(response => {console.log('response.json(): ', response.json())});
	const body = await response.json();
	if(response.status !== 200) {
		throw Error(body.message);
	}
	console.log('body: ', body);
	return body;
}