//links to firestore DB
import firebase from 'firebase';
import * as CONSTANTS from '../Constants';

var db = {};

export function initDB(){
	// Initialize Firebase
	console.log("initializing DB");
 	var config = {
		apiKey: "AIzaSyAULd3RszujB2MJT4M2Zmc4hG8zD7VFn2I",
    	authDomain: "thirdpartyscraper.firebaseapp.com",
    	databaseURL: "https://thirdpartyscraper.firebaseio.com",
    	projectId: "thirdpartyscraper",
    	storageBucket: "thirdpartyscraper.appspot.com",
    	messagingSenderId: "848404238479"
  	};
  	if(!firebase.apps.length){
  		firebase.initializeApp(config);
  	}
  	
  	db = firebase.firestore();
	//db.settings({ timestampsInSnapshots: true });

}

export function listenToUsersQueries(userId, caller, callerComponent){
	//set a listener for this directory
	db.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.QUERY_COLLECTION)
		.onSnapshot(function(snapshot){
			snapshot.docChanges().forEach(async function(change){
				if(change.type === 'added' || change.type === 'modified' || change.type === 'removed'){		
					//console.log("old caller: ", caller);
					caller = await getQueriesByUser(userId);
					//console.log("new caller: ", caller);
					callerComponent.setState({
						queries: caller
					});
					callerComponent.forceUpdate();
				}
			})
		});
}

export function getQueriesByUser(userId){
	const promises = [];

	let queriesPromise = db
		   .collection(CONSTANTS.USER_COLLECTION)
		   .doc(userId.toString())
		   .collection(CONSTANTS.QUERY_COLLECTION)
		   .get();
   	return queriesPromise.then(function (queries){
   		//let queryNamePromise = queries.query;
   		//console.log('queries: ', queries.query);
   		// promises.push(
   		// 	queryNamePromise.then(function(queryName){
   		// 		console.log("queryNamePromise: ", queryName);
   		// 	})
   		// );
		
		
		// console.log("query.docs[0]: ", queries.docs);
		//var index = 0;
		//const queryName = queries.docs[index].data().name;
		//console.log('queryName: ', queryName);
   		queries.forEach(function(query){
   			//console.log('query: ', query.get('name'));
   			let deletedFlag = query.get('DELETED_FLAG');
   			let queryName = query.get('name');
   			if(queryName === undefined) queryName = 'New Query Title';
   			let queryDate = query.get('createdOn');
   			let stepsPromise = query.ref.collection('Steps').get();
   			var steps = {};
   			//++index;
   			promises.push(
	   			stepsPromise.then(function(stepsSnapshot){
	   				stepsSnapshot.forEach(function(step){
	   					steps[step.id] = step.data();
	   				});
	   				return {steps: steps, queryId: query.id, name: queryName, createdOn: queryDate, deleted: deletedFlag};
	   			})
   			);
   		});
   		return Promise.all(promises);
   	});
}

export async function saveNewQueryName(userId, queryId, newQueryName){
	console.log('db: ', db);
	const promises = [];
	let queryPromise = db.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.QUERY_COLLECTION)
		.doc(queryId.toString()).update({
			name: newQueryName
		});

	promises.push(queryPromise.then(function(){
			console.log("Saved new query name successfully");
			return 1;
		}).catch(function(error){
			console.log("Error setting query name: ", error);
			return 0;
		}));
	return Promise.all(promises);
}

export async function saveNewQueryDetails(userId, queryId, obj){
	console.log('db: ', db);
	const promises = [];
	let queryPromise = db.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.QUERY_COLLECTION)
		.doc(queryId.toString()).update({
			// name: obj.name,
			// createdOn: obj.createdOn,
			frequency: obj.frequency,
			dayOfWeek: obj.dayOfWeek,
			day: obj.day,
			hour: obj.hour,
			minute: obj.minute
		});
	promises.push(queryPromise.then(function(){
		console.log("saved query frequency successfully");
		return 1;
	}).catch(function(error){
		console.log("error setting query frequency: ", error);
		return 0;
	}));
	return Promise.all(promises);
}

export async function deleteQuery(userId, queryId){
	console.log("deleting: ", queryId);
	db.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.QUERY_COLLECTION)
		.doc(queryId.toString()).delete().then(function(){
			console.log("Document deleted successfully");
			return 'done';
		}).catch(function(error) {
			console.log("Error removing document: ", error);
		});
}

//retrieve the component name from Id, and return to component
export async function getQueryNamesForUser(userId, component){
	console.log('getting query name');
	const promises = [];
	let queryPromise = db
		.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.QUERY_COLLECTION)
		.get();

	promises.push(queryPromise.then(function(data){
		component.setState({
			queryName: data
		});
		console.log('got name: ', data);
		return data;
	}));

	Promise.all(promises);
}

//one parent snapshot listener to listen to all subclass and nested queries
export function dataPageListenToDataCollectedChanges(userId, component){
	console.log("initializing query listener for: ", userId);

	//console.log("returning to component: ", component);
	const promises = []; 
	//let dbQuery = 
	const unsubscribe = db.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.DATA_COLLECTION)
		.onSnapshot(function(dataQuerySnapshot){
			var dataQueries = [];
			dataQuerySnapshot.forEach(function(doc){
				dataQueries.push({queryId: doc.id, data: doc.data()});

			});
			dataQuerySnapshot.docChanges().forEach(function(change){
				if(change.type === 'added'){
					//console.log("new data added||initialized: ", change.doc.data());
					// component.setState({
					// 	data: change.doc.data()
					// })
				}
				if(change.type === 'modified'){
					console.log("Modified city: ", change.doc.data());
				}
				if(change.type === 'removed'){
					console.log("removed city: ", change.doc.data());
				}
			});
			//console.log("Current queries: ", dataQueries);
			component.setState({
				data: dataQueries,
				selectedQuery: dataQueries[0]
			});
			// global.stepArea.current.dataPageStepAreaRef.current.setState({
			// 	data: dataQueries,
			// 	selectedQuery: dataQueries[0] //default selected query to the first one
			// });
			//console.log('dataqueries: ', dataQueries);
			global.dataPage.current.setState({
				query: dataQueries[0],
			}, () => {
				//global.dataPage.current.processData();
				//console.log("setting datapage query: ", dataQueries[0]);
			});
		});
	//console.log("Setting Datapage Unsubscriber!!: ", unsubscribe);
	return unsubscribe;
}

export async function saveFeedbackToDB(userId, feedback, component){
	console.log("Saving feedback...");
	const promises = [];
	promises.push(db.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.FEEDBACK_COLLECTION)
		.doc(Date.now().toString())
		.set({		
			name: feedback.name,
			email: feedback.email,
			message: feedback.message
		})
		.then(function(){
			console.log("feedback saved successfully");
			component.clearAll();
		})
		.catch(function(err){
			console.log("err caught writing feedback to DB: ", err);
		})
	);
}

export async function saveWNDPCToDB(userId, websiteUrl, issueMsg){
	console.log("Saving WNDPC to DB");
	const promises = [];
	promises.push(db.collection(CONSTANTS.USER_COLLECTION)
		.doc(userId.toString())
		.collection(CONSTANTS.WNDPC_COLLECTION)
		.doc(Date.now().toString())
		.set({
			issueUrl: websiteUrl,
			issueMsg: issueMsg 
		})
		.then(function(){
			console.log("WNDPC Saved successfully");
		})
		.catch(function(err){
			console.log("err caught writing WNDPC to DB: ", err);
		})
	);
}

/*
//.get();

	// promises.push(dbQuery.then(function(querySnapshot){
	// 	querySnapshot.forEach(function(doc){
	// 		console.log('doc.id: ' + doc.id + 'doc.data: ', doc.data());
	// 		doc.ref.get().then(function(timestampSnapshot){
	// 			console.log("timestampSnapshot: ", timestampSnapshot.data());
	// 			timestampSnapshot.ref.forEach(function(dataStepNo){
	// 				console.log("dataStepNo: ", dataStepNo);
	// 				console.log("datasetpno: " +  dataStepNo.id + ' data: ' + dataStepNo.data());
	// 			})
	// 		})
	// 	});
	// }));
*/

/*
INSTEAD OF USING FOREACH, USE A FOR LOOP
const docSnapshots = querysnapshot.docs;

for (var i in docSnapshots) {
    const doc = docSnapshots[i].data();

    // Check for your document data here and break when you find it
}
Or if you don't actually need the full QuerySnapshot, you can apply the filter using the where function before calling get on the query object:

const dataKey_1 = "dataKey_1";    
const initialQuery = ref_serial_setting;
const filteredQuery = initialQuery.where('data_name', '==', dataKey_1);

filteredQuery.get()
    .then(querySnapshot => {
        // If your data is unique in that document collection, you should
        // get a query snapshot containing only 1 document snapshot here
    })

    .catch(error => {
        // Catch errors
    });

*/




/*
.get().then(
						   	snapshot => {console.log('snapshot.id: ', snapshot)})
		   console.log('userqueriesref: ', userQueriesRef.id + '=>' );
*/
/*
export async function getQueriesByUser(userId){
	var queries = {};
	//console.log('db: ', db);
	let queriesRef = await db.collection(CONSTANTS.USER_COLLECTION).doc(userId.toString()).collection(CONSTANTS.QUERY_COLLECTION).get()
	//console.log('queriesRef: ', queriesRef);
							
						   //.onSnapshot(function(snapshot) {
						   .then(async function(snapshot) {
						   		var outer = 0, inner = 0;
						    	snapshot.forEach(async function(query) {
						    		//console.dir(query.data().debug);
						    		++outer;
					   				await query.ref
					   					.collection('Steps')
					   					.get()
							   			.then(function(stepSnapshot) {
							   				var steps = {};
							   				stepSnapshot.forEach(async function(step){
							   					await console.log(step.data());
							   					steps[step.id] = await step.data();
							   				}, console.log('starting inner'));
							   				//map queryId to steps
							   				queries[query.id] = steps;
							   				global.queries = queries;

							   			}).catch(err => {
							   				console.log('ERROR GETTING STEP FROM QUERY: ', err);
							   			}).finally(() => {
							   				console.log('done');
							   				++inner;
							   				// if(inner == stepSnapshot.size && outer == snapshot.size){
							   				// 	global.queries = queries;
							   				// }
							   			});
							   			
						   		}, console.log('starting outer'));
						   		//console.log('snapshot.size: ', snapshot.size);
						   		async function endReturn(){
						   			console.log("returning queries: ", queries);
						   			//return queries;
						   		}
						   		//await callback();
						   		console.log('length1: ', Object.keys(queries).length);
						   		await console.log("reutrning: ", queries);
						   		console.log('length2: ', Object.keys(queries).length);
						   		return await queries;
						   })
						   .catch(err => {
						   		console.log('ERROR GETTING QUERIES FROM USER: ', err);
						   })
						   .finally(() => {
						   			console.log('finally done');
						   			
						   		});
*/


		   		//transferObj.outerCounter = 0;
		   		//transferObj.outer = snapshot.size;
		   		
		   		//console.log('starting loop: ', snapshot.size);
		   //  	snapshot.forEach((query) => {
		   //  		//console.log('incrementing');
		    		
		   // 			transferObj.outerCounter++;
		   // 			transferObj.iterations++;
	   	// 		   	promises.push(query.ref
	   	// 				.collection('Steps')
	   	// 				.get());
	   	// 				//.onSnapshot(stepSnapshot => {
	   	// 					//console.log('stepSnapshot: ', stepSnapshot.size);
	   	// 					transferObj.innerSizes += stepSnapshot.size;
			  //  				var steps = {};
			  //  				//let promise = stepSnapshot.docs.map(step => {
			  //  				stepSnapshot.forEach(step => {
	   	// 						transferObj.iterations++;
			  //  					console.log(step.data());
			  //  					//steps[step.id] = step.data();
		   // 						promises.push(step.data());
			  //  				});
			  //  				promises.push(promise);
			  //  				//map queryId to steps
			  //  				queries[query.id] = steps;
			  //  				//console.log('returning');
			  //  				//return queries;
			  //  				//checkReturn();
			  //  				//return Promise.all(promises);

			  //  			});
		   // 		});
		   		
		   // 		//return queries;
		   	
		   // 		console.log('length2: ', Object.keys(queries).length);
		   // });

	// Promise.all(promises).then(results => {
	// 	console.log('really done');
	// })

	// function checkReturn(){
	// 	console.log('transferObj: ', transferObj);
	// 	if(transferObj.outerCounter == transferObj.outer 
	// 		&& transferObj.iterations == transferObj.outer + transferObj.innerSizes){
	// 		console.log('returning now!!: ');
	// 	}
		
	// }			   
					
	//console.log('done: ');
	//return queries;
 //   	async function callback(){
 //   		//window.setTimeout(() =>{
 //   			console.log('returning queries:: ', queries);
 //   			global.queries = queries;
 //   			//return await queries;
 //   		//}, 1000);
 //   		//console.log(queries);
 //   		//return queries;
   		
 //   }
 //   console.log('returning queries:: ', queries);
	// global.queries = queries;
   //return await callback();
   //return await queries;



/*

	for await (let queryRef of queriesRef{
		console.log('queryRef: ', queryRef);
		// let query = await queryRef.ref.collection('Steps').get();
		// var steps = {};
		// for(let step of query){
		// 	steps[step.id] = step.data();
		// }
		// queries[query.id] = steps;

	}
	return queries;

*/