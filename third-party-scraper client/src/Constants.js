

export const CREATE_NEW_ROOM = 'createRoom';
export const REMOVE_ROOM = 'removeRoom';
export const UPDATE_ROOM = 'updateRoom';

//Action Types
export const ACTION_TYPE_CLICK = 'CLICK';
export const ACTION_TYPE_SCROLL_DOWN = 'SCROLL DOWN';
export const ACTION_TYPE_SCROLL_UP = 'SCROLL UP';
export const ACTION_TYPE_TYPE = 'TYPE';
export const ACTION_TYPE_SCREENSHOT = 'SCREENSHOT';
//export const ACTION_TYPE_HOLD = 'at_hold';
export const ACTION_TYPE_NEXT_URL = 'NEXT URL';
export const ACTION_TYPE_URL = 'GO TO URL';
export const ACTION_TYPE_GET = 'READ DATA';
export const ACTION_TYPE_LIST = 'READ MANY';
//export const ACTION_TYPE_BACK = 'BACK';

export const FIELD_TYPE_TEXT = 'Text';
export const FIELD_TYPE_DOLLAR = 'Price ($)';
export const FIELD_TYPE_NUMBER = 'Number';

// Transported URL string types
export const URL_ACTION_TYPE_NEXT_URL = 'nexturl';
export const URL_ACTION_TYPE_URL = 'url';
export const URL_ACTION_TYPE_GET = 'readdata';
export const URL_ACTION_TYPE_LIST = 'list';
export const URL_ACTION_TYPE_CLICK = 'click';
export const URL_ACTION_TYPE_TYPE = 'type';


//FIRESTORE REF CONSTANTS
export const USER_COLLECTION = 'Users';
export const QUERY_COLLECTION = "Queries";
export const FEEDBACK_COLLECTION = "Feedback";
export const DATA_COLLECTION = "Data";
export const TIMESTAMP_COLLECTION = 'Timestamps';
export const STEPS_COLLECTION = 'Steps';
export const WNDPC_COLLECTION = 'WNDPC';

//query details
export const QUERY_FREQUENCY_MONTHLY = 'Monthly';
export const QUERY_FREQUENCY_WEEKLY = 'Weekly';
export const QUERY_FREQUENCY_DAILY = 'Daily';
export const QUERY_FREQUENCY_HOURLY = 'Hourly';

export function translateStringToDisplay(string){
	switch(string){
		case URL_ACTION_TYPE_NEXT_URL:
			return ACTION_TYPE_NEXT_URL;
		case URL_ACTION_TYPE_URL:
			return ACTION_TYPE_URL;
		case URL_ACTION_TYPE_CLICK:
			return ACTION_TYPE_CLICK;
		case URL_ACTION_TYPE_TYPE:
			return ACTION_TYPE_TYPE;
		case URL_ACTION_TYPE_GET:
			return ACTION_TYPE_GET;
		default:
			return string;
	} 
}

/*
	Takes a string as input and returns as string for transport over sockets or HTTP
	If not found, defaults to the original string, as it means the string was correct
*/
export function translateStringToTransport(string){
	switch(string){
		case ACTION_TYPE_NEXT_URL:
			return URL_ACTION_TYPE_NEXT_URL;
		case ACTION_TYPE_URL:
			return URL_ACTION_TYPE_URL;
		case ACTION_TYPE_CLICK:
			return URL_ACTION_TYPE_CLICK;
		case ACTION_TYPE_TYPE:
			return URL_ACTION_TYPE_TYPE;
		case ACTION_TYPE_GET:
			return URL_ACTION_TYPE_GET;
		default: 
			return string;
	}
}
