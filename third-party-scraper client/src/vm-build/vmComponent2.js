import React from 'react';

//import * as v86 from "./v86/build/libv86.js"
//import {V86Starter} from "./v86/build/libv86.js";
import * as v86 from 'v86';
import fs from 'fs';
//const fs = require('fs')
// import f from './files/seabios.bin';
// import f2 from './files/vgabios.bin';
// import f3 from './files/linux.iso';



class VMComponent extends React.Component {
	constructor(props){
		super(props);
		this.state = {}
	}

	//TODO directly load the VM from here.
	//Change the onClick to directly load the VM instead

	componentDidMount(){
		//var emulator = window.emulator= new v86.V86Starter({
		
		var bios = this.readFile(__dirname + "/v86/bios/seabios.bin");
		var linux = this.readFile(__dirname + "/v86/images/linux.iso");
		var boot_start = Date.now();
		var booted = false;
		console.log('...now booting... please stand by...');

		
	    var emulator = new v86.V86Starter({ 
	        memory_size: 32 * 1024 * 1024,
	        vga_memory_size: 2 * 1024 * 1024,
	        screen_container: document.getElementById("screen_container"),
	        bios: {
	            buffer: bios,//document.all.hd_image.files[0],
	            //url: "../bios/seabios.bin",
	        },
	        // vga_bios: {
	        //     buffer: f2,
	        //     //url: "../bios/vgabios.bin",
	        // },
	        cdrom: {
	            buffer: linux,
	            //url: "../images/linux.iso",
	        },
	        autostart: true,
	    });
	    //console.log('V86Starter: ', v86);

	    console.log('EMULATOR: ', emulator);
	    emulator.run();
	}

	readFile = (path) => {
		console.log('fs: ', fs);
		return new Uint8Array(fs.readFileSync(path)).buffer;
	}

	render(){
		return(
			<div id="screen_container">
			    <div style={{whiteSpace: 'pre', font: '14px monospace', lineHeight: '14px'}}></div>
			    <canvas style={{display: 'none'}}></canvas>
			</div>
		);
	}
}

export default VMComponent;