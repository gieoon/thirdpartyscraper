import React from 'react'

import './v86/v86.css';
import * as vm86 from "./v86/build/v86_all.js";

class VMComponent extends React.Component {
	constructor(props){
		super(props);
		this.state ={};
	}

	render(){
		return(
			<div>
				<Inner />
			</div>
		);
	}
}


function Inner(props){
	return(
		<div>
		    <div id="boot_options">
		        <input type="button" value="ReactOS (32 MB)" id="start_reactos"/>
		        - Restored from snapshot<br/>
		        <input type="button" value="Windows 95 (6.7 MB)" id="start_windows95"/>
		        - Restored from snapshot<br/>
		        <input type="button" value="FreeBSD 10.2 (13.0 MB)" id="start_freebsd"/>
		        - Restored from snapshot<br/>
		        <input type="button" value="Oberon (16.0 MB)" id="start_oberon"/>
		        - Native Oberon 2.3.6 (<a href="https://lists.inf.ethz.ch/pipermail/oberon/2013/006844.html">via</a>)<br/>
		        <input type="button" value="Windows 98 (12.0 MB)" id="start_windows98"/>
		        - Including Minesweeper and audio, additional sectors are loaded as needed<br/>
		        <input type="button" value="Arch Linux (10.1 MB)" id="start_archlinux"/>
		        - A complete Arch Linux restored from a snapshot, additional files are loaded as needed<br/>
		        <input type="button" value="KolibriOS (1.4 MB)" id="start_kolibrios"/>
		        - Graphical OS, takes about 60 seconds to boot<br/>
		        <input type="button" value="Linux 2.6 (5.4 MB)" id="start_linux26"/>
		        - With busybox, Lua interpreter and test cases, takes about 20 seconds to boot<br/>
		        <input type="button" value="Linux 3.18 (8.3 MB)" id="start_linux3"/>
		        - With internet access, telnet, ping, wget and links. Takes about 60 seconds to boot. Run <code>udhcpc</code> for networking. Exchange files through <code>/mnt/</code>.<br/>
		        <input type="button" value="Windows 1.01 (1.4 MB)" id="start_windows1"/>
		        - Takes 1 second to boot<br/>
		        <input type="button" value="MS-DOS 6.22 (3.4 MB)" id="start_msdos"/>
		        - Takes 10 seconds to boot. With Enhanced Tools, QBasic and everything from the FreeDOS image<br/>
		        <input type="button" value="FreeDOS (0.7 MB)" id="start_freedos"/>
		        - With nasm, vim, debug.com, some games and demos, takes 1 second to boot<br/>
		        <input type="button" value="OpenBSD (1.4 MB)" id="start_openbsd"/>
		        - Random boot floppy, takes about 60 seconds<br/>
		        <input type="button" value="Solar OS (1.4 MB)" id="start_solos"/>
		        - Simple graphical OS<br/>
		        <input type="button" value="Bootchess (0.2 MB)" id="start_bootchess"/>
		        - A tiny chess program written in the boot sector
		        <button id="start_emulation">Start Emulation</button>
			</div>

			<div id="screen_container" style={{display: 'none'}}>
			    <div id="screen"></div>
			    <canvas id="vga"></canvas>
			    <div style={{position: 'absolute', top: 0, zIndex: 10}}>
			        <textarea className="phone_keyboard"></textarea>
			    </div>
			</div>
		</div>

	);
}

export default VMComponent;



