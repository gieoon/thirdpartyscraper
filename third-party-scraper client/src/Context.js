//react contexts

import React from 'react';

const SocketContext = React.createContext({});


export const SocketProvider = SocketContext.Provider;
export const SocketConsumer = SocketContext.Consumer;

// export const StepAreaProvider = StepAreaContext.Provider;
// export const StepAreaConsumer = StepAreaContext.Consumer;
