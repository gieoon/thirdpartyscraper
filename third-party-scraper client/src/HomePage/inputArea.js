import React from 'react';

class InputArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			url: '',//'Select a URL',//"https://www.",
			searchUrl: props.searchUrl
		}
	}

	urlInputClick = (e) => {
		this.state.searchUrl(this.state.url);
	}

	handleUrlInputChange = (e) => {
		this.setState ({
			url: e.target.value
		});
	}

	enterPressed = (e) => {
		if(e.key === 'Enter'){
			this.urlInputClick(e);
		}
	}

	render(){
		return(
			<div className="g-pd-1">
				<div id="url-form" className="g-text-center g-pd-1">
				
					<input id="url-input" 
						className="g-border-r-4" 
						value={this.state.url} 
						// autoComplete="off" 
						placeholder='Select a URL'//"https://www." 
						onChange={this.handleUrlInputChange.bind(this)}
						spellCheck="false"
						onKeyPress={this.enterPressed.bind(this)}>

					</input>
					<button id="url-submit-btn" onClick={ () => this.urlInputClick(this) }>Go</button>

				</div>
			</div>
		);
	}


}

export default InputArea;