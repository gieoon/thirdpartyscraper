import React from 'react';
import * as CONSTANTS from '../Constants';
import helpers, {getElementDirectInnerHTML} from '../API/scrapeAPI';
import * as DB from '../API/DB.js';
//import finder from '@medv/finder';
import cssPath from './homePageHelpers.js';
import cssesc from 'cssesc';
//import medv from './medv_finder.js';

//user chooses which action to do.
//try clicking, or try typing
class ActionArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			actions: [
				CONSTANTS.ACTION_TYPE_CLICK,
				CONSTANTS.ACTION_TYPE_TYPE,
				CONSTANTS.ACTION_TYPE_GET,
				CONSTANTS.ACTION_TYPE_NEXT_URL
				//CONSTANTS.ACTION_TYPE_BACK
				//CONSTANTS.ACTION_TYPE_LIST
				//CONSTANTS.ACTION_TYPE_SCREENSHOT
			],
			fields: [
				CONSTANTS.FIELD_TYPE_TEXT,
				CONSTANTS.FIELD_TYPE_DOLLAR,
				CONSTANTS.FIELD_TYPE_NUMBER
			],
			showSelectField: false,
			showNextUrl: false,
			instanceId: this.props.instanceId,
			stepArea: this.props.stepArea,
			showTypingArea: false,
			currentInput: '',
			setShowActionArea: props.setShowActionArea,
			fieldType: 'Number',
			pos1: 0, pos2: 0, pos3: 0, pos4: 0
		}

		this.actionAreaRef = React.createRef();
		this.selectFieldNameRef = React.createRef();
		this.selectNextUrlRef = React.createRef();
	}

	componentDidMount(){
		//this.startDrag();
	}


	actionClicked = (e) => {
		e.preventDefault();
		const component = this;
		if(e.target.innerText === CONSTANTS.ACTION_TYPE_GET){
			//set directRender to highlight this element
			this.props.directRender.current.setElementToGetDataOf(this.props.currentElement);
			//console.log("1: ")
			//bring up additional dialogue to name fields and set value type
			component.setState({
				showSelectField: true
			}, () => {
				console.log("show select field clicked: ", component.state.showSelectField);
			});
		}
		else if(e.target.innerText === CONSTANTS.ACTION_TYPE_NEXT_URL){
			console.log("next url!!!");
			component.setState({
				showNextUrl: true
			});
		}
		else {
			this.sendAction(e.target.innerText, this.props.currentElement);//this.props.classValues.current.getSelectedDOMObject()
			this.state.setShowActionArea(false);
		}
	}

	pressedSavedNextUrl = () => {
		this.setState({
			showNextUrl: false
		});
		this.state.setShowActionArea(false);
		this.sendAction(
			CONSTANTS.ACTION_TYPE_NEXT_URL,
			null,
			this.selectNextUrlRef.current.value,
			null
		);
	}

	pressedSaveReadDataField = () => {
		this.setState({
			showSelectField: false
		});
		this.state.setShowActionArea(false);
		//console.log("this.selectFieldNameRef.current.value: ", this.selectFieldNameRef.current.value);
		//console.log('this.state.fieldType: ', this.state.fieldType);
		this.sendAction(
			CONSTANTS.ACTION_TYPE_GET, 
			this.props.currentElement,
			this.selectFieldNameRef.current.value,
			this.state.fieldType
		);
	}

	updatePosition = (x, y) => {
		console.log('this.actionAreaRef: ', this.actionAreaRef.current);

	}

	sendAction = async (actionType, domObject, fieldName, fieldType) => {
		//disable loading on these parts

		if(actionType !== CONSTANTS.ACTION_TYPE_TYPE && actionType !== CONSTANTS.ACTION_TYPE_GET) {
			this.props.setLoading(true);
			console.log("Loading is true!!!");
		}
		//store tags into a list
		//var inputText = getElementDirectInnerHTML(domObject);
		var inputText = '';
		var description = '';
		//console.log('inputText: ', inputText);
		console.log('actionType: ', actionType);
		if(actionType === CONSTANTS.ACTION_TYPE_TYPE) {
			inputText = this.props.currentString; 
			//console.log('inputText: ', inputText);
			description = inputText;
		}
		else if(actionType === CONSTANTS.ACTION_TYPE_GET || actionType === CONSTANTS.ACTION_TYPE_LIST){
			description = domObject.tagName;
		}
		else if(actionType === CONSTANTS.ACTION_TYPE_NEXT_URL){
			description = fieldName;
		}
		else if(actionType === CONSTANTS.ACTION_TYPE_CLICK){
			//console.log('domobject: ', domObject.value);
			//console.log('domobject: ', domObject.innerHTML);

			if(domObject.value !== '' && domObject.value !== undefined){
				description = domObject.value;
			}
			else if(domObject.textContent !== ''){
				description = domObject.textContent;
			}
			else {
				description = domObject.tagName;
			}
			console.log("description");
		}

		this.state.stepArea.current.addNewAction({
			action: actionType,
			//DONT SHOW THIS UNTIL SERVER RESPONDS desc: description //domObject.innerHTML
		});
		console.log("starting search for selector");

		var selector = '';
		//check if the selector has an id, if it does, just use that
		//if the ID is duplicated, then just create a new one.
		
		// if(domObject.id !== null && domObject.id !== undefined){
		// 	selector = '#' + domObject.id;
		// }
		//else {
			//get selector of current element //one line here instead of 100 lines in the server :) //open source :)
			//greedily blocks all threads until execution is finished
			
			//BOTH OPTIONS
			//selector = finder(this.props.currentElement); //lags out when there's more than one, and finds a super complicated one.
			//selector = medv(this.props.currentElement);
			//TODO doesn't work when #page-wrapper is included as part of the page?
			if(actionType !== CONSTANTS.ACTION_TYPE_NEXT_URL)
				selector = cssPath(this.props.currentElement, true);
		//}
		var selectors = selector.length > 0 ? document.querySelectorAll(selector) : [];
		console.log('SELECTOR: ', selector);
		console.log('how many of these selectors were found: ', selectors.length);

		// console.log("added fieldName: ", fieldName);
		// console.log("added fieldType: ", fieldType);

		//console.log('actionType: ', actionType);
		const controller = helpers.getController();
		var response = await helpers.actionCSSSelector(
			actionType, 
			selector, 
			controller.signal, 
			this.state.instanceId, 
			actionType == CONSTANTS.ACTION_TYPE_TYPE || actionType == CONSTANTS.ACTION_TYPE_GET ? inputText : undefined,
			(actionType == CONSTANTS.ACTION_TYPE_GET) || (actionType == CONSTANTS.ACTION_TYPE_NEXT_URL) ? fieldName : undefined,
			actionType == CONSTANTS.ACTION_TYPE_GET ? fieldType : undefined
		);
		
		this.setState({ 
			apiController: controller, 
		});
		console.log("response: ", response);
		if(Object.keys(response).length === 0){
			console.log("response empty");
			return;
		}

		this.props.setHomePageInstanceId(response.results.instanceId);
		//steparea updated after resposne from server is received
		if(actionType === CONSTANTS.ACTION_TYPE_CLICK || actionType === CONSTANTS.ACTION_TYPE_NEXT_URL){
			if(response.results.siteList.allHTML !== ''){
				var tempQuery = helpers.receiveSelectorResponse(response.results.siteList);
				this.props.setQuery(tempQuery);
				if(actionType===CONSTANTS.ACTION_TYPE_CLICK)
					this.props.processAllHTML(response, actionType, description);
				else if(actionType === CONSTANTS.ACTION_TYPE_NEXT_URL)
					this.props.processAllHTML(response, actionType);
			}
			
		}
		else if(actionType === CONSTANTS.ACTION_TYPE_TYPE){
			if(response.results.siteList.allHTML !== ''){
				console.log('response: ', response);
				
				var tempQuery = helpers.receiveSelectorResponse(response.results.siteList);
				this.props.setQuery(tempQuery, 'typing' === 'typing');
				//dont refresh and dont wait for the page to load
				//this.props.processAllHTML(response);

				global.stepArea.current.updateAction({
					number: response.results.siteList.step, 
					action: CONSTANTS.ACTION_TYPE_TYPE,
					input: inputText
				});
			}
		}
		else if(actionType === CONSTANTS.ACTION_TYPE_GET){
			//console.log('received response: ', response);
			this.props.updateFromGetDataResponse(response);
			var tempQuery = helpers.receiveSelectorGetDataResponse(response);
			this.props.updateHomePageGetDataQuery(tempQuery);
			
			global.stepArea.current.updateAction({
				number: response.results.siteList.step, 
				action: CONSTANTS.ACTION_TYPE_GET,
				input: fieldName + ' : ' + fieldType//domObject.tagName
			});

			domObject.className += ' readdata-highlighted '
		}
	}

	handleFieldTypeChange = (e) => {
		console.log("changed frequency: ", e.target.value);
		console.log("this field name: ", this.selectFieldNameRef.current.value)
		this.setState({
			fieldType: e.target.value
		});
	}

	startDrag = () => {
		var dragElem = this.actionAreaRef.current;
		dragElem.onmousedown = this.dragMouseDown();
	}

	drag = (e) => {
		e = e || window.event;
		e.preventDefault();
		this.setState({
			pos3: e.clientX,
			pos4: e.clientY
		}, () => {
			document.onmouseup = this.closeDragElement();
			document.onmousemove = this.dragElement();
		})
	}

	dragElement = (e) => {
		const element = this.actionAreaRef.current;
		const component = this;
		e = e || window.event;
		e.preventDefault();
		this.setState({
			pos1: component.state.pos3 - e.clientX,
			pos2: component.state.pos4 - e.clientY,
			pos3: e.clientX,
			pos4: e.clientY
		}, () => {
			element.style.top = (element.offsetTop - component.state.pos2) + "px";
			element.style.left = (element.offsetLeft - component.state.pos1) + "px";
		})	
	}

	closeDragElement = () => {
		document.onmouseup = null;
		document.onmousemove = null;
	}




	// onChangeListener = (e) => {
	// 	console.log('input change detected: ', e.target.value);
	// 	this.setState({
	// 		currentInput: e.target.value
	// 	});
	// }

	// typingComplete = () => {
	// 	console.log('typing complete');
	// }

	removeActionArea = () => {
		this.setState({
			showSelectField: false,
		});
		this.state.setShowActionArea(false);
	}

	removeShowFieldsArea = () => {
		this.setState({
			showSelectField: false
		})
	}

	removeShowNextUrl = () => {
		this.setState({
			showNextUrl: false
		})
	}

	render(){
		return(
			<div ref={this.actionAreaRef} className="actionButton-container">
				{
					this.state.showSelectField ?
						<ReadDataFieldName 
							handleFieldTypeChange={this.handleFieldTypeChange}
							pressedSaveReadDataField={this.pressedSaveReadDataField}
							selectFieldNameRef={this.selectFieldNameRef}
							fields={this.state.fields} 
							removeShowFieldsArea={() => this.removeShowFieldsArea()}/>
						:
						this.state.showNextUrl ?
							<NextUrl 
								pressedSavedNextUrl={this.pressedSavedNextUrl}
								selectNextUrlRef={this.selectNextUrlRef}
								removeShowNextUrl={() => this.removeShowNextUrl()}/>
							: 
							<div id="currentActionArea" className="g-flex g-justify-space-around actionButton">
								<div className="g-flex">
									<div className="spacer"></div>
									<div className="actionAreaRemoveX" 
										 onClick={() => this.removeActionArea()}>
										<i className="material-icons">clear</i>
									</div>
								</div>
								{this.state.actions.map((action, index) =>
										<button key={index} onClick={this.actionClicked.bind(this)}>{action}</button>
								)}
							</div>
				}
			</div>
		);
	}
}
/*
{
					this.state.showTypingArea ? 
						<TypingArea onChange={this.onChangeListener.bind(this)} typingComplete={this.typingComplete}/>
						:
						''
				}
*/
//creates an additional textarea for the user to type in.
function TypingArea(props) {
	return(
		<div>
			<input className="" onChange={props.onChange}></input>
			<button onClick={() => props.typingComplete()}>Ok</button>
		</div>
	);
}

//create a field area to label the READDATA selector
function ReadDataFieldName(props) {
	return(
		<div id="currentActionArea" className="actionButton">
			<div className="g-flex">
				<div className="spacer"></div>
				<div className="actionAreaRemoveX"
					 onClick={()=>props.removeShowFieldsArea()}><i className="material-icons">clear</i></div>
			</div>
			<span>Name of this Field:</span>
			<input ref={props.selectFieldNameRef} placeholder="Default name" />
			<span>What type of Value is it:</span>
			<select defaultValue="Number" onChange={props.handleFieldTypeChange.bind(this)}>
				{
					props.fields.map((field, index) =>(
						<option key={index}>{field}</option>
					))
				}
			</select>
			<button onClick={() => props.pressedSaveReadDataField()} >Done</button>
		</div>
	);
}

function NextUrl(props) {
	return(
		<div id="currentActionArea" className="actionButton">
			<div className="g-flex">
				<div className="spacer"></div>
				<div className="actionAreaRemoveX" 
					 onClick={()=>props.removeShowNextUrl()}>
					 <i className="material-icons">clear</i>
				</div>
			</div>
			<span>Type in the next Url:</span>
			<input ref={props.selectNextUrlRef} placeholder="https://" />
			<button onClick={() => props.pressedSavedNextUrl()}>Done</button>
		</div>
	);
}

//<input ref={props.selectFieldType} placeholder="Number" />

export default ActionArea;