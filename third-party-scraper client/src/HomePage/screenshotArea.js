import React from 'react';

class ScreenshotArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			src: props.src
		}
		//console.log('src: ', this.props.src);
		
	}

	componentDidMount(){
		this.encodeToBase64(this.props.src);
	}

	componentDidUpdate(prevProps){
		//console.log('props.src: ', this.props);
		//console.log('state.: ', this.state);
		//if(this.props.src !== undefined) {
			//this.encodeToBase64(this.props.src);
		//}
	}

	//image is in uint8array buffer type, so need to convert it to base64 
	encodeToBase64(buffer){
		var binary = '';
	    var bytes = new Uint8Array( buffer );
	    var len = bytes.byteLength;
	    for (var i = 0; i < len; i++) {
	        binary += String.fromCharCode( bytes[ i ] );
	    }
		var image = btoa(binary);

		//var base64String = btoa(String.fromCharCode.apply(null, new Uint8Array(buffer)));
		this.setState({ src: 'data:image/jpeg;base64,' + image }, () => {
			//console.log('updated screenshot data: ', this.state.src);
		});
	}

	render(){

		return(
			<div className="">
			{	
				this.state.src !== undefined ?
					<div>
						<div className="g-pd-1">
							<span className="title-display"></span>
						</div>
						<img className="website-screenshot" src={this.state.src} alt="screenshot"></img>
					</div>
					:
					''
			}
			</div>
		)
	}
}

export default ScreenshotArea;