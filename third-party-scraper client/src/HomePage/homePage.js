import React from 'react';
import InputArea from './inputArea';
import ResultsArea from './resultsArea';
import DirectRender from '../Results/directRender';
import { Loader } from '../Loader/loader.js';
import helpers, { APIRequest } from '../API/scrapeAPI.js';
import * as constants from '../Constants.js';
import ActionArea from './actionArea';
import MouseHighlight from './mouseHighlight';
import SimulatedBrowser from './simulatedBrowser.js';
import './homePage.css';
import { StepAreaContext } from '../App';
//trying out ways of obtaining xpath
import io from 'socket.io-client';
import * as DB from '../API/DB.js';
import * as diff from './diff.js';
//import elementXPath from 'element-xpath';

class HomePage extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			queries: [],
			currentQuery: {},
			loading: props.loading ||false,
			screenshotData: [],
			rawContent: ' ',
			//scripts: '',
			noPage: false,
			apiController: undefined,
			instanceId: '', //unique instanceId that tracks puppeteer scraper
			rootUrl: '', //the root url to relatively load images from
			currentElement: {},
			keysArray: [],
			currentString: '',
			playingBack: props.playingBack,
			queryToShow: props.queryToShow,
			queryId: '',
			showingInput: true,
			showActionArea: false,
			socket: {},
			playingBackStepNo: 1,
			socketConnected: false,
			selectorsToHighlight: [],
			showQueryFinished: false,
			scripts: []
		}
		const query = {
			step: 0,
			title: '',
			screenshotData: [], //binary raw img data
			tagTypes: []	
		}
		this.directRender = React.createRef();
		this.actionArea = React.createRef();
		//this.containsChild = this.containsChild.bind(this);
	}

	setLoading = (isLoading) => {
		this.setState({
			loading: isLoading
		});
		console.log('updated loading state');
	}

	componentDidMount(){
		const socket = io(
			global.SERVER_PORT,
			//'localhost:5000', 
			{
				timeout: 900000
			}
		);
		if(this.state.playingBack){		
			console.log("initializing playback sockets");	
			this.initializePlaybackSockets(socket);
		}
		this.initClicker(this);
		//this.initKeyListener();	
		this.initScrollListener();
		//TODO this.initializeDirectRenderUpdatesSocket(socket);	
		
		global.stepArea.current.setAsCreateQueryPage();
		this.setState({
			socket: socket
		});
		global.homePage = this;
	}

	initializePlaybackSockets = (socket) => {
		//console.log('initializing sockets');
		const component = this;
		socket.emit('requestPlaybackConnection');
		socket.on('connectedPlayback', function(){
			//console.log('connected to server');
			socket.emit('playbackStart', { query: component.state.queryToShow, instanceId: component.state.instanceId });
			global.stepArea.current.setState({
				playbackSocket: socket,
				queryToShow: component.state.queryToShow
			}, () => {
				
			});
			socket.on('playbackStep', function(response){
				//TOADD document.getElementById('dangerous-script-container').innerHTML = '';
				//console.log("received playbackstep to display: ", response);
				component.processAllHTML(response, undefined, undefined);
				global.stepArea.current.setState({
					selectedPlaybackStepNo: response.stepNo,
					instanceId: response.instanceId,
					currentlyRequestingStep: false
				});
				component.setState({
					instanceId: response.instanceId,
					socketConnected: true
				});
			});
		});
	}

	initializeDirectRenderUpdatesSocket = (socket) => {
		//console.log("initializing direct render updates socket");
		const component = this;
		console.log("rootURL: ", this.state.rootURL);
		socket.emit('requestDirectRenderConnection');
		socket.on('connectedDirectRender', function(instanceId){
			console.log("received instanceId: ", instanceId);
			component.setState({
				socketConnected: true,
				instanceId: instanceId
			});
		});
		socket.on('appendClientDirectRender', function(document){
			//console.log("received document: ", document);
			component.receiveDocument(document);
		});
		socket.on('updateStyleSheet', function(stylesheet){
			//console.log("received stylesheet: ", stylesheet);
			var style = document.createElement('style');
			document.getElementById('dangerous-script-container').appendChild(style);
			style.type = 'text/css';
			style.appendChild(document.createTextNode(stylesheet));
		});
	}

	receiveDocument = (document) => {
		//TODO
		// var importNode = document.createElement('link');
		// importNode.href = href; 
		// importNode.rel = 'import';
		// importNode.onload = importLoaded.bind(this, importNode);
		// document.head.appendChild(importNode);

		// var importLoaded = function(importNode, event){
		// 	importNode.import = event.target.import;
		// }
	}

	updateSocketInstanceId = () => {
		const component = this;
		//this.state.socket.emit('instanceId', this.state.instanceId);
		this.state.socket.on('updateDirectRender', function(newHtml){
			console.log("received new HTML");
			component.setState({
				loading: true
			})
			
			var allHTML = component.handleDirectRender(newHtml);
			const noScriptsHTML = component.extractScriptTagsFromHTML(allHTML);
			const obj = component.extractStyleTagsFromHTML(noScriptsHTML.htmlNoScripts);
			//diff.getDiff(component.state.rawContent, allHTML);
			//console.log('allHTML: ', allHTML);
			component.setState({
				//rawContent: obj.htmlNoStyles,
				scripts: '',//obj.scripts,
				loading: false
			}, () => {
				//console.log('styles: ', obj.styles);
			});

		});
		this.state.socket.on('updateDirectRenderJavascript', function(script){
			//component.state.scripts.push(script);
			console.log("received script");
			//component.handleUpdateJavascript(script);
		})
	}

	updateHomePageQuery = (tempQuery, typing) => {
		//if typing, then don't add to render
		this.state.queries.push(tempQuery);
		if(typing === undefined){
			this.setState({
				rawContent: tempQuery.allHTML
			});
		}
		this.setState({ 
			loading: false,
			currentQuery: tempQuery,
			//rootUrl: tempQuery //disabled this, may be making rootURL undefined after the initial step
		});
	}

	updateHomePageGetDataQuery = (tempQuery) => {
		this.state.queries.push(tempQuery);
	}

	processAllHTML = async (response, actionType, input) => {
		//update action again, url may be updated
		//add action
		//TODO change this to use Redis Store, or React.Context.
		console.log("processing html: ", input);
		if(actionType !== undefined){
			global.stepArea.current.updateAction({
				number: response.results.siteList.step, 
				action: actionType,
				input: input === undefined ? response.results.siteList.url : input
			});
		}
		
		console.log('received response: ', response.results);
		//process root URL as received from server, due to server url being the correct one
		if(response.results.siteList.url !== undefined){
			var rootUrl = response.results.siteList.url;
		
			//remove the first solo slash, using negative lookbehind, and negative lookahead
			var matchResult = rootUrl.match(/(?<!\/)\/(?!\/)/);
			if(matchResult !== null){
				var indexOfMatch = matchResult.index;
				//console.log("url root match at: ", indexOfMatch);
				if(rootUrl.length > indexOfMatch){
					rootUrl = rootUrl.substring(0, indexOfMatch); // indexOfMatch + 1  cut off last slash!!!
				}
			}
			else {
				//no action, url has no subdomain to strip and is already at root
			}
			this.setState({
				rootUrl: rootUrl
			}, () => {
				console.log('this.state.rootUrl: ', this.state.rootUrl);
			});

			response = response;
			//console.log('res: ', response);
			//var allHTML = '';
			//check if page exists

			//check to highlight element to read data from
			if(response.results.selectorToHighlight !== '' && response.results.selectorToHighlight !== undefined){
				//console.log("response.results.selector: ", response.results.selectorToHighlight );
				this.highlightReadDataSelector(response.results.selectorToHighlight);
			}

			this.manageServerResponse(response);
			
		}
		else {
			console.log("page does not exist");
			this.setState({
				loading: false,
				noPage: true
			});
		}
	}

	manageServerResponse = async (response) => {
		console.log("manageServerResponse()");
		if(response.results.siteList.allHTML !== ''){
			const component = this;
			var tempQuery = helpers.receiveSelectorResponse(response.results.siteList);
			var allHTML = this.handleDirectRender(response.results.siteList.allHTML);
			//console.log("allHTML: ", allHTML);
			const htmlNoScripts = this.extractScriptTagsFromHTML(allHTML);
			const obj = this.extractStyleTagsFromHTML(htmlNoScripts.htmlNoScripts);
			this.updateHomePageQuery(tempQuery);
			this.setHomePageInstanceId(response.results.instanceId);
			this.setState({
				rawContent: obj.htmlNoStyles,
				scripts: '',//obj.scripts,
				showingInput: false
			}, () => {
				//console.log('this.state.rawContent: ', this.state.rawContent);
					//wait and rerender()
					//console.log('styles: ', obj.styles);
				//this.directRender.current.reRender();
			});
		}
	}

	extractScriptTagsFromHTML = (html) => { 
		
		//document.getElementById('dangerous-script-container').innerHTML = '';
		//console.log("extracting scripts");
		this.setState({
			loading: true
		})
		var scripts = html.match(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gmi);
		//var scripts = document.querySelectorAll('#page-wrapper script'); //cant do this because elemnt is not rendered yet
		//remove script tag from original HTML
		return { 
			htmlNoScripts: html.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gmi,''),
			scripts: scripts   //TODO
		}	
		
	}

	extractStyleTagsFromHTML = (html) => {
		//console.log('html: ', html);
		var styles = html.match(/<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gmi);
		return {
			htmlNoStyles: html.replace(/<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gmi,''),
			styles: styles
		}
		for(var s in styles){
			document.body.appendChild(s)
		}
		
	}

	addScriptTagsToHTML = () => {
		//console.log("adding scripts");
		/*
		for(var index in this.state.scripts){
			//TODO add XSS security check here, or just do not render JS at all.
			//if PASSED XSS:
			//add to original HTML area.
			//replace script tags
			//console.log("s: ", this.state.scripts[index]);
			var code = this.state.scripts[index].replace(/<script.*?>|<\/script>/gm,''); ///<script?.+?>|<\/script>/gm
			//console.log("code: ", code);
			// var parent = document.getElementById('dangerous-inner');
			// console.log("parent: ", parent);
			if(code.length > 0){
				var script = document.createElement('script');
				script.type = 'text/javascript';
				try{
					script.appendChild(document.createTextNode(code));
				} catch(err){
					script.text = code;		
				}
				//console.log("appended script to: ", document.getElementById('dangerous-script-container'));
				//document.body.appendChild(script);
				try{
					document.getElementById('dangerous-script-container').appendChild(script);
				}catch(err){
					console.log("Caught script syntax error: ", err);
					console.log("errant script: ", script);
				}
				
			}
			//FAILED XSS
			//do nothing
		}
		this.setState({
			loading: false
		})
		*/
		
	}

	highlightReadDataSelector = (cssSelector) => {
		this.state.selectorsToHighlight.push(cssSelector);
	}

	updateFromGetDataResponse = (response) => {
		global.stepArea.current.updateAction({
			number: response.results.step, 
			action: constants.ACTION_TYPE_GET, 
			input: response.results.innerData
		});
		console.log('updated data response for steparea');
	}

	searchUrlDirectRender = async (url) => {
		this.setState({ 
			loading: true,
			noPage: false,
			rawContent: url,
			apiController: this.state.apiController !== undefined ?  this.state.apiController.abort() : undefined
		});
		//add action
		//console.log('this.context: ', this.context); //this doesnt work for some reason, so using global.variable instead
		
		global.stepArea.current.updateAction({
			number: 1, 
			action: constants.ACTION_TYPE_URL, 
			input: url
		});
		const controller = helpers.getController(); 
		var response = await helpers.directRender(url, this.state.instanceId, controller.signal);//.then(res => {
		console.log('res', response);
		if(Object.keys(response).length > 0){
			this.setState({ 
				apiController: controller,
				instanceId: response.results.instanceId 
			});
			this.processAllHTML(response, constants.ACTION_TYPE_URL);
		}
		else {
			//error in response
			this.setState({
				noPage: true
			});
		}

		
	}

	handleUpdateJavascript = (script) => {
		// var scripts = '';
		// const component = this;
		// this.setState({
		// 	rawContent: component.state.rawContent + '<script>' + script + '</script>'
		// }, () => {
		// 	console.log("script added successfully");
		// });
	}

	handleDirectRender = (html) => {
		var allHtml = html;
		var styleString = ''; //'<style>'
		// response.results.styles.styles.forEach ((style) => {
		// 	console.log('adding style');
		// 	//localize every class so it doesn't affect the DOM outside of this container
		// 	//add class to front
		// 	style = '#page-wrapper ' + style;
		// 	style = style.replace(/}/gi, '} #page-wrapper '); 
		// 	//styleString += '<style>' + style + '</style>';
		// });	
		// response.results.styles.scripts.forEach((script) => {
		// 	console.log('adding script');
		// 	scripts += '<script>' + script + '</script>';
		// });

		//prepend '.dangerous' to the front of the CSS to make it only apply locally

		//allHtml = allHtml.replace(/[ ]href="https/gi,' href="https://cors.io/?https');
		//allHtml = allHtml.replace(/[ ]href="http/gi,' href="http://cors.io/?http');

		
		

		//mutate relative links, but NOT absolute links, otherwise crucial local CSS files will not load
		allHtml = allHtml.replace(/[ ]href="(?!http)(?!\/\/)/gi,' href="' + this.state.rootUrl + '/');
		allHtml = allHtml.replace(/[ ]src="(?!http)(?!\/\/)/gi,' src="' + this.state.rootUrl + '/');
		allHtml = allHtml.replace(/[ ]href=\'(?!http)(?!\/\/)/gi,' href=\'' + this.state.rootUrl + '/');
		allHtml = allHtml.replace(/[ ]src=\'(?!http)(?!\/\/)/gi,' src=\'' + this.state.rootUrl + '/');
		allHtml = allHtml.replace(/[ ]srcset="(?!http)(?!\/\/)/gi,' src="' + this.state.rootUrl + '/');
		allHtml = allHtml.replace(/[ ]srcset=\'(?!http)(?!\/\/)/gi,' src=\'' + this.state.rootUrl + '/');
		allHtml = allHtml.replace(/[ ]data-related-url=\'(?!http)(?!\/\/)/gi,' src=\'' + this.state.rootUrl + '/');
		allHtml = allHtml.replace(/[ ]data-related-url="(?!http)(?!\/\/)/gi,' src="' + this.state.rootUrl + '/');
		//change position: fixed to position:relative to be able to scroll down
		//doesn't work becuase it is linked CSS
		//allHtml = allHtml.replace(/position: fixed/g,'position: absolute');
		
		//add reverse proxy to sites that require HTML imports.
		//'https://cors.io/?'
		//identify index, and set the next href to cors.
		//while (result = /(as=("|')document("|')|rel=("|')import("|'))[^>]+/gmi.exec(allHtml)){
		let results = allHtml.matchAll(/(as=("|')document("|')|rel=("|')import("|')|rel=("|')preload("|'))[^>]+/gmi); //|rel=("|')stylesheet("|')
		//console.log("match found at " + result.index); 
		//console.log("text is: ", results);
		//replace the first link found from index
		var count = 0;
		for(var w of results){
			//console.log('w: ', w.input);
			var text = w[0].toString().replace(/[ ]href=("|')/, function(str, offset, input){
				//return str.includes("\"") ? ' href="' + global.PROXY_SERVER_PORT : " href='" + global.PROXY_SERVER_PORT;
				return str.includes("\"") ? ' href="https://cors.io/?' : " href='https://cors.io/?";
			});
			//console.log("modified text is: ", text);
			//console.log('index is: ', w.index);
			//console.log("count: ", count);
			allHtml = allHtml.substring(0, w.index + (count * 17)) + text + allHtml.substring(w.index + (count * 17));
			//console.log('related area: ', allHtml.substring(w.index + (count * 17) - 75, w.index + (count * 17) + 75));
			count++;
		}

		//replace new tab openings with opening in the current window by replacing target="_blank" with target="_self"
		//allHtml = allHtml.replace(/target="_blank"/gi,'target="_self"'); //serverside change

		//styleString += scripts;
		styleString = styleString + allHtml;
		styleString = styleString.replace(/<html|<head(?!er)|<body/gi, '<div');
		styleString = styleString.replace(/<\/html>|<\/head(?!er)>|<\/body>/gi, '</div>');
		//styles = styleString.match(/<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gmi);
		//console.log('styleString: ', styleString);
		var re = /.+?[ ]?{/gi
		var count = 0;
		//TOFIX prepend '#page-wrapper' before all CSS
		/*
		var cssStyles = styleString.replace(re, function(match, index){			
			//filter out css selector :not()
			var containsNot = match.match(/not/) !== null;
			//filter out functions
			if(!containsNot && match.match(/(\(|\))/) !== null){
				//NO ACTION
			}
			else {
				//if comma in between, split it and process on both css selectors
				if(match.match(/,/) !== null){
					var selectors = match.split(',');
					var prevLength = 0;
					var newIndex = index + (count * 15);;
					for(var i in selectors){
						if(i > 0){
							newIndex += 15;
							newIndex += prevLength; //+ 1 for comma delimiter
						}
						//console.log('previous length is: ', prevLength);
						//console.log("newIndex: ", newIndex);
						//console.log('count: ', count);
						prevLength = selectors[i].length + 1;
						styleString = styleString.substring(0, newIndex) + ' #page-wrapper ' + styleString.substring(newIndex);
						//console.log('styleString: ', newIndex, ' : ', styleString);
					}
				}
				else {
					//console.log('index: ', index);
					//console.log('match: ', match);
					var offset = (count * 15);
					++count;
					index = index + offset;
					//add 15 because ' #page-wrapper ' is 15 characters 
					styleString = styleString.substring(0, index) + ' #page-wrapper ' + styleString.substring(index);
					//console.log('styleString: ', index, ' : ', styleString)//.substring(index - offset - 5, index + 15));
				}
				
			}
		});
		*/
		
		
		
		
		//console.log("<style> html { background-color: rgba(250,150,150,.75); } </style><style> html{ background-color: rgba(250,150,150,.75); } </style>");
		//change css targeting HTML, BODY or HEAD elements to be targeting '.dangerous' div instead
		//TODO styleString = styleString.replace(/html|head|body/g,'#page-wrapper') ///html[ ]?{/g
		//wrap in css limiting classes
		//change <HTML>, <HEAD> and <BODY> tags  to <div> and leave content inside the tags
		//TODO not compatible with chrome finder 
		styleString = '<div id="page-wrapper">' + styleString + '</div>';
		
		//update image tags to be absolute links to original rootUrl
		//have to do this via index, because unsure of what kind fo quotation mark is being used
		 ///[ ]src='?"?/g 
		// var pattern = /[ ]src='?"?(?!http)/g, match = []; //[ ]src='?"?  //[ ]src=..(?!http)
		// while((match = pattern.exec(styleString))){
		// 	//console.log("updating image tag to absolute website location");
		// 	//console.log("match index: ", match.index);
		// 	styleString = styleString.substring(0, match.index + 6) + this.state.rootUrl + styleString.substring(match.index + 5);
		// }
		//match srcset for Google.com
		//[ ]srcset='?"?/g
		// var pattern2 = /[ ]srcset="(?!http)(?!\/\/)/gmi, match = [];
		// while((match = pattern2.exec(styleString))){
		// 	styleString = styleString.substring(0, match.index + 9) + this.state.rootUrl + styleString.substring(match.index + 8);
		// }
		//console.log('.allHTML: ', styleString);
		return styleString;
	}

	//for testing direct rendering
	renderTest = async(e) => {
		//var response = await helpers.renderTest();
	}

	//detects the element being clicked
	initClicker = () => {
		//console.log('initClick()!!');
		const component = this;

		document.addEventListener('click',function(e){
			//console.log('this.state.keysArray: ', component.state.keysArray)
			//console.log("clicked: ", e);
			if(component.directRender.current !== null){
				if(!component.state.loading && !component.state.noPage && component.state.rawContent.length > 0){
					//check if the element is a child of .page-wrapper
					//so, elements outside the rendered page should not be selected
					var parent = document.getElementById('page-wrapper');
					if(parent !== null && parent !== undefined){
						if(component.containsChild(parent, e.target || e.srcElement)){
							//console.log('e: ', e.target);
							//console.log('finder: ' + finder(e.target));
							//console.log("e.mouseevent: ", e.MouseEvent);
							//console.log("clientX: ", e.clientX);
							//console.log("clientY: ", e.clientY);
							//console.log('window.pageYOffset: ', window.pageYOffset);
							component.setState({
								currentElement: e.target,
								showActionArea: true,
								actionAreaX: e.clientX,
								actionAreaY: e.clientY
							}, () => {
								//console.log('component.actionArea: ', component.actionArea.current.actionAreaRef.current.style);
								if(!component.props.playingBack){ 
									//var height = component.actionArea.current.actionAreaRef.current.clientHeight;
									//var width = component.actionArea.current.actionAreaRef.current.clientWidth;
									var height = document.getElementsByClassName('actionButton')[0].clientHeight;
									var width = document.getElementsByClassName('actionButton')[0].clientWidth;
									//console.log('detected height: ', height);
									//console.log('detected width: ', width);

									//wrap around if on right of screen
									if((e.clientX + width) > window.innerWidth){
										width = (-1 * width) - (width / 10); 
										//move if the read data field panel is showing
										//console.log('adjust extra ? ', component.actionArea.current.state); //showSelectField
										if(component.actionArea.current.state.showSelectField){
											//console.log("adjusting extra");
											width *= 1.1;
										}
									}
									else {
										width = width / 10;
									}
									
									//at top of screen
									if(e.clientY < height){
										height = height / 10;
									}
									//at bottom of screen
									else if((e.clientY + height) > window.innerHeight){
										height = -1 * height;
									}
									else {
										height = -1 * (height / 2);
									}
								
									component.actionArea.current.actionAreaRef.current.style.top = (e.clientY + height) + 'px';
									component.actionArea.current.actionAreaRef.current.style.left = (e.clientX + width) + 'px';
									
								}
							});
							//console.log('component.directRender: ', component.directRender);
							if(!component.props.playingBack){
								if(component.directRender.current.state.selected){
									component.directRender.current.selectNewHighlightedElement(e);
								}
								else{
									component.directRender.current.selectHighlightedElement(e);
								}	

								//dynamically add onchange listeners to input and textarea elements
								//console.log(e.target.tagName);
								if(e.target.tagName === 'INPUT' || e.target.tagName === 'TEXTAREA'){
									//add on change listener
									//console.log('adding on change listener');
									e.target.onchange = function(e){
										//console.log('user has typed content: ', e.target.value);
										component.setState({
											currentString: e.target.value
										});
									}
								}
							}
							//disable clicks
							e.preventDefault();
							return false;
						}
					}	
				}
			}
		});
	}

	initScrollListener = () => {
		var mainArea = document.querySelectorAll('.mainArea')[0];
		console.log("adding scroll to element: ", mainArea);
		const component = this;

		mainArea.onscroll = function(){
			const el = document.querySelectorAll('.mainArea')[0];
			//console.log("scrollHeight: ", el.scrollHeight);
			//console.log("scrollTop: ", el.scrollTop);
			//console.log("clientHeight: ", el.clientHeight);
			if(!component.state.loading){
				if(el.scrollHeight - el.scrollTop === el.clientHeight){
				//if(el.getBoundingClientRect().bottom <= window.innerHeight){
					//check if user has scrolled to bottom of screen
					console.log("Scrolled to bottom!!!");
					component.state.socket.emit('scrollPls', component.state.instanceId);
				}
			}
		}
	}



	subscriber = (mutations) => {
		mutations.forEach((mutation) => {
			//handle mutations
			//console.log('mutation: ', mutation);
			//console.log('mutation.target.value: ', mutation.target.value);
			if(mutation.target.value !== undefined && mutation.target.value.length > 0){
				this.setState({
					currentString: mutation.target.value
				}, () => {
					//console.log('this.state.currentString: ', this.state.currentString);
				});
			}
		})
	}

	containsChild = (parent, child) => {
		//console.log('comparing parent: ' + parent + ' and child: ', child);
		return parent !== child && parent.contains(child);
	}

	setHomePageInstanceId = (instanceId)=> {
		const component = this;
		this.setState({
			instanceId: instanceId
		}, () => {
			//TODO this shoudln't be initializing each time randomly
			//component.initializeDirectRenderUpdatesSocket(component.state.socket);
			if(component.state.socketConnected){
				component.updateSocketInstanceId();
			}
		});
		this.setStepAreaInstanceId(instanceId);
	}

	setShowActionArea = (bool) => {
		this.setState({
			showActionArea: bool
		});
	}

	setStepAreaInstanceId = (instanceId) => {
		global.stepArea.current.setInstanceId(instanceId);
	}

	componentWillUnmount(){
		document.removeEventListener('click', () => {});
		//this.state.observer.disconnect(); //mutation observer
		//observer.takeRecords(); // -> MutationRecords

		document.getElementById('dangerous-script-container').innerHTML = '';

		//don't know the ID so randomly remove it. -- dont even know if it was successful, but brute force remove to be safe.
		for(var i = 0; i < 10000; i++){
			window.clearInterval(i);
			window.clearTimeout();
		}
		if(this.state.socket !== null){
			this.state.socket.disconnect();
		}
		console.log("HOMEPAGE unmounted");
	}

	finishedModalClickedDone = () => {
		this.setState({
			showQueryFinished: false
		});

		//console.log("saving minute: ", document.getElementsByClassName('rc-time-picker-input')[0].value);
		//split minute and hours if exists
		var minute = '', hour = '',
			dayOfWeek = global.stepArea.current.dayOfWeekRef.current !== null ? global.stepArea.current.dayOfWeekRef.current.value : '',
			day = global.stepArea.current.dayRef.current !== null ? global.stepArea.current.dayRef.current.value : ''
		if(document.getElementsByClassName('rc-time-picker-input')[0] !== null){
			minute = document.getElementsByClassName('rc-time-picker-input')[0].value
		}
		if(document.getElementsByClassName('rc-time-picker-input')[0].value.match(/:/) !== null){
			hour = document.getElementsByClassName('rc-time-picker-input')[0].value.split(':')[0];
			minute = document.getElementsByClassName('rc-time-picker-input')[0].value.split(':')[1];
		}
		console.log('minute: ', minute);
		console.log('hour: ', hour);
		console.log('day of week: ', dayOfWeek);
		console.log('day: ', day);

		DB.saveNewQueryDetails('guest', this.state.instanceId, {
			frequency: global.stepArea.current.state.renderSelector,
			dayOfWeek:	dayOfWeek,
			day: day,
			hour: hour,
			minute: minute
		});
		this.clearQuery();
	}

	clearQuery = () => {
		global.stepArea.current.reloadScreen();
		document.getElementById('dangerous-script-container').innerHTML = '';

		//don't know the ID so randomly remove it. -- dont even know if it was successful, but brute force remove to be safe.
		for(var i = 0; i < 10000; i++){
			window.clearInterval(i);
			window.clearTimeout();
		}
		this.setState({
			showingInput: true,
			rawContent: ''
		});
		global.stepArea.current.state.instanceId = '0';

	}

	finishedModalClose = () => {
		this.setState({
			showQueryFinished: false
		});
	}

	render(){
		return(
			<div className="g-flex mainArea">
					<div className="g-max-width">
					<div className="g-flex g-flex-column">
						{
							!this.state.playingBack &&
							<div>
								{	this.state.showingInput &&
									<h2 className="instruction-span">Select your website</h2>
								}
								{
									this.state.showingInput ?
										<div>
										
											<InputArea searchUrl={this.searchUrlDirectRender} />
											<div>
												{/*<a className="" href="/explore">
													<p className="ad-desc">See how others are using Automatic Processes</p>
												</a>*/}
											</div>
										</div>
										:
										''
								}

							</div>	
						}

						<div>
							{ this.state.noPage ?
								<div><p>We couldn't find that page</p>
									<ul> Have you tried the following?
										<li>Make sure the url is correct, directly copy and paste the URL from your browser</li>
										<li>The site might be down at the moment, try going to it and see what happens</li>
										<li>Just not working? <a className="g-normal-link" href="/contact">File a bug report</a></li>
									</ul>
								</div>
								:
								<div>
									{ this.state.loading ?			
										<Loader /> 	
										:
										<div>

											{this.state.rawContent && !this.state.playingBack &&
												<div>
													{
														//<p className="instruction-span">Step 2: Select your action</p> 
													}
													{
														this.state.showActionArea &&
															<ActionArea 
																ref={this.actionArea}
																setLoading={this.setLoading} 
																directRender={this.directRender}
																currentElement={this.state.currentElement}
																actionAreaX={this.state.actionAreaX}
																actionAreaY={this.state.actionAreaY}
																//classValues={this.classValuesAreaRef} 
																processAllHTML={this.processAllHTML}
																instanceId={this.state.instanceId}
																setQuery={this.updateHomePageQuery}
																setHomePageInstanceId={this.setHomePageInstanceId}
																stepArea={global.stepArea} 
																updateFromGetDataResponse={this.updateFromGetDataResponse}
																updateHomePageGetDataQuery={this.updateHomePageGetDataQuery}
																currentString={this.state.currentString}
																setShowActionArea={this.setShowActionArea}
															/>
													}
													
												</div>
											}
												<DirectRender 
														ref={this.directRender}
														setLoading={this.setLoading} 
														loading={this.state.loading} 
														query={this.state.currentQuery} 
														rawContent={this.state.rawContent}
														addScripts={this.addScriptTagsToHTML} 
														instanceId={this.state.instanceId}
														updateHomePageQuery={this.updateHomePageQuery}
														stepArea={global.stepArea}
														updateClicker={this.initClicker}
														isPlayingBack={this.state.playingBack}
														selectorsToHighlight={this.state.selectorsToHighlight}
														showingInput={this.state.showingInput}
														/>
											<>{ false && <ResultsArea 
												setLoading={this.setLoading} 
												loading={this.state.loading} 
												query={this.state.currentQuery} 
												rawContent={this.state.rawContent} 
												instanceId={this.state.instanceId}
												updateHomePageQuery={this.updateHomePageQuery}
												stepArea={global.stepArea}/>
											}</>
									</div>
								
									}
									<SimulatedBrowser />
								</div>

							}
						</div>
					</div>
				</div>
				{
					this.state.showQueryFinished &&	
						<QueryFinishedModal finishedModalClose={this.finishedModalClose} finishedModalClickedDone={this.finishedModalClickedDone} readdata={global.stepArea.current.state.actions} frequency={global.stepArea.current.state.renderSelector} />
				}
			</div>
		);
	}
}


const QueryFinishedModal = (props) => {
	console.log("props.readdata: ", global.stepArea);
	var count = 0;
	return(
		<div className="queryFinishedModal" onClick={() => props.finishedModalClose()}>
			<div className="queryFinishedModal-inner">
				<span className="removeX">
					<i className="material-icon">clear</i>
				</span>
				<h2>{global.stepArea.current.queryNameInput.current.value || 'New Query Title'}</h2>
				<div className="g-flex g-justify-space-between queryDetailsModal">
					<span>Frequency: </span>
					<span>{props.frequency}</span>
				</div>
				<div className="g-flex g-justify-space-between queryDetailsModal">	
					<span>Data Collected: </span>
					<div className="queryDetails">
					{
						Object.keys(props.readdata).map((query, index) => (
								
								props.readdata[query].action === constants.ACTION_TYPE_GET &&
								<div key={index} className="queryDetailsModal">
									<hr style={{margin: '0'}}/>
									<span>{props.readdata[query].input !== undefined && ++count}</span>
									<span >{props.readdata[query].input}</span>

								</div>
							
						))
					}
					</div>
					
				</div>
				<div className="queryFinishedModalBtn" onClick={() => props.finishedModalClickedDone()}>Done</div>
			</div>
		</div>
	);
}

/*

<StepAreaContext.Consumer>
					{(context) => console.log('CONTEXT: ', context)}
				</StepAreaContext.Consumer>


<button style={{height: '10vh'}}onClick={this.renderTest.bind(this)}>Render Test
						</button>

 <div>
    { this.state.isLoading &&
    <div>Loading.. please wait!</div>
    }
    { !this.state.isLoading &&
    <div>My data has arrived!</div>
    }
  </div>
*/

export default HomePage;


		//string = string + '</style>';
		// response.results.scripts.forEach((script) => {
		// 	string += script;
		// });
		// response.results.fonts.forEach((font) => {
		// 	string += font;
		// })

			//DEPRECATED
	// searchUrl = async (url) => {
	// 	this.setState({ 
	// 		loading: true,
	// 		rawContent: url,
	// 		apiController: this.state.apiController !== undefined ?  this.state.apiController.abort() : undefined
	// 	});
	// 	//add action
	// 	this.stepArea.current.updateAction({
	// 		number: 1, 
	// 		action: constants.ACTION_TYPE_URL, 
	// 		input: url
	// 	});
	// 	const controller = helpers.getController(); 
	// 	var response = await helpers.searchUrl(url, controller.signal);//.then(res => {
	// 	console.log('res', response);
	// 	this.setState({ 
	// 		apiController: controller,
	// 		instanceId: response.results.instanceId 
	// 	});
		
	// 	response = response.results.siteList;
		
	// 	//check if page exists
	// 	if(response.allHTML !== ''){
	// 		var tempQuery = helpers.receiveSelectorResponse(response);
	// 		//console.log('tags: ', response.tags);
	// 		this.updateHomePageQuery(tempQuery);
	// 	}
	// 	else {
	// 		console.log("page does not exist");
	// 		this.setState({
	// 			loading: false,
	// 			noPage: true
	// 		})
	// 	}
	// }


	/*
	//https://codeburst.io/observe-changes-in-dom-using-mutationobserver-9c2705222751
	initKeyListener = () => {
		// const component = this;
		// //monitor changes to the entire document
		// const target = document;//document.body
		// const observerConfig = {
		// 	attributes: true,
		// 	attributeOldValue: true,
		// 	characterData: true,
		// 	characterDataOldValue: true,
		// 	childList: true,
		// 	subtree: true
		// }
		// this.setState({
		// 	observer: new MutationObserver(this.subscriber)
		// }, () => {
		// 	this.state.observer.observe(target, observerConfig);
		// });
		

		// document.addEventListener("keypress", (e) => {
		// 	component.state.keysArray.push(e.key);
		// });
		// document.onkeydown = function(e){
		// 	console.log('keydown: ', e);
		// 	if(e.code === 'Backspace'){
		// 		//remove the last element
		// 		component.state.keysArray.splice(-1);
		// 	}
		// }
	}


	*/