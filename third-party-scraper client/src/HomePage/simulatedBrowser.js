/*
Rendering HTML strings as a webpage 
This section has proved to be the most difficult part of this product.

There are a variety of options which have been considered to some extent
1. Render an Embedded Chromium Browser Instance
2. Render an EMbedded CEFSharp Browser Instance
3. Render the strings and don't execute js scripts for safety. Extract stylesheets out and render those separately as well
4. Use a Chrome Extension for the User to gather data
5. Create a Docker container running Chrome and display that as a VM.
6. Copy existing ChromeDriver functionality to update XHR.
7. Copy iframe methods of rendering. Look at Chromium source code for iframe polyfill (to make up for lack of this functionality, add a polyfill as a plugin to access this)
8. Bypass iframe (because can't find source code)
9. Look at how Talkify does it.
10. Get HTML asynchronously without using Puppeteer, this should improve performance, but may not work with scripts.
11. Reverse Proxy, but am having trouble making this work for xmlhttprequests from the iframe.
12. Use a system that loads javascript directly.
*/

import React, {Component} from 'react';


class SimulatedBrowser extends Component {
	constructor(props){
		super(props);
		this.state = {

		}
	}

	componentDidMount(){
		//this.getHTML('https://www.youtube.com', () => {})
		// $.getJSON('http://anyorigin.com/get?url=google.com&callback=?', function(data){
  //   $('#output').html(data.contents);
//});
	}

	getHTML = (url, callback) => {
		//Feature Detection
		if(!window.XMLHttpRequest) return;
		// Create new request
		var xhr = new XMLHttpRequest();

		// Setup callback
		xhr.onload = function(){
			console.log("something loaded");
			if(callback && typeof(callback) === 'function'){
				callback(this.responseXML);
			}
		}

		// Get the HTML
		xhr.open('GET', url);
		xhr.responseType = 'document';
		xhr.send(); 
	}

	render(){
		return (
			<div></div>
		);
	}
}

export default SimulatedBrowser;