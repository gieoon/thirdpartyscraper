import React from 'react';
import ScreenshotArea from './screenshotArea';
import ClassValuesArea from './classValuesArea';

class ResultsArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			loading: props.loading,
			query: this.props.query,
			rawContent: this.props.rawContent
		}
		this.classValuesAreaRef = React.createRef();
	}

	componentDidUpdate (prevProps){
		//console.log('prevProps: ', prevProps);
		//console.log("current props: ", this.props);
		if(this.props.loading !== prevProps.loading){
			this.setState({
				loading: this.props.loading,
			}, () => {
				//console.log('query: ', this.state.query)
				//this.forceUpdate();
			});
		}
		if(this.props.query.title !== prevProps.query.title){// || this.props.query.query !== prevProps.query){
			this.setState ({
				query: this.props.query
			}, () =>{
				this.forceUpdate();
			});
		}

		if(this.props.rawContent !== prevProps.rawContent){
			//console.log('rawcontent: ', this.state.rawContent); 
			this.setState({
				rawContent: this.props.rawContent//this.renderDom()
			});
		}
	}

	//called from ActionArea when updating the visible query
	//also pushes as new query to HomePage
	setQuery = (newQuery) =>{
		this.setState({
			query: newQuery
		});
		this.props.updateHomePageQuery(newQuery);
	}

	render(){
		//console.log('this.state.query: ', this.state.query);
		return(
			<div className="g-relative g-max-width">
				{ this.state.loading ?
					''
					:
					!this.state.loading &&
					<div className="g-flex">
						<div className="left-column g-pd-1">
							{ this.state.query.url !== undefined && <div>CURRENT URL: <a className="g-highlight-link" target="_blank" rel="noopener noreferrer" href={this.state.query.url}>{this.state.query.url}</a></div>}
						
							{ this.state.query.tags !== undefined && 
								<div>
									<ClassValuesArea ref={this.classValuesAreaRef} tags={this.state.query.tags}/>
								</div>
							}
						</div>
						<div className="right-column g-pd-1">
							{ this.state.query.title !== undefined && <span className="title-display">Title: <span className="highlight-yellow">{this.state.query.title}</span></span> }
							{ this.state.query.screenshotData !== undefined && 
								<div>
									<ScreenshotArea src={this.state.query.screenshotData} />
								</div>
								}
						</div>
					</div>
				}
				
			</div>
		);
	}
}

export default ResultsArea;