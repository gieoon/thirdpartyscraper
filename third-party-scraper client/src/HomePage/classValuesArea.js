import React from 'react';

class ClassValuesArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			tags: this.props.tags,
			showType: 'Top to Bottom',
			selectedIndex: -1
		}
	}

	processData = () => {
		//TODO apply tag filtering and text search here
	}

	selectRow = (e, index) => {
		this.setState({ 
			selectedIndex: index 
		}); 
	}

	getSelectedDOMObject = () => {
		return this.props.tags[this.state.selectedIndex];
	}

	render(){
		return(
			<div>
				<div>
					Page Results: {this.state.showType}
				</div>
			{
				this.props.tags.map((dom, index) => 
					<Row key={index}
						 element={dom}
						 index={index}
						 selectRow={this.selectRow}
						 selectedIndex={this.state.selectedIndex}
		 				 />	
				)
			}
			</div>
		);
	}
}

const Row = (props) => (
	<div 
		onClick={() => props.selectRow(this, props.index)} 
		className={props.selectedIndex === props.index ? "selected-row tag-row" : "tag-row"}>
	{

		props.element.tag !== 'option' ?
		<div className="g-flex g-justify-space-between">
			<div><span>tag: <b>{props.element.tag}</b></span></div>
			{ props.element.label && <div><span>label: <b>{props.element.label}</b></span></div>}
			
			{ props.element.arialabel && <div><span>aria-label: <b>{props.element.arialabel}</b></span></div>}
			{ props.element.text && <div><span>text: <b>{props.element.text}</b></span></div>}
		</div>
		:
		''
	}
	</div>
)
/*
{ props.element.href && <div><span>href: <b>{props.element.href}</b></span></div>}
{ props.element.id && <div><span>id: <b>{props.element.id}</b></span></div> }
			{ props.element.class && <div><span>class: <b>{props.element.class}</b></span></div> }
*/
//<React.Fragment></React.Fragment>
//<> </>

export default ClassValuesArea;