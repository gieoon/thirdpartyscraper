import React from'react';
import './contact.css';
import * as DB from '../API/DB.js';
import * as FeedbackAPI from '../API/feedbackAPI.js';

class ContactPage extends React.Component {
	constructor(props){
		super(props);
		this.state = {

		}

	}

	render(){
		return(
			<div>
				<ReactForm />
			</div>
		);
	}
}


/*
<ReactContactForm to="jun.a.kagaya@gmail.com" />

			<div className="">
				<form>

					<p>Something you want to do? Help us grow by letting us know what you to see</p>
					
					<input id="feedbackInput" value="feedback" placeholder="feedback" />
					<input type="submit" /> 
				</form>

				<div className="react-form-container"></div>
			</div>


*/

export default ContactPage;


class ReactFormLabel extends React.Component {
	constructor(props){
		super(props);
		this.state = {

		}

	}
	render(){
		return(
			<label htmlFor={this.props.htmlFor}>{this.props.title}</label>
		);
	}
}

class ReactForm extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			name: '',
			email: '',
			subject: '',
			message: '',
			relatedTo: '',
			submittedSuccessfully: false
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);

		this.nameRef = React.createRef();
		this.emailRef = React.createRef();
		this.msgRef = React.createRef();
		this.submitBtnRef = React.createRef();
	}

	handleChange = (e) => {
		const component = this;
		//console.log("msgRef: ", e.target.value);
		let newState = {};
		newState[e.target.name] = e.target.value;
		this.setState(newState, () => {
			//console.log(component.state);
	
		});
		
	}

	clearAll = () => {
		
		const component = this;
		this.submitBtnRef.current.value = 'Submitted Successfully';
		this.nameRef.current.value = '';
		this.emailRef.current.value = '';
		this.msgRef.current.value = '';
		this.setState({
			submittedSuccessfully: true
		}, () => {
			//console.log("submittedSuccessfully: ");
		});
		// window.setTimeout(function(){
		// 	component.setState({
		// 		submittedSuccessfully: false
		// 	});
		// }, 2500);

	}

	handleSubmit = (e, message) => {
		console.log("SUBMITTING FEEDBACK");
		e.preventDefault();
		console.log(this.state);
		// let formData = {
		// 	formSender: this.state.name,
		// 	formEmail: this.state.email,
		// 	formSubject: this.state.subject,
		// 	formMessage: this.state.message
		// }
		// if(formData.formSender.length <1 || formData.formEmail.length < 1 || formData.formSubject.length < 1 || formData.formMessage.length < 1){
		// 	return false;
		// }
		DB.saveFeedbackToDB('guest', {
			name: this.state.name,
			email: this.state.email,
			message: this.state.message
		}, this);
		FeedbackAPI.APIRequest(this.state.name || 'NONE', this.state.email || 'NONE', this.state.message)

		// $.ajax({
		//   	url: '/some/url',
		//   	dataType: 'json',
		//   	type: 'POST',
		//   	data: formData,
		//   	success: function(data) {
		// 	   	if (confirm('Thank you for your message. Can I erase the form?')) {
		// 	    	document.querySelector('.form-input').val('')
		// 	   	}
		//   	},
		//   	error: function(xhr, status, err) {
		// 	   	console.error(status, err.toString())

		// 	  	alert('There was some problem with sending your message.')
		//  	}
		// });
	}

	render(){
		return(
			<div className="g-flex g-pd-1">
				<div className="contactPage">
				{
					this.state.submittedSuccessfully ?
						<div className="react-form submitSuccessful">
							<p>Thank you for your feedback</p>
						</div>					
						:
					<form className='react-form' onSubmit={this.handleSubmit}>
					    <h2>We value your feedback</h2>
					    <p>Let us know how we can improve your experience</p>
					    <div className="g-pd-1 form-group">
						    <fieldset className='form-group'>
						    	<ReactFormLabel htmlFor='formName' title='Name' />

						    	<input ref={this.nameRef} id='formName' className='form-input' name='name' type='text' onChange={this.handleChange} value={this.state.name} />
						    </fieldset>

						    <fieldset className='form-group'>
						    	<ReactFormLabel htmlFor='formEmail' title='How do we contact you?' />

						    	<input ref={this.emailRef} id='formEmail' className='form-input' name='email'  onChange={this.handleChange} value={this.state.email} />
						    </fieldset>	    
					    </div>

					    <fieldset className='form-group g-pd-1'>
					    	<ReactFormLabel htmlFor='formMessage' title='Message'/>
					    	<textarea ref={this.msgRef} id='formMessage' className='form-input'  rows={6} name='message' type='text' required onChange={this.handleChange}  />
					    </fieldset>

					    
					    <div className='form-group'>
					    	<input ref={this.submitBtnRef} id='formButton' className='btn' type='submit' placeholder='Send message' value='Send' />
					    </div>
					    
					</form>
				}					
				</div>
			</div>
		);
	}
}

/*
				<div className="">
					<div className="contactCard">
						<h3>Contact Details:</h3>
						<p>jun.a.kagaya@gmail.com</p>
						<p>juunoco@gmail.com</p>					
					</div>

					<div className="contactCard middle">
					</div>
					
					<div className="contactCard lower"></div>
				</div>


*/

/*
type='email'

			    <fieldset className='form-group'>
			    	<ReactFormLabel htmlFor='formSubject' title='Related Query:'/>
			    	<input id='formSubject' className='form-input' name='relatedTo' type='text' required onChange={this.handleChange} value={this.state.relatedTo} />
			    </fieldset>

<select> 
			    		<option>QUERY ID</option>
			    		<option>QUERY ID</option>
			    	</select>
			    <fieldset className='form-group'>
			    	<ReactFormLabel htmlFor='formMessage' title='Message:' />

			    	<textarea id='formMessage' className='form-textarea' name='message' required onChange={this.handleChange}></textarea>
			    </fieldset>
*/