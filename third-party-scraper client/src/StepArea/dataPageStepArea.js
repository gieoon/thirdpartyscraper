import React from 'react';
import * as DB from '../API/DB.js';

class DataPageStepArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			dataSnapshotListener: undefined,
			data: {},
			selectedQuery: {},
			selectedElement: {},
			queries: {},
		};
	}

	componentDidMount(){
		//initialize snapshot onChange listeners to DB
		const component = this;
		global.dataPage.current.setState({
			loading: true
		});

		this.setState({
			dataSnapshotListenerUnsubscribe: DB.dataPageListenToDataCollectedChanges('guest', component),
		}, () => {
			console.log("dataSnapshotListener set successfully: ", component.state.dataSnapshotListener);
		});

		DB.getQueriesByUser('guest').then(results => {
	    	console.log('got results: ', results);
	    	if(results.length > 0){
		    	this.setState({
		    		queries: results,
		    		selectedQuery: results[0] //default selected query to the first one
		    	}, () => {
		    		//console.log('results[0]: ', results[0]);
		    		global.dataPage.current.setState({
		    			//query: this.state.selectedQuery,

						queryName: results[0].name,
						loading: false
					}, () => {
						global.dataPage.current.createRunIdsFromQuery();
					});
		    	});
		    }
		    else {
		    	//TODO fix this.
		    	if(global.dataPage.current !== undefined){
			    	global.dataPage.current.setState({
			    		loading: false,
			    		query: { data: {}, queryId: 0 }
			    	})
			    }
		    }
	    })
	}

	componentWillUnmount(){
		this.state.dataSnapshotListenerUnsubscribe();
	}

	selectQuery = (e, query) => {
		//console.log("query: ", query);
		if(e.target !== undefined) e = e.target;
		if(this.state.selectedElement !== undefined && this.state.selectedElement !== null){
			this.state.selectedElement.className = 'exploreQueryCard g-flex g-flex-row g-align-items-center';
		}
		if(e.className.match(/selected/) == null ) e.className += ' selected ';
		this.setState({
			selectedElement: e,
			selectedQuery: query
		}, () => {
			this.viewSelectedQuery();
		});
	}

	viewSelectedQuery = () => {
		if(global.dataPage !== null && global.dataPage !== undefined){
			//get queryId
			const queryId = this.state.selectedQuery.queryId;
			//save queryname as a new property to data results
			if(Object.keys(this.state.queries).length > 0){
				const queryName = this.state.queries.map(query => {
					if(query.queryId === queryId) {
						return query.name;
					}
				});
				console.log('selectedQuery: ', this.state.selectedQuery);
				// this.setState({
				// 	queryName: queryName
				// })
				//console.log("selected query name: ", queryName);
				global.dataPage.current.setState({
					query: this.state.selectedQuery,
					queryName: queryName
				}, () => {
					global.dataPage.current.createRunIdsFromQuery();
				});
			}
		}
	}

	render(){
		const component = this;
		return(
			<div className="explorePageDefaultText">
				{ 
					this.props.showing && <h3 className="g-color-white">Your Queries</h3>
				}
				{	
					<div>
						{
						Object.keys(this.state.queries).length > 0 && Object.keys(this.state.data).length ?
							this.state.queries.map((obj, index) => (
								<div key={obj.queryId} 
									className={ 
										obj.queryId === this.state.selectedQuery.queryId ? "exploreQueryCard g-flex g-flex-row g-align-items-center selected" : "exploreQueryCard g-flex g-flex-row g-align-items-center"
									}
									onClick={(e) => this.selectQuery(e, obj)}>

									<div className={ component.props.showing ? "queryTitle g-no-click" : "queryTitleAbs g-no-click"}>									
										{

											<span className={ component.props.showing ? "exploreQueryStepNumber" : ""}>{index + 1}  </span>
										}
										{
											component.props.showing && 
											<span >{
												component.state.queries[index].name ?
													component.state.queries[index].name 
													: 'New Query Title'
											}</span>
										}
										
									</div>
									{
										component.props.showing &&
										<span className="highlightable" >
											<i className="fa fa-caret-left giveMeSpace"></i>
										</span>
									}
									
									
								</div>

							))
						
						:
						<div className="emptyText">
						{
							this.props.showing &&
							<span>No data has been collected yet</span>
						}
						</div>	
					}	
					</div>
				}
			</div>
		);
	}
}
//className={component.state.queries[index].deleted ? 'deletedQuery' : ''}
											
export default DataPageStepArea;