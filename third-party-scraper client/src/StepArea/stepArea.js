import React from 'react';
import * as CONSTANTS from '../Constants.js';
import ExplorePageStepArea from './explorePageStepArea.js';
import DataPageStepArea from './dataPageStepArea.js';
//import logo from '../logo.svg';
import './stepArea.css';
import * as DB from '../API/DB.js';
import PlaybackArea from './playbackArea.js';
import 'rc-time-picker/assets/index.css';
//import ReactDom from 'react-dom';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import './timePicker.css';


class StepArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			actions: [
				{
					action: 'Select a URL',
					selection: ''//this.selectorSwitch(CONSTANTS.ACTION_TYPE_NEXT_URL, 'http://www.wefwef.com')//<UrlSelector url='http://www.google.com' />	
				}
			],
			showing: true,
			instanceId: '0',
			showQueryName: true,
			showExplorePageDefaultText: false,
			showCollectedDataDefaultText: false,
			showPlayBackArea: false,
			selectedPlaybackStepNo: 1,
			playbackSocket: {},
			queryToShow: {},
			currentlyRequestingStep: false,
			showQueryFinished: false,
			showFinishedBtn: false,
			renderSelector: false,
			queryName: ''
		}
		this.queryNameInput = React.createRef();
		this.explorePageStepAreaRef = React.createRef();
		this.dataPageStepAreaRef = React.createRef();
		this.queryNameContainer = React.createRef();
		this.frequencyRef = React.createRef();
		this.dayOfWeekRef = React.createRef();
		this.dayRef = React.createRef();
		this.removeAction = this.removeAction.bind(this);
	}

	componentDidMount(){
		DB.initDB();
	}

	requestPlaybackStep = (stepNo) => {
		console.log("requesting step: ", this.state.instanceId);
		if(!this.state.currentlyRequestingStep){
			this.state.playbackSocket.emit('getPlaybackStep', 
			{
				query:this.state.queryToShow, 
				instanceId: this.state.instanceId, 
				stepNo: stepNo
			});
			this.setState({
				currentlyRequestingStep: true
			});
		}
	}

	requestPlaybackStop = () => {}
	requestPlaybackPause = () => {}
	requestPlaybackStart = () => {}

	setAsCreateQueryPage = () => {
		//console.log("setting to explore page");
		this.setState({
			actions: [
				{
					action: 'Select a URL',
					selection: ''//this.selectorSwitch(CONSTANTS.ACTION_TYPE_NEXT_URL, 'http://www.wefwef.com')//<UrlSelector url='http://www.google.com' />	
				}
			],
			showQueryName: true,
			showFinishedBtn: true, 
			showExplorePageDefaultText: false,
			showCollectedDataDefaultText: false,
			showPlayBackArea: false,
			selectedPlaybackStepNo: 1
		}, () => {
			this.forceUpdate();
		});
	}
	//user has chosen to explore a query
	setAsExplorePage = () => {
		//console.log("setting to explore page");
		this.setState({
			actions: [],
			showQueryName: false,
			showExplorePageDefaultText: true,
			showCollectedDataDefaultText: false,
			showPlayBackArea: false,
			selectedPlaybackStepNo: 1,
			showQueryFinished: false,
			showFinishedBtn: false
		}, () => {
			this.forceUpdate();
		});
		
	}

	setAsDataPage = () => {
		this.setState({
			actions: [],
			showQueryName: false,
			showCollectedDataDefaultText: true,
			showExplorePageDefaultText: false,
			showPlayBackArea: false,
			showQueryFinished: false,
			showFinishedBtn: false
		}, () => {
			this.forceUpdate();
		});
	}

	addNewAction = (step) => {
		//console.log('adding new action: ', step);
		this.state.actions.push({action: step.action, selection: step.desc});	
		this.forceUpdate();
	}

	updateAction = (step) => {
		console.log("updating action: ", step);
		this.state.actions[step.number - 1] = {
			action: step.action, 
			selection: this.selectorSwitch(step.action, step.input),
			input: step.input
		};
		this.forceUpdate();
	}

	removeAction = (index) => {
		console.log('removing action: ', index);
		this.state.actions.splice(index, 1);
		//cascade all other actions down by reducing key number
		this.forceUpdate();
	}

	reloadScreen = () =>{
		this.state.actions =  [
			{
				action: 'Select a URL',
				selection: ''//this.selectorSwitch(CONSTANTS.ACTION_TYPE_NEXT_URL, 'http://www.wefwef.com')//<UrlSelector url='http://www.google.com' />	
			}
		];
		this.forceUpdate();
		//this.queryId = 
		//create a new queryId
	}

	//for loading a whole query to display
	//called directly from explorePage.js
	loadPlaybackQuery = (query) => {
		const component = this;
		console.log('loading query: ', query);
		const newActions = [];

		Object.keys(query.steps).forEach(step => {
			newActions[step - 1] = {
				action: CONSTANTS.translateStringToDisplay(query.steps[step].action),
				selection: query.steps[step].selector
			};
		});
		this.setState({
			actions: Object.assign([], this.state.actions, newActions),
			showQueryName: true,
			showFinishedBtn: true, 
			showPlayBackArea: true,
			//instanceId: 1 //this is 1 until server returns a value
		}, () => {
			this.queryNameInput.current.value = query.name || 'New Query Title';
			console.log('actions are: ', this.state.actions);
			//update action CSS
			component.state.actions.forEach(()=> {
				component.forceUpdate();
			});
		});
	}

	//the .desc part of the action
	selectorSwitch = (param, input) => {
		switch(param){
			case CONSTANTS.ACTION_TYPE_URL:
			case CONSTANTS.ACTION_TYPE_NEXT_URL:
				return <UrlSelector url={input} /> 
			case CONSTANTS.ACTION_TYPE_TYPE:
			case CONSTANTS.ACTION_TYPE_CLICK:
			case CONSTANTS.ACTION_TYPE_GET:
				return <span>{input}</span>
		}
	}

	triggerShowing = () => {
		//console.log("triggering");
		this.setState({
			showing: !this.state.showing
		});
		//console.log('global.dataPage: ', global.dataPage);
		if(global.dataPage.current !== undefined && global.dataPage.current !== null){
			global.dataPage.current.forceUpdate();
		}
	}

	//TODO get new query number from database
	getDefaultQueryNumber = () => {
		//console.log('default query number: ');
		//get the number of queries this user has, and set the new query number to be that value + 1
		//or add (2) to make it different
		return 'New Query Title';
	}

	saveQueryName = async () => {
		console.log("saving new query name: ", this.queryNameInput.current.value);
		var newName = this.queryNameInput.current.value;
		this.setState({
			queryName: newName
		});
		//need to save to the instanceId
		var success = await DB.saveNewQueryName('guest', this.state.instanceId, this.queryNameInput.current.value);
		//console.log("success: ", success);
		if(success > 0){
			//console.log("this.queryNameContainer: ", this.queryNameContainer);
			this.queryNameContainer.current.className = 'queryNameArea';
		}
	}

	//called externally
	setInstanceId = (instanceId) => {
		//console.log('setting instanceId: ', instanceId);
		this.setState({
			instanceId: instanceId
		});
	}

	clickQueryNameArea = (e) => {
		//console.log("clicked query name area: ", e.target.parentNode.className);
		if(this.state.queryName !== this.queryNameInput.current.value){
			if(e.target.parentNode.className.match(/queryNameAreaShowing/) !== null){
				e.target.parentNode.className = 'queryNameArea';
			}
			else {
				e.target.parentNode.className += ' queryNameAreaShowing ';
			}
		}
	}

	queryNameAreaChange = (e) => {
		if(this.state.queryName !== this.queryNameInput.current.value){
			e.target.parentNode.className = 'queryNameArea queryNameAreaShowing ';
		}
		else {
			e.target.parentNode.className = 'queryNameArea';
		}		
	}

	selectAction = (e) => {
		console.log('e.target: ', e.target);
		if(this.state.showPlayBackArea){
			console.log("this.state.selectedAction: ", this.state.selectedAction);
			if(this.state.selectedAction !== undefined){
				this.state.selectedAction.target.className = this.state.selectedAction.target.className.replace(/selected/, '');
			}
			if(e.target.className.match(/selected/) == null ) e.target.className += ' selected ';
			this.setState({
				selectedAction: e,
			});
		}
	}

	//for explore page to get the currently active query
	viewSelectedQuery = () => {
		//console.log('updating query to: ', this.explorePageStepAreaRef.current.state.selectedQuery);
		if(this.explorePageStepAreaRef.current !== null && global.explorePageQueriesArea.current !== null){
			//global.explorePageQueriesArea.current.state.selectedQuery = this.explorePageStepAreaRef.current.state.selectedQuery;
			//global.explorePageQueriesArea.current.forceUpdate();
			global.explorePageQueriesArea.current.setState({
				selectedQuery: this.explorePageStepAreaRef.current.state.selectedQuery
			});
		}
	}

	getActionColor = (index) => {
		switch(this.state.actions[index].action){
			case CONSTANTS.ACTION_TYPE_URL:
			case CONSTANTS.URL_ACTION_TYPE_URL:
				return 'url';
			case CONSTANTS.ACTION_TYPE_NEXT_URL:
			case CONSTANTS.URL_ACTION_TYPE_NEXT_URL:
				return 'nexturl';
			case CONSTANTS.ACTION_TYPE_CLICK:
			case CONSTANTS.URL_ACTION_TYPE_CLICK:
				return "click"
			case CONSTANTS.ACTION_TYPE_GET:
			case CONSTANTS.URL_ACTION_TYPE_GET:
				return "readdata"
			case CONSTANTS.ACTION_TYPE_TYPE:
			case CONSTANTS.URL_ACTION_TYPE_TYPE:
				return "typetext"
			default:
				return ""
		}
	}

	clickedQueryFinished = () => {
		global.homePage.setState({
			showQueryFinished: true
		});
	}

	updateFrequency = (value) => {
		this.setState({
			renderSelector: value
		});
	}

	render(){
		return(
			<div className={this.state.showing ? "stepArea showing" : "stepArea"}>
				<div id="left-logo-container">
					<div id="left-logo">
						<span className={this.state.showing? "showing" : "collapsed"}>
							{this.state.showing 
								? <span>DATA<span style={{color: 'red', fontWeight: 'bold', letterSpacing: '1.75px'}}>2</span>U</span> 
								: <span>D<span style={{color: 'red', fontWeight: 'bold', letterSpacing: '1.75px'}}>2</span></span>
							}
						</span>
						<br/>
						<span className={this.state.showing? "showing" : "collapsed"} style={{fontSize: ".85rem"}}>
							{this.state.showing ? "Automate your online workflow" : ""}
						</span>
						
					</div>
					{
						this.state.showing && this.state.showQueryName && this.state.instanceId !== 0 && this.state.instanceId !== '0' &&
						<div>
							<div ref={this.queryNameContainer} className="queryNameArea">
								<input ref={this.queryNameInput} 
									placeholder={this.getDefaultQueryNumber()}
									onClick={this.clickQueryNameArea.bind(this)}
									onChange={this.queryNameAreaChange.bind(this)} />

								{
									/*
									<div className="pencil-container" onClick={() => this.saveQueryName()}>
										<i className="fa fa-pencil"></i>
									</div>
									*/
								}
								<div id="queryNameAreaSave" onClick={() => this.saveQueryName()}>
									<span>Save</span>
								</div>
								
							</div>
							<FrequencyToggle frequencyRef={this.frequencyRef} updateFrequency={this.updateFrequency}/>
							{
								this.state.renderSelector &&
								<SelectRenderer renderSelector={this.state.renderSelector} dayRef={this.dayRef} dayOfWeekRef={this.dayOfWeekRef}/>	
							
							}
							
							
						</div>	
					}
					{
						//(this.state.showExplorePageDefaultText || this.state.showCollectedDataDefaultText) && this.state.showing &&
						//<h3 className="explorePageDefaultText">Your Queries</h3>
					}
					</div>
					<div id="stepArea-scrollable" className={ Object.keys(this.state.actions).length > 0 ? "scrollbar not-empty" : "scrollbar"}>
					{
						this.state.showExplorePageDefaultText &&
							<ExplorePageStepArea ref={this.explorePageStepAreaRef} viewSelectedQuery={this.viewSelectedQuery} showing={this.state.showing}/>
							
					}{	this.state.showCollectedDataDefaultText &&
							<DataPageStepArea ref={this.dataPageStepAreaRef} showing={this.state.showing} />
					}

					{ this.state.actions.map((stepnumber, index) => (

						<div key={index} 
							className={
								(() => {
									var string = 'actionStep';
									if(this.state.showing) string += ' showing ';
									//console.log("index: ", index);
									//console.log('playbackstepno: ', this.state.selectedPlaybackStepNo);
									//this was an accident, but it's REALLY GOOD!!
									if(this.state.showPlayBackArea){
										if(this.state.selectedPlaybackStepNo !== index + 1) string += ' selected ';
									}
									return string;
								//this.state.showing ? "actionStep showing" : "actionStep"
								})() 
							} 
							//onClick={this.selectAction.bind(this)}
							>
							
						{
							//console.log("rendering action: " + stepnumber)
						}
						{ this.state.showing ?
							this.state.actions[index].action &&
								<div key={index} className={this.getActionColor(index)}>
									<div className="stepNumber">{index + 1}</div> 
									<div className="stepLeft action">{this.state.actions[index].action }
										<div key={index * 2} className="stepIcon"><i key={index * 3} className={
											(() => {
												switch(this.state.actions[index].action){
													case CONSTANTS.ACTION_TYPE_URL:
													case CONSTANTS.URL_ACTION_TYPE_URL:
													case CONSTANTS.ACTION_TYPE_NEXT_URL:
													case CONSTANTS.URL_ACTION_TYPE_NEXT_URL:
														return "fa fa-link";
													case CONSTANTS.ACTION_TYPE_CLICK:
													case CONSTANTS.URL_ACTION_TYPE_CLICK:
														return "fa fa-mouse-pointer";
													case CONSTANTS.ACTION_TYPE_GET:
													case CONSTANTS.URL_ACTION_TYPE_GET:
														return "fa fa-book";//"fa fa-eye" //"fas fa-book-open"
													case CONSTANTS.ACTION_TYPE_TYPE:
													case CONSTANTS.URL_ACTION_TYPE_TYPE:
														return "fa fa-pencil";//"far fa-keyboard"
													default:
														return ""
												}
											})()
										}
										></i></div>
									</div>
									
									<div className="stepLeft"> <span>{this.state.actions[index].selection }</span></div>	
									<div className="removeX" onClick={() => this.removeAction(index)}><i className="material-icons">clear</i></div>
								</div>
							:
							<div>
								<div className="">{index + 1}</div>
							</div>
						}
						</div>
					)) 
					}
				</div>
				{
					this.state.showFinishedBtn && this.state.instanceId !== 0 && this.state.instanceId !== '0' &&
					<div className="queryFinishedBtn-outer">
						<div className="queryFinishedBtn" onClick={() => this.clickedQueryFinished()}>
							{
								this.state.showing ?
									<span>Save</span>
								:
									<span><i className="querySaveIcon material-icons">save</i></span>
							}
						</div> 
					</div>
				}
				{
					this.state.showPlayBackArea &&
					<PlaybackArea 
						showing={this.state.showing} 
						currentlyRequestingStep={this.state.currentlyRequestingStep}
						currentPlaybackStep={this.state.selectedPlaybackStepNo}
						requestPlaybackStep={this.requestPlaybackStep}
						requestPlaybackStart={this.requestPlaybackStart}
						requestPlaybackPause={this.requestPlaybackPause}
						requestPlaybackStop={this.requestPlaybackStop}/>
				}
				
				<div id="arrow-container" onClick={() => this.triggerShowing()}>
					<i className={this.state.showing ? "material-icons showing" : "material-icons collapsed"} id="arrow-button"  >chevron_right</i>
				</div>


			</div>
		);
	}
}

function UrlSelector(props) {
	return(
		<div>
			<a  className="g-highlight-link" 
				target="_blank" 
				rel="noopener noreferrer" 
				href={props.url}>
				{props.url}
			</a>
		</div>
	);
}





	// 	return(
	// 		<div className="collectedDataDefaultText">
	// 			{
	// 				//<h3>Your Queries</h3>
	// 			}
	// 			{
	// 				this.props.showing && 
	// 					<div>
	// 						<h3 className="g-color-white">Your Queries</h3>
	// 						{
	// 							Object.keys(this.state.data).length === 0 &&
	// 							<span className="emptyText">Create a query to display it here</span>
	// 						}
							
	// 					</div>
	// 			}	
	// 			<div>
	// 				{
	// 					Object.keys(this.state.data).length > 0 &&
	// 					<div>
	// 					{
	// 						this.state.data.map((queryData, index) => (
	// 							<div key={index}>
	// 								{
	// 									queryData.queryId
	// 								}
	// 							</div>
	// 						))
	// 					}
	// 					</div>
	// 				}
	// 			</div>
				
				
	// 		</div>
	// 	);
	// }

//{/*<span>Email me </span>*/}
const SelectRenderer = (props) => {
	//console.log("props.frequencyRef: ", props.frequencyRef);
	switch(props.renderSelector){
		case CONSTANTS.QUERY_FREQUENCY_MONTHLY:
			return <div><DayToggle dayRef={props.dayRef} /><TimeToggle /></div>;
		case CONSTANTS.QUERY_FREQUENCY_WEEKLY:
			return <div><DayOfWeekToggle dayOfWeekRef={props.dayOfWeekRef}/><TimeToggle /></div>;
		case CONSTANTS.QUERY_FREQUENCY_DAILY:
			return <div><TimeToggle /></div>;
		case CONSTANTS.QUERY_FREQUENCY_HOURLY:
			return <div><TimeToggle onlyMinutes={true}/></div>;
		default: 
			return null;
	}	
}

class FrequencyToggle extends React.Component {
	constructor(props){
		super(props);
		this.state = {

		}
	}

	componentDidMount(){
		this.props.updateFrequency("Daily");
	}

	handleChange = (e) => {
		console.log("changed frequency: ", e.target);
		this.props.updateFrequency(e.target.value);
	}

	render(){
		return(

			<div className="g-flex g-justify-space-between toggle">
				<div  className="frequencyToggle-label"><span>Frequency:</span></div>
				<div className="frequencyToggle">
					<select onChange={this.handleChange.bind(this)} defaultValue="Daily">
						<option value="Monthly">{CONSTANTS.QUERY_FREQUENCY_MONTHLY}</option>
						<option value="Weekly">{CONSTANTS.QUERY_FREQUENCY_WEEKLY}</option>
						<option value="Daily" >{CONSTANTS.QUERY_FREQUENCY_DAILY}</option>
						<option value="Hourly">{CONSTANTS.QUERY_FREQUENCY_HOURLY}</option>
					</select>
				</div>
			</div>

		);
	}
}

const TimeToggle = (props) => {
	return(
		<div className="g-flex g-justify-space-between toggle">
			<div className="timeToggle-label">
				<span>Time{props.onlyMinutes && ' (Minutes)'}: </span>
			</div>
			<div className="timeToggle">
				<div></div>
				<TimePicker defaultValue={moment()} showHour={!props.onlyMinutes} showSecond={false} minuteStep={1}/>
			</div>
		</div>
	);
}

const DayOfWeekToggle = (props) => {
	return(
		<div className="g-flex g-justify-space-between toggle">
			<div className="toggle-label">
				<span>Day of Week:</span>
			</div>
			<div className="timeToggle">
				<select ref={props.dayOfWeekRef}>
					<option value="monday">Monday</option>
					<option value="tuesday">Tuesday</option>
					<option value="wednesday">Wednesday</option>
					<option value="thursday">Thursday</option>
					<option value="friday">Friday</option>
					<option value="saturday">Saturday</option>
					<option value="sunday">Sunday</option>
				</select>
			</div>
		</div>
	);
}

const DayToggle = (props) => {
	const days = [];
	for(var i = 1; i < 32; i++){
		days.push(i);
	}
	return(
		<div className="g-flex g-justify-space-between toggle">
			<div className="toggle-label">
				<span>Day of Month:</span>
			</div>
			<div className="timeToggle">
				<select ref={props.dayRef}>
					{
						days.map((day, index) =>(
							<option key={index} value={day}>{day}</option>
						))
					}
				</select>
			</div>
		</div>
	);
}

export default StepArea;



// const urlSelector = (
// 	<div>
// 		<a>wefwef</a>
// 	</div>
// );


/*

{
			props.toggleCount === 3 &&
			<div class="switch-toggle switch-candy switch-block">
			  <input id="week" name="view" type="radio" checked/>
			  <label for="week" onclick="">Week</label>

			  <input id="month" name="view" type="radio"/>
			  <label for="month" onclick="">Month</label>

			  <input id="day" name="view" type="radio"/>
			  <label for="day" onclick="">Day</label>
			  <a></a>
			</div>
		}
		{
			props.toggleCount === 2 &&
			<div class="switch-block">
			  <label class="switch-light switch-candy">
			    <input type="checkbox" name="" id="" />
			    <span>
			      <span>off</span>
			      <span>on</span>
			      <a></a>
			    </span>
			  </label>
			</div>
		}
		{
			props.toggleCount > 3 &&
			<div class="switch-toggle switch-candy">
			  <input type="radio" name="view3" id="hour3" checked />
			  <label for="hour3">Hour</label>
			  
			  <input type="radio" name="view3" id="day3" />
			  <label for="day3">Day</label>
			  
			  <input type="radio" name="view3" id="week3" />
			  <label for="week3">Week</label>
			  
			  <input type="radio" name="view3" id="Month3" />
			  <label for="Month3">Month</label>
			  
			  <input type="radio" name="view3" id="year3" />
			  <label for="year3">Year</label>
			  
			  <input type="radio" name="view3" id="decade3" />
			  <label for="decade3">Deacade</label>
			  <a href=""></a>
			</div>
		}	

*/