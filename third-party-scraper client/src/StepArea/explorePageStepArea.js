import React from 'react';
import * as DB from '../API/DB.js';

class ExplorePageStepArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			queries: [],
			showing: this.props.showing,
			selectedQuery: {},
			selectedElement: {}
		};
		//this.selectQuery = this.selectQuery.bind(this);
	}

	componentDidMount(){
	    //const component = this;
	    DB.getQueriesByUser('guest').then(results => {
	    	//console.log('got results: ', results[0]);
	    	this.setState({
	    		queries: results,
	    		selectedQuery: results[0] //default selected query to the first one
	    	});
	    });
	    DB.listenToUsersQueries('guest', this.state.queries, this);
	}

	// componentDidUpdate(prevProps, prevState){
	// 	console.log('prevProps: ', prevProps);
	// 	console.log("prevState: ", prevState);
	// 	console.log("this.state.queries: ", this.state.queries);
	// 	//this.forceUpdate();
	// }

	selectQuery = (e, query) => {
		//console.log("query selected: ", query);
		//console.log('element returned: ', e.target);
		if(e.target !== undefined) e = e.target;
		
		//console.log("selected query: ", e.target);
		//change previously selected query back
		if(this.state.selectedElement !== undefined && this.state.selectedElement !== null){
			//old query is standard
			this.state.selectedElement.className = 'exploreQueryCard g-flex g-flex-row g-align-items-center';
		}
		//set new query to selected one
		if(e.className.match(/selected/) == null ) e.className += ' selected ';
		this.setState({
			selectedElement: e,
			selectedQuery: query
		}, () => {
			this.props.viewSelectedQuery();
		});
	}

	// selectQueryUsingIndex = (index) => {
	// 	const component = this;
	// 	if(this.state.selectedElement.target !== undefined ){
	// 		//old query is standard
	// 		this.state.selectedElement.target.className = 'exploreQueryCard g-flex g-flex-row g-align-items-center';
	// 	}

	// 	var element = document.getElementsByClassName('exploreQueryCard')[index];
	// 	console.log("elements: ", element);
	// 	element.className += ' selected ';
	// 		this.setState({
	// 		selectedElement: element,
	// 		selectedQuery: component.state.queries[index] 
	// 	}, () => {
	// 		this.props.viewSelectedQuery();
	// 	});
	// }

	render(){
		const component = this;
		//console.log("state.queries: ", this.state.queries);
		return(
			<div className="explorePageDefaultText">
				{ 
					this.props.showing && <h3 className="g-color-white">Your Queries</h3>
				}
				{
					<div>
						{
						Object.keys(this.state.queries).length > 0 ?
							this.state.queries.map((obj, index) => (
								<div key={obj.queryId} className={ 
										obj.queryId === this.state.selectedQuery.queryId ? "exploreQueryCard g-flex g-flex-row g-align-items-center selected" : "exploreQueryCard g-flex g-flex-row g-align-items-center"
									}
									onClick={(e) => this.selectQuery(e, obj)}>

									<div className={ component.props.showing ? "queryTitle g-no-click" : "queryTitleAbs g-no-click"}>									
										{

											<span className={ component.props.showing ? "exploreQueryStepNumber" : ""}>{index + 1}  </span>
										}
										{
											component.props.showing &&
											<span>{obj.name || 'New Query Title'}</span>
										}
										
										{
											//<span>{obj.queryId}</span>
										}
										
									</div>
									{
										component.props.showing &&
										<span className="highlightable" >
											<i className="fa fa-caret-left giveMeSpace"></i>
										</span>
									}
									
									
								</div>

							))
						
						:
						<div className="emptyText">
						{
							this.props.showing &&
							<span>Create a query to display it here</span>
						}
						</div>	
					}	
					</div>
				}
			</div>
		);
	}
}

export default ExplorePageStepArea;