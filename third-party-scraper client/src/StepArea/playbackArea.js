
import React from 'react';

class PlaybackArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			showing: props.showing,
			currentlyRequestingStep: props.currentlyRequestingStep, //can only request one step at a time and then have to wait for server
			currentPlaybackStep: props.currentPlaybackStep,
			requestPlaybackStep: props.requestPlaybackStep,
			requestPlaybackStop: props.requestPlaybackStop,
			requestPlaybackStart: props.requestPlaybackStart,
			requestPlaybackPause: props.requestPlaybackPause
		}
	}

	render(){
		return(
			<div className="flex-container">
				<div id="playbackArea" className={this.state.showing ? 'showing' : ''}>
					{
						this.props.showing ? 
							<div>
								<div className="g-flex">
									<div className="playbackWrapper g-flex g-justify-space-between"
										onClick={() => this.state.requestPlaybackStep(this.props.currentPlaybackStep - 1)}>
										<div className="playbackDesc ">Previous</div>
										<i className="material-icons playbackButtonUpper showing">skip_previous</i>	
										{/*<div className="verticalDivider"></div>*/}
									</div>
									<div className="playbackWrapper g-flex g-justify-space-between"
										onClick={() => this.state.requestPlaybackStep(this.props.currentPlaybackStep + 1)}>
										<div className="playbackDesc">Next</div>
										<i className="material-icons playbackButtonUpper showing">skip_next</i>	
									</div>
								</div>
								{ /*<hr className="divider" align="center"/> */}
								<div className="g-flex">
									<div className="playbackWrapper">
										<i className="material-icons playbackButton showing">play_arrow</i>	
									</div>
									
									<div className="playbackWrapper">
										<i className="material-icons playbackButton showing">pause</i>
									</div>	
									
									<div className="playbackWrapper">
										{/*<div className="playbackDesc">Stop</div>*/}
										<i className="material-icons playbackButton showing">stop</i>
									</div>	
								</div>
							</div>
							:
							<div>
								<div className="playbackWrapper hidden">
									<i className="material-icons  collapsed">skip_previous</i>
								</div>
								<div className="playbackWrapper hidden">
									<i className="material-icons  collapsed">skip_next</i>
								</div>
								<div className="playbackWrapper hidden">
									<i className="material-icons playbackButton collapsed">play_arrow</i>
								</div>
								
								<div className="playbackWrapper hidden">
									<i className="material-icons playbackButton collapsed">pause</i>
								</div>
								
								<div className="playbackWrapper hidden">
									<i className="material-icons playbackButton collapsed">stop</i>
								</div>
							</div>
					}
				</div>
				
			</div>
		);
	}
}

export default PlaybackArea;