import React from 'react';
import './visualize.css';
import DirectRender from '../Results/directRender';
import { Loader } from '../Loader/loader';
import io from 'socket.io-client';


//TODO, depending on UX, move this to a sidepanel of explore.
//the problem with that is, where does the query area go then? And what does the main side panel do?
class VisualizeArea extends React.Component {
	constructor(props){
		super(props);
		console.log('props: ', props);
		this.state = {
			loading: true,
			queryId: props.queryId
		}
	}

	componentDidMount(){
		const socket = io('localhost:5000');
		this.initializeSockets(socket);
	}

	initializeSockets = (socket) => {
		console.log('initializing sockets');
		const component = this;
		socket.on('connection', function(){
			//console.log('connected to server');
			socket.emit('playbackStart', component.state.queryId);
		});

		socket.on('playbackStep', function(result){
			console.log("received playbackstep to display: ", result);
			this.setState({
				
			})
		});
	}

	render(){
		return(
			<div className="visualizeArea">
				{
					this.state.loading ?
						<Loader />
						:
						<DirectRender 
							ref={this.directRender}
							loading={this.state.loading}
							query={this.state.currentQuery}
							rawContent={this.state.rawContent}
							instanceId={this.state.instanceId}
							updateHomePageQuery={this.updateHomePageQuery}
							stepArea={global.stepArea}
						/>
				}
			</div>
		);
	}
}

export default VisualizeArea;