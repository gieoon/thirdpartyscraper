import React from 'react';
import './dataPage.css';
import * as DB from '../API/DB.js';
import D3Component from './d3Component';

class DataPage extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			data: {},
			query: {},
			unsubscribeDBListener: {}
		};
		console.log("state set");
	}
	//data is loaded from StepArea.js
	componentDidMount(){
		global.stepArea.current.setAsDataPage();
	}

	render(){
		const component = this;
		console.log("data page rendering");

		return(
			<div className="dataArea g-relative">
				<D3Component 
					ref={global.dataPage}
					data={this.state.data || {}} />
			</div>
		);
	}

}

export default DataPage;