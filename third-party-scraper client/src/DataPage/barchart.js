import React from 'react';
import Responsive from './responsiveChart.jsx';

import {
    select,
    scaleBand,
    scaleLinear,
} from 'd3';





class BarChart extends React.Component {
    constructor() {
        super();
        this.draw = this.draw.bind(this);
    }

    componentDidMount() {
        this.draw();
    }

    draw() {
        const node = select(this.node);
        const {
            width: w,
            height: h,
            data,
        } = this.props;

        const xscale = scaleBand();
        xscale.domain(data.map(d => d.x));
        xscale.padding(0.2);
        xscale.range([0, w]);

        const yscale = scaleLinear();
        yscale.domain([0, 100]);
        yscale.range([0, h]);
        const upd = node.selectAll('rect').data(data);
        upd.enter()
            .append('rect')
            .merge(upd)
            .attr('x', d => xscale(d.x))
            .attr('y', d => h - yscale(d.y))
            .attr('width', xscale.bandwidth())
            .attr('height', d => yscale(d.y))
            .attr('fill', 'black');
    }

    componentDidUpdate() {
        this.draw();
    }


    render() {
        //make svg stretch fully in width and height of parent node
        return (
            <svg
                style={{ width: '100%', height: '100%' }}
                ref={node => {
                    this.node = node;
                }}
            >
            </svg>
        );
    }
}

export default Responsive(BarChart);
