import React from 'react';
import './linechart.css';
import Responsive from './responsiveChart.jsx';
import {
	select, scaleBand, scaleLinear
} from 'd3';
import * as d3 from 'd3';
import moment from 'moment';
//https://momentjs.com/

class LineChart extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			resizeChartWidth: props.resizeChartWidth,
			maxX: props.maxX,
			maxY: props.maxY,
			n: props.n,
			data: props.data,
			width: 0
		};
		this.draw = this.draw.bind(this);
		//console.log('data: ', this.props.data);
	}

	componentDidMount(){
		
		this.draw();
	}

	componentDidUpdate(){
		//console.log("updated LINECHART: ", this.props.data)
		this.draw();
		
	}

	draw(){
		const component = this;
		
		//console.log('data: ', this.props.data);

		//remove previous nodes
		select(this.node).selectAll("svg").selectAll('path').remove();
		select(this.node).selectAll("svg").selectAll('g').remove();
		select(this.node).selectAll('circle').remove();
		const node = select(this.node);
		var {
			width: w, height: h, data, n, maxX, maxY, minX, minY
		} = this.props;
		//console.log("linechart data: ", data);
		//console.log("linechart: w, h: " + w + ' : ' + h);
		//console.log('minX: ', minX);
		//console.log("minY: ", minY);
		//console.log('maxX: ', maxX);
		//console.log("maxY: ", maxY);
		var margin = {
			top: 20, 
			right: 50, 
			bottom: 80, 
			left: 50 }
  		, width = window.innerWidth - margin.left - margin.right // Use the window's width 
  		, height = (window.innerHeight - margin.top - margin.bottom) * .8// Use the window's height
  		//w = width;
  		h = height;
		// console.log("linechart width, height: " + width + ' : ' + height);
		// if(this.state.width !== width){
		// 	this.setState({
		// 		width: width
		// 	});
		// }
		var datapoints = 21//this.props.n;
		var xScale = d3.scaleTime()
			.domain([minX, maxX]) //input  //can put n - 1 here
			.range([margin.left, w - margin.right]); //output
		var yScale = d3.scaleLinear()
			.domain([minY, maxY]) //input
			.nice()
			//.range([height, 0]);
			.range([h - margin.bottom, margin.top]); //output
		//var dataset = d3.range(datapoints).map(function(d) { return {"y": d3.randomUniform(component.props.maxY)() } })


		var line = d3.line()
		//
			.x(function(d, i){ return xScale(d.timestamp); }) //set the x values for line generator
			.y(function(d){ return yScale(d.data); }) //set the y values for line generator
			.curve(d3.curveMonotoneX); //apply smoothing to the line
			//.curve(d3.curveCatmullRom.alpha(0.5))

		var upd = node.selectAll('svg').data(data);
		var svg1 = upd.enter()
			.append('svg')
			//.merge(upd)
			.attr("width", width + margin.left + margin.right)
			.attr("height", h + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		

		var div = d3.select("body").append("div")
			.attr("class", "tooltip")
			.style("opacity", 0)

		//horizontal and vertical gridlines
		upd.append("g")
			.attr("class", "grid")
			.attr("transform", "translate(0," + (h - margin.bottom) + ")")
			.call(this.make_x_gridlines(xScale)
				.tickSize((-h + (margin.top + (margin.bottom / 1.5))))
				.tickFormat(""))	

		upd.append("g")
			.attr("class", "grid")
			.attr("transform", "translate(" + margin.left + ",0)")
			.call(this.make_y_gridlines(yScale)
				.tickSize((-w + margin.left + (margin.right / 1.5)))
				.tickFormat(""))
		
		//axes
		var g1 = upd.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + (h - margin.bottom) + ")")
			.call(d3.axisBottom(xScale)); //create an axis component with d3.axisBottom

		var g2 = upd.append("g")
			.attr("class", "y axis")
			.attr("transform", "translate(" + margin.left + ",0)")
			.call(d3.axisLeft(yScale)); //create a y scale componet with d3.axis left	

		//line
		var path = upd.append("path")
			.datum(data)
			.attr("class", "line")
			.attr("d", line)
			//.attr("transform", "translate(" + margin.left + "," + margin.bottom +")");
		
		var dot = upd.selectAll("dot")
			.data(data)
			.enter().append("circle")
			.attr("class", "dot")
			.attr("cx", function(d, i){ return xScale(d.timestamp) }) //use i for index number
			.attr("cy", function(d){ return yScale(d.data) })
			.attr("r", 5)
			.on("mouseover", function(a, b, c){
				//console.log("mouseover: ", a, b, c)
				div.transition()
					.duration(200)
					.style("opacity", 1)
				div	
					.html(a.precursor + a.data.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] + "<br/>" + moment(a.timestamp).format('h:mm:ss a'))		
					.style("left", (d3.event.pageX - 30) + 'px')
					.style("top", (d3.event.pageY - 56) + 'px')
					.style("display", 'block')
				//console.log("div: ", document.getElementsByClassName('tooltip')[0]);
				d3.select(this).attr("r", 7.5)//.style("fill", "red");
			})
			.on("mouseout", function(){
				//console.log("mouseout!!")
				//upd.attr("class", "")
				d3.select(this).attr("r", 5)//.style("fill", "#fff8ee");
				div 
					.style("opacity", 0)
			})
	}

	// gridlines in x axis function
	make_x_gridlines = (xScale) => {		
	    return d3.axisBottom(xScale)
	        .ticks(5)
	}

	// gridlines in y axis function
	make_y_gridlines = (yScale) => {		
	    return d3.axisLeft(yScale)
	        .ticks(5)
	}


	render(){
		return(
			<svg id="d3svg"
				style={{width: '100%', height: '65vh'}}
				//style={{width: this.state.width, height: '70vh'}}
				//style={{width: '100%', height: '100%'}}
				ref={node => { 
					this.node = node;
				}}
			>
			</svg>
		);
	}
}

export default Responsive(LineChart);

