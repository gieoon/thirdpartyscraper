//import rd3 from 'react-d3-library';
//import node from 'd3file';
import React from 'react';
import * as d3 from 'd3';
import BarChart from './barchart';
import LineChartContainer from './linechartContainer';
import { Loader } from '../Loader/loader.js';
//const RD3Component = rd3.Component;
//var node = document.createElement('div');
class D3Component extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: props.data || {},
            query: { data: {}, queryId: 0 },
            queryName: '',
            loading: false
        };
    }

    componentDidUpdate(p1, p2){
        //console.log('1: ', p1);
        //console.log('2:', p2);
        //this.processData();
    }

    createRunIdsFromQuery = () => {
        //console.log("query: ", this.state.query);
        //console.log('queryName: ', this.state.queryName); 
        if(this.state.query !== undefined){
            const component = this;
            var queryData = {};
            const queryId = this.state.query.queryId;
            //console.log("queryId: ", queryId);
            const runIds = Object.keys(this.state.query.data || {});
            //console.log("runIds: ", runIds);
            if(runIds.length > 0){
                runIds.map(function(runId){ 
                    queryData[runId] = component.state.query.data[runId];  
                });
            }
            this.setState({
                queryData: queryData
            }, () => {
                //console.log('queryData: ', this.state.queryData);
            });     
        }
        
    }

    render() {
        console.log("this.state.query: ", this.state.query);
        return (
            <div style={{ height: '80%' }} >
            {
                this.state.loading ?
                <Loader />
                :
                <div>
                { 
                    this.state.query !== undefined && Object.keys(this.state.query.data).length === 0 ?
                    <div className="noDataText">
                        <h3><b>No Data</b></h3>
                        <span>Data that your queries have collected will be displayed here</span>
                    </div>
                    :            
                    <div>
                        <div className="dataPageTitle">
                            <h3>{this.state.queryName || 'New Query Title'}</h3>
                        </div>
                        <div className="chartContainer">
                            <LineChartContainer data={this.state.queryData || {}}/>
                        </div>
                    </div>
                }
                </div>
            }        
            </div>
        );
    }
};

/*

                            {
                                this.state.data.map((queryData, index) => (
                                    <div key={index}>
                                        {
                                            queryData.queryId
                                        }
                                    </div>
                                ))
                            }


*/

//<BarChart data={data} />
//<RD3Component data={this.state.d3} />


export default D3Component;

        /*
        const data = [
            { x: 2012, y: 50 },
            { x: 2013, y: 30 },
            { x: 2014, y: 80 },
            { x: 2015, y: 20 },
            { x: 2016, y: 55 },
            { x: 2017, y: 83 },
        ];
        */