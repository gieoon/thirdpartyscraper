import React from 'react';
import LineChart from './linechart';


//receives a query to render. 
//this class DOES NOT care about which query is selected, but only cares about drawing linechart of the query it is given 
class LineChartContainer extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			query: this.props.query,//receive query to render
			selectedStepNo: 0, //defaults to first queried step
			//object of arrays of stepNo objects 
			/*
			{[{stepNo: 1, data: $3.99, timestamp: 12345},{...},{...}], [{...},{...},{...}]}
			*/
			stepNos: {},
			runsArray: [],
        	maxX: 0,
        	maxY: 0,
        	minX: 0,
        	minY: 0,
        	stepNoVarianceKeys: [],
        	queryHasNoReadData: false,
        	currentStepData: {}
		}
	}

	componentDidMount(){
		this.loadDefaultStepNo();
	}

	componentDidUpdate(prevProps, prevState){
		//console.log("prevProps: ", prevProps.data);
		//console.log("state.currentStepData: ", this.props.data);
		//console.log('update? : ', prevProps.data == this.props.data);
		//console.log('prevState: ', prevState);
		//if(prevProps.currentStepData !== undefined && prevState.currentStepData !== null){
			//console.log("LS UPDATED");
	     	//console.log('prevState:', Object.keys(prevState.currentStepData).length === 0);
			if(prevProps.data != this.props.data){ //shallow comparison of object, and not memory location
				//console.log("WILL UPDATE");
				this.processData();
			}
			
		//}
		//this.processData();
	}

	//get the first step numebr that has readdata.
	//if none exists, do not display this query at all.
	loadDefaultStepNo = () => {

	}

	handleChange = (e) => {
		console.log('updated stepNo select e: ', e.target.value);
		this.setState({
			selectedStepNo: e.target.value
		});
		this.processData();
	}

	processData = () => {
		//console.log("Processing data: ", this.props.data);
		var lineChartMaxX, lineChartMaxY, lineChartMinX, lineChartMinY;
        const component = this;
        var queryId = component.props.data.queryId;
        //console.log('component.props.data: ', component.props.data);
        //var runs = Object.keys(component.props.data[key].data);
        var runs = Object.keys(component.props.data);
        //console.log("runs: ", runs);

        const runsArray = [], values = [], timestamps = [], stepNoVariance = {}, allSteps = {};

        runs.map(function(runId){
            //console.log("runId: ", runId)
            var obj = component.props.data[runId];
            //console.log("obj: ", obj);
            const stepNos = {};
            //unwrap values
            for(var index in obj.values){
            	//console.log('index: ', index);
                var data = obj.values[index];
                let tempData = {
                	precursor: '$', //TODO user sets this based on text or integers.
                	data: parseFloat(data.value.toString().replace(/\$/,''), 10),
                	stepNo: parseFloat(data.stepNo, 10),
                	timestamp: parseFloat(data.timestamp, 10)
                };
                //push into array based on stepNo as key.
                if(allSteps[tempData.stepNo] === undefined){
                	allSteps[tempData.stepNo] = [];
                }
                allSteps[tempData.stepNo].push(tempData);

                stepNos[tempData.stepNo] = tempData;
                timestamps.push(tempData.timestamp);
                values.push(tempData.data);
                //track the indexes that are filled with unique key hashmap.
                stepNoVariance[data.stepNo] = true;
            }  

            runsArray.push({
            	actualFireDate: obj.actualFireDate,
        	    executionDuration: obj.executionDuration,
            	fireDisparity: obj.fireDisparity,
            	jobId: obj.jobId,
            	scheduledFireDate: obj.scheduledFireDate,
            	stepNos: stepNos             
            });

        });
        //console.log('runsArray: ', runsArray);
        //create arrays of timestamps and values
       	if(values.length > 0 && timestamps.length > 0){ 
	        //get maximum Y value and pass to graph
	        lineChartMaxY = values.reduce(function(a,b){
	            //var a = component.state.stepNos[a];
	            //var b = component.state.stepNos[b];
	            //console.log('a: ', a);
	            return Math.max(a > b) ? a : b
	        });
	        lineChartMaxX = timestamps.reduce(function(a, b){
	            //var a = component.state.stepNos[a];
	            //var b = component.state.stepNos[b];
	            return Math.max(a > b) ? a : b
	        });
	        lineChartMinX = Math.min(...timestamps.map(d => d));
	        lineChartMinY = Math.min(...values.map(d => d));

	        // console.log("lineChartMaxX is: ", lineChartMaxX);
	        // console.log("lineChartMaxY is: ", lineChartMaxY);
	        // console.log("lineChartMinY: ", lineChartMinY);
	        // console.log("lineChartMinX: ", lineChartMinX);

        }
    		
        const stepNoVarianceKeys = Object.keys(stepNoVariance);
        //console.log("stepNoVarianceKeys: ", stepNoVarianceKeys);
		//console.log("this.state.selectedStepNo: ", this.state.selectedStepNo)
		//if(stepNoVarianceKeys.length > 0){
	        const currentStepData = allSteps[stepNoVarianceKeys[0]];
	        //console.log("allSteps: ", allSteps);
	        //console.log('current step data: ', currentStepData);
	        //console.log('this.state.currentStepData: ', this.state.currentStepData);
	        //set default to first index
	        //false condition to stop rendering overflow.
	        //if(this.state.currentStepData !== currentStepData){
		        this.setState({
		        	selectedStepNo: this.state.selectedStepNo || stepNoVarianceKeys[0],
		        	maxX: lineChartMaxX,
		        	maxY: lineChartMaxY,
		        	minX: lineChartMinX,
		        	minY: lineChartMinY,
		        	stepNoVarianceKeys: stepNoVarianceKeys,
		        	runsArray: runsArray,
		        	currentStepData: currentStepData,
		        	queryHasNoReadData: stepNoVarianceKeys.length === 0
		        }, () => {
		        	//console.log("stepNovairance: ", stepNoVarianceKeys);	
		        	//console.log('updated state: ', this.state);	
		        });
		    //}
		//}
	}

	render(){

		const component = this;
		//console.log("queryHasNoReadData: ", this.state.queryHasNoReadData);
		return(
			<div>
			{
				this.state.queryHasNoReadData ?
				<div>
					<p>Your query does not have any fields to collect</p>
				</div>
				:
				<div>
					<div className="g-flex g-justify-space-between">
						<p style={{marginLeft: '4rem', fontSize: '1.1rem'}}>Line Chart</p>
						<div className="stepNo-select">
							<select onChange={this.handleChange.bind(this)}>
								{							
									this.state.stepNoVarianceKeys.length > 0 &&
									this.state.stepNoVarianceKeys.map((stepNo, index) => (
										<option key={index} value={component.state.stepNoVarianceKeys[index]}>
											{component.state.stepNoVarianceKeys[index]}
										</option>
									))
								}
							</select>
						</div>
					</div>
		            <LineChart 
		                data={component.state.currentStepData || {}} 
		                n={Object.keys(component.state.runsArray).length} 
		                maxX={this.state.maxX || 0} 
		                maxY={this.state.maxY|| 0} 
		                minX={this.state.minX || 0} 
		                minY ={this.state.minY || 0} 
		                resizeChartWidth={this.props.resizeChartWidth}
		            />
	           	</div>
	        }
            </div>	
		);
	}
}

export default LineChartContainer;
