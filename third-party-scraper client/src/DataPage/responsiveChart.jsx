import React from 'react';

// hoc that wraps a component inside another component with lifecycle
// events
function Responsive(Component) {
    return class extends React.Component {
        constructor() {
            super();
            this.state = { width: undefined, height: undefined };
            this.resize = this.resize.bind(this);
        }

        componentDidMount() {
            window.addEventListener('resize', this.resize);
            this.resize();
            
            //resize wheneever parent div changes size when steparea increases and decreases
            var ro = new ResizeObserver( entries => {
                for(let entry of entries){
                    const cr = entry.contentRect;
                    //console.log('Element: ', entry.target);
                    //document.getelementbyId('d3svg')
                }
                //console.log("div resize detected, re-rendering");
                this.resize();
            });
            //console.log("RESIZE!!!: ", document.getElementsByClassName('dataArea')[0]);
            ro.observe(document.getElementsByClassName('dataArea')[0]);
        }

        resize() {
            const node = this.node;
            if(this.node !== null){
                const bounds = node.getBoundingClientRect();
                const width = bounds.width;
                const height = bounds.height * 0.8;
                //console.log("BOUNDS: WIDTH: ", width);
                this.setState({ width, height });
            }
        }

        componentDidUpdate(p1, p2){
            // const node = this.node;
            // const bounds = node.getBoundingClientRect();
            // const width = bounds.width;

            // console.log("window width: ", window.innerWidth- 100);
            // console.log('state.width: ', this.state.width);
            // console.log('bounding width: ', width);
            // console.log('p2: ', p2);
            // if(this.state.width !== window.innerWidth- 100){
            //     this.setState({
            //         width: window.innerWidth- 100
            //     })
            // }
        }

        componentWillMount() {
            //remove any listeners that are already attached
            window.removeEventListener('resize', this.resize);
        }

        componentWillUnmount(){
            //console.log("RESPONSIVE CHART UNMOUNTED");
            window.removeEventListener('resize', this.resize);
        }

        render() {
            const { width, height } = this.state;
            const { props } = this;
            //console.log("width: ", this.state.width);
            //console.log("height: ", this.state.height);
            return (
                <div
                    style={{ width: '100%', height: '100%' }}
                    ref={node => this.node = node}
                >
                    {
                        width && <Component
                            width={width}
                            height={height}
                            {...props}
                        />
                    }
                </div>
            );
        }
    }
}

export default Responsive;