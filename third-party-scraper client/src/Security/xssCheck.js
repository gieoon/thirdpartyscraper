
/*
	Validate external script tags by running them through cross site scripting security libraries.
	This is because dangerouslySetInnerHTML does not run script tags for security reasons.
	However, we can opt to run these tags, but will have to add extra validation to ensure safety.
	Remember, safety first at all time!
*/

//Use DOMPurify