import React, { Component } from 'react';
//import logo from './logo.svg';
import './g.css';
import './resources.css';
import Routes from './router';
import StepArea from './StepArea/stepArea';
import Header from './Header/header.js';
import { SocketProvider } from './Context';
import {BrowserRouter } from 'react-router-dom';
//import * as DB from './API/DB';

//socket
import io from 'socket.io-client';
const socket = '';//io(process.env.REACT_SERVER_PORT);
const StepAreaContext = React.createContext();

<<<<<<< HEAD
global.SERVER_PORT = "localhost:5000"; //"data2u-nodejs-server.herokuapp.com"
=======
global.SERVER_PORT = "http://localhost:5000" //"https://data2u-nodejs-server.herokuapp.com" //"http://localhost:5000";
>>>>>>> 8117893de30f4abef6bbf67843e685894591834f
global.PROXY_SERVER_PORT = "https://third-party-scraper-cors.herokuapp.com/";
global.stepArea = {};

//LOG VERSION NUMBER
global.VERSION = 'v0.1';
console.log(global.VERSION);

class App extends Component {
  constructor(props){
    super(props);
    this.state = {

    }
    global.stepArea = React.createRef();
    global.explorePageQueriesArea = React.createRef();
    global.dataPage = React.createRef();
    global.homePage = React.createRef();
    console.log("React Version : ", React.version);
  }

  componentDidMount(){
    
  }

  render() {
    //can also just use a ref to access it.
    return (
      <div className="App">
        <SocketProvider value={socket}>
          <BrowserRouter>
            <StepAreaContext.Provider value={global.stepArea}>  
              <div className="g-flex g-flex-row g-flex-1 g-flex-1-1-100">        
                <StepArea ref={global.stepArea} />         
                <div id="wrapper">
                  <div id="dangerous-script-container"></div>
                  <Header />
                  <Routes />
                </div>
              </div>
            </StepAreaContext.Provider>
          </BrowserRouter>
        </SocketProvider>
      </div>
    );
  }
}
//<Header />

export default App;
export { StepAreaContext }

