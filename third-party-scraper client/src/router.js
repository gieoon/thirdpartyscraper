import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import HomePage from './HomePage/homePage';
import ProfilePage from './User/profilePage';
import ResultsPage from './Results/resultsPage';
import ExplorePage from './Explore/explorePage';
import ContactPage from './Contact/contactPage';
import DataPage from './DataPage/dataPage';
import QueriesPage from './QueriesPage/queriesPage';
import Visualize from './Visualize/visualizePage';
import VMComponent from './vm-build/vmComponent2.js'; //'./vm-build/vmComponent.js';

//defines what component each page points to.
//if a url is structured in this way, deliver this component.
//if a url has /table/followed by something else, treat those parts as what is defined here.
const Routes = () => (
	<main style={{overflowY: 'auto', height: '93vh'}}>
			<Switch>
				<PrivateRoute exact path='/profile' component={ProfilePage}/>
				<Route exact path='/table/:roomId/:size/:adCount/:alias/:title' component={ResultsPage}/>
				<Route exact path='/explore' component={ExplorePage}/>
				<Route exact path='/queries' component={QueriesPage}/>
				<Route exact path='/contact' component={ContactPage}/>
				<Route exact path='/feedback' component={ContactPage}/>
				<Route exact path='/create' component = {HomePage}  />
				<Route exact path='/data' component={DataPage} />
				<Route exact path='/visualize' component={Visualize} />
				<Route path='/vm' component={VMComponent} />
				<RedirectRouteFromBlank exact path='/' />
				<RedirectRouteFromBlank path={''} />
			</Switch>
	</main>
);

///redirects
const authentication = {
	isAuthenticated: false,
	authenticate(cb) {
		this.isAuthenticated= true;
	},
	signout(cb) {
		this.isAuthenticated = false;
	} 
}

function PrivateRoute({ component: Component, ...rest }){
	return (
		<Route
			{...rest}
			render={props =>
				authentication.isAuthenticated ? (
					<Component {...props} />
				) : (
					<Redirect
						to={{
							pathname: "/browse",
							state: { from: props.location }
						}}
					/>
				)
			}
		/>	
	);
}

function RedirectRouteFromBlank({ component: Component, ...rest }){
	return(
		<Route {...rest} render= { props =>
			<Redirect to={{pathname: '/queries', state: { from: props.location} }}/> } />
	);
}

export default Routes;

