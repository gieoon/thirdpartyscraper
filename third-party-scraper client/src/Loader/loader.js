import React from 'react';
import './loader.css';

export function Loader(){
	return (
		<div id="loading-top-margin">
			<div className="loading-position"> 
				<div className="sk-folding-cube">
					<div className="sk-cube1 sk-cube"></div>
					<div className="sk-cube2 sk-cube"></div>
					<div className="sk-cube4 sk-cube"></div>
					<div className="sk-cube3 sk-cube"></div>
				</div>
			</div>
		</div>
	);
}