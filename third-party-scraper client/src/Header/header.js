import React, { Component } from 'react';
import Links from './links';
import RightLinks from './links-right';
//import Login from '../Authentication/login.js';
import './header.css';
import logo from '../logo.svg';
import { Route } from 'react-router-dom';
//import { Route, withRouter } from 'react-router-dom';

class Header extends Component {
	constructor(props){
		super(props);
		this.state = {

		}
	}

	render(){
		return(
			<div>
				<nav className="topNav g-flex-shrink-0 g-header-background-color ">
					<div className="topNav-inner g-justify-space-between g-header-background-color g-align-items-stretch g-flex g-flex-nowrap g-full-height">
						<div className="g-align-items-center g-flex-shrink-0 g-inline-flex">
							<Route render={({ history }) => (	
								<span></span>		
								
							)} />
						</div>
						
						<Links />
						<RightLinks />
						
					</div>			
				</nav>
				<nav className="topNav g-flex-shrink-0 g-relative g-header-background-color g-not-visible ">			
				</nav>
			
			</div>
		);
	}
}
//<Login />
//<img src={logo} onClick={() => { history.push('/') }} className="App-logo g-pd-1 g-inline-flex" alt="logo" />
export default Header;