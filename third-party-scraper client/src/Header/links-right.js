import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './links.css';

class RightLinks extends Component {
	render() {
		return (
			<div className="right-links g-justify-end g-flex-shrink-0 g-flex-grow-1 g-align-items-stretch g-flex g-flex-nowrap">
				<div className="g-align-items-stretch g-flex g-flex-nowrap g-flex-shrink-0">
					<Link className="g-nav-link g-link" to='/contact'>Contact</Link>
				</div>
			</div>
		);
	}
}
export default RightLinks;

//<Link className="g-nav-link g-link" to='/docs'>DEV Docs (connect this to sphinx docs)</Link>
//<Link className="g-nav-link g-link" to='/pricing'>Pricing</Link>
//<Link className="g-nav-link g-link" to='/help'>Help</Link>
//<Link className="g-nav-link g-link" to='/feedback'>Feedback</Link>
//	<Link className="g-nav-link g-link" to='/login'>Login</Link>					
//<Link className="g-nav-link g-link" to='/bug-report'>Report a Bug</Link>