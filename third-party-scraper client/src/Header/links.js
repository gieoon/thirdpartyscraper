import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './links.css';

class Links extends Component {
	render() {
		return (
			<div className="g-flex-shrink-0 g-flex-grow-1 g-align-items-stretch g-flex g-flex-nowrap">
				<div className="g-align-items-stretch g-flex g-flex-nowrap g-flex-shrink-0">
					
					<Link className="g-nav-link g-link" to='/queries'>Queries</Link>
					<Link className="g-nav-link g-link" to='/create'>Create Query</Link>
				</div>
			</div>
		);
	}
}
export default Links;

//<Link className="g-nav-link g-link" to='/visualize'>Visualize</Link>
// <Link className="g-nav-link g-link" to='/data'>Data</Link>
// <Link className="g-nav-link g-link" to='/explore'>Explore</Link>