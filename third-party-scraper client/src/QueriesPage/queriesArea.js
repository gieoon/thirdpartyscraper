import React from 'react';

import { Loader } from '../Loader/loader';
import * as DB from '../API/DB.js';

class QueriesArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			queries: [],
			loading: true,
			selectedQuery: {}
		};
	}

	componentDidMount(){
		//DB.initDB();
	    const component = this;
	    DB.getQueriesByUser('guest').then(results => {
	    	//console.log('got results: ', results);
	    	this.setState({
	    		queries: results,
	    		loading: false,
	    		selectedQuery: results[0]  //default to show the first query
	    	});
	    });
	}

	componentDidUpdate(prevProps, prevState){
		//console.log('prevProps: ', prevProps);
		//console.log('prevState: ', prevState.selectedQuery);
		//console.log("this.state.selectedQuery: ", this.state.selectedQuery);
		if(this.state.selectedQuery !== prevState.selectedQuery){
			//console.log("UPDATING!!");
			//this.forceUpdate();
		}
	}

	dropdownQuery = (e) => {
		//console.log('e.target.className: ', e.target.className);
		//e.target.style.height = '20vh';
		//console.log('e.target.className.match(/showing/): ', e.target.className.match(/showing/));
		// if(e.target.className.match(/queryCard/) !== null){
		// 	if(e.target.className.match(/showing/) !== null){
		// 		e.target.className = 'queryCard';
		// 	}
		// 	else e.target.className += ' showing ';
		// }
	}

	startPlaybackQuery = (query) => {
		console.log('clicked to playback query: ', query);
		this.props.setPlaybackQuery(query);
	}

	deleteQuery = async (queryId) => {
		//console.log('clicked delete query: ', queryId);
		//console.log("this.state.queries: ", this.state.queries)
		this.setState({ loading: true});
		const component = this;
		var queryIndex = 0;
		var count = 0;
		for(const query of this.state.queries){
			//console.log("query.queryId: ", query.queryId);
			//console.log("queryId: ", queryId);	
			if(query.queryId === queryId){
				queryIndex = count;
				break;
			}
			++count;
		}
		queryIndex = Math.max(0, --queryIndex);

		
		//console.log("query index: ", queryIndex);
		//get the index of the query
		await DB.deleteQuery('guest', queryId); 
		DB.getQueriesByUser('guest').then(results => {
	    	//console.log('got results: ', results);
	    	this.setState({
	    		queries: results,
	    		loading: false,
	    		//selectedQuery: results[queryIndex]
	    	}, () => {
	    		console.log("query to render: ", results[queryIndex]);
	    		if(results[queryIndex] !== undefined){
	    			var element = document.getElementsByClassName('exploreQueryCard')[queryIndex];
	    			global.stepArea.current.explorePageStepAreaRef.current.selectQuery(element, results[queryIndex]);
	    		}
	    	});
	    	
	    });
	}

	clickQuery = (value) => {
		//console.log('clicked on query');
	}

	timestampToDate = (timestamp) => {
		  var a = new Date(parseInt( timestamp, 10 ));
		  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		  var year = a.getFullYear();
		  var month = months[a.getMonth()];
		  var date = a.getDate();
		  var hour = a.getHours();
		  var min = a.getMinutes();
		  var sec = a.getSeconds();
		  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
		  return time;
	}

	render(){
		//console.log('rendering state.queries; ', this.state.selectedQuery);

		return(
			<div className="queriesArea">
			{
				this.state.loading ?
					<Loader />
					: 
					<div>
						<div>
							<h3 className="queryDetails">Query Details</h3>
							{ 
								Object.keys(this.state.queries).length > 0 && Object.keys(this.state.selectedQuery || {}).length > 0 &&
								<div className="queryTitle-icon-container g-text-right">	
									{/*<span className="" onClick={() => this.startPlaybackQuery(this.state.selectedQuery)}>
										<i className="material-icons visualize">
										pageview
										</i>
									</span>*/}
									<span className="" onClick={() => this.deleteQuery(this.state.selectedQuery.queryId)}>
										<i className="material-icons visualize">
										delete
										</i>
									</span>
								</div>
							}
						</div>
					{
						Object.keys(this.state.queries).length > 0 ?
							
						
								
									
							<div key={this.state.selectedQuery.queryId} className="queryCard showing" onClick={this.dropdownQuery.bind(this)}>
								

								<div className="queryTitle g-no-click" onClick={() => this.clickQuery(this.state.selectedQuery.queryId)}>									
									{
										//<span>Query: {index + 1}</span>
									}
									<div className="queryTitle-inner">
										
										<div className="queryTitle-inner-inner">
											<div>Title: </div>
											<div><b>{this.state.selectedQuery.name || 'New Query Title'}</b></div>
										</div>	
										<div className="queryTitle-inner-inner">
											<div>Created on: </div>
											<div><b>{this.timestampToDate(this.state.selectedQuery.createdOn) || 'N/A'}</b></div>
										</div>
									{	/*
										<div className="queryTitle-inner-inner">
											<div>Last run: </div>
											<div><b>26/03/2019{}</b></div>
										</div>

										<div className="queryTitle-inner-inner">
											<div>Completed runs: </div>
											<div><b>
												 23/24 
												 -
												 <span style={{color:'red'}} className="failedRun"> 1 Failed</span>{}
											</b></div>
										</div>
										<div className="queryTitle-inner-inner">
											<div>Average Runtime duration: </div>
											<div><b>5.9 seconds{}</b></div>
										</div>
										<div className="queryTitle-inner-inner">
											<div>Average Runs per month: </div>
											<div><b>3.24 {}</b></div>
										</div>		
										<div className="queryTitle-inner-inner">
											<div>Average Rows collected per run: </div>
											<div><b><a>2306{}</a></b></div>
										</div>	
										<div className="queryTitle-inner-inner">
											<div>Total Collected Rows: </div>
											<div><b><a>52254{}</a></b></div>
										</div>	
										<div className="queryTitle-inner-inner">
											<div>Runs remaining: </div>
											<div><b>12{}</b></div>
										</div>	
										*/
									}
									</div>
									{
										//<span>{obj.queryId}</span>
									}
									
									{/*
										<span className="highlightable" >
										<i className="fa fa-caret-down"></i>
									</span>*/
									}
									
								</div>
								<hr/>
								<h4 className="querySteps">Steps ({Object.keys(this.state.selectedQuery.steps || {}).length || 0})</h4>
								<table>
									<tbody>
									{
										<tr className="tableHeader">
											<td>Step No</td>
											<td>Action</td>
											<td>On</td>
											<td>Text to Type</td>
										</tr>
									}
									{
										Object.keys(this.state.selectedQuery.steps || {}).map((key) => (
											<tr key={key} className="query">
												<td>{key}</td>
												<td>{this.state.selectedQuery.steps[key].action}</td>
												<td>{this.state.selectedQuery.steps[key].selector}</td>
												<td>{this.state.selectedQuery.steps[key].input ? this.state.selectedQuery.steps[key].input : ''}</td>
											</tr>
										))
									}
									</tbody>
								</table>
							</div>
						:
							<div className="noDataText">
								<span>The queries you have created will be displayed here</span>
								
							</div>	
					}
				</div>
			}		
			</div>
		)
	}
}

export default QueriesArea;