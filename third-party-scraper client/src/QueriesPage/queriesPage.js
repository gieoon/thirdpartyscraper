import React, { Component } from 'react';

import QueriesArea from './queriesArea';
import D3Component from '../DataPage/d3Component';

class QueriesPage extends Component {
	constructor(props){
		super(props);
		this.state = {
			queries: {},
			showingQuery: false,
			playingBack: true,
			queryToShow: {}
		}
	}

	componentDidMount(){
		global.stepArea.current.setAsExplorePage();
	}

	setPlaybackQuery = (query) => {
		const component = this;
		this.setState({
			showingQuery: true,
			queryToShow: query			
		}, () => {
			global.stepArea.current.loadPlaybackQuery(query);
		});

	}

	render(){
		return(
			<div>
				<QueriesArea 
					ref={global.explorePageQueriesArea} 
					setShowingQuery={this.setShowingQuery}
					setPlaybackQuery={this.setPlaybackQuery}/>

				<div className="dataArea g-relative">
					<D3Component 
						ref={global.dataPage}
						data={this.state.data || {}} />
				</div>
			</div>
		);
	}
}

export default QueriesPage;