import React from 'react';
//import { Loader } from '../HomePage/loader.js';
//directly renders the frame
//testing this out without using iframe. 
//trying to render manually
import './directRender.css';
import './iframeExtender.js';

import WNDPC from '../WebpageNotDisplayingProperlyDialog/wndpc.js';

class DirectRender extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			loading: props.loading,
			query: this.props.query,
			rawContent: this.props.rawContent,
			elementBackgroundColorBeforeHighlight: '',
			elementHighlighted: {},
			elementSelected: {},
			selected: false,
			elementsToRead: [],
			updateClicker: this.props.updateClicker,
			isPlayingBack: this.props.isPlayingBack,
			selectorsToHighlight: this.props.selectorsToHighlight,
			showingInput: this.props.showingInput
		}
	}

	componentDidMount(){
		//console.log("Mounted, adding scripts");
		this.props.addScripts();
	}

	componentDidUpdate (prevProps){
		const component = this;
		//console.log('prevProps: ', prevProps.rawContent);
		//console.log("current props: ", this.props.rawContent);
		if(this.props.loading !== prevProps.loading){
			this.setState({
				loading: this.props.loading,
			}, () => {
				//console.log('query: ', this.state.query)
				//this.forceUpdate();
			});
		}
		if(this.props.query.title !== prevProps.query.title){// || this.props.query.query !== prevProps.query){
			this.setState ({
				query: this.props.query
			}, () =>{
				this.forceUpdate();
			});
		}

		if(this.props.rawContent !== prevProps.rawContent){
			//console.log('rawcontent: ', this.state.rawContent); 
			this.setState({
				rawContent: this.props.rawContent//this.renderDom()
			}, () => {
				console.log('updated state!!!!');
				component.props.updateClicker();
			});
		}

		if(this.props.scripts !== prevProps.scripts){
			this.setState({
				scripts: this.props.scripts
			})
		}
	}

	setElementToGetDataOf = (element) => {
		console.log("highlighting element to get data of: ", element);
		this.state.elementsToRead.push(element);

		//var element = document.elementFromPoint(e.clientX, e.clientY);
		element.style.backgroundColor = 'rgba(244, 131, 39, .5)';
		console.log('updated element; ', element);
	}

	selectHighlightedElement = (e) => {
		const component = this;
		this.setState({
			selected: true,
			elementSelected: this.state.elementHighlighted
		}, () => {
			component.updateHighlight(e);
		});
	}

	selectNewHighlightedElement = (e) => {
		//console.log('e: ', e);
		this.updateHighlight(e);
	}

	onMouseMove = (e) => {
		//console.log('updating color, x: ' + e.clientX + ' y: ' + e.clientY);
		//change color of element at this point
		if(!this.state.selected && !this.props.isPlayingBack){
			//revert previous element's color
			this.updateHighlight(e);
		}
	}

	updateHighlight = (e)=> {
		const component = this;
		var element = document.elementFromPoint(e.clientX, e.clientY);
		if(this.state.elementHighlighted.style){
			//console.log('this.state.elementHighlighted.style: ', this.state.elementHighlighted.style);
			//if(this.state.elementHighlighted.style.backgroundColor !== 'rgba(244, 131, 39, .5)'){
				//console.log('elementHighlighted.style.backgroundColor: ', this.state.elementHighlighted.style.backgroundColor);
				this.state.elementHighlighted.style.backgroundColor = this.state.elementBackgroundColorBeforeHighlight;
			//}
		}
			
		//set new elmeent's color	
		this.setState ({
			elementBackgroundColorBeforeHighlight: element.style.backgroundColor,
			elementHighlighted: element
		}, () => {
			//make sure this is not an element that data is being read from, that is a permanent orange highlight
			//console.log('element.style.backgroundColor: ', element.style.backgroundColor);
			element.style.backgroundColor = component.state.selected ? 
				'rgba(255,255,0,.5)' //selected color
				:
				'rgba(255,255,0,.3)' //unselected color	
		});
	}


	render(){
			// setTimeout(() => {
			// 	var elems = document.body.getElementsByTagName("*");
			// 	var pageWrapper = document.getElementById('page-wrapper');
			// 	if(pageWrapper){
			// 		for (var i=0;i<elems.length;i++) {
			// 			if(pageWrapper.contains(elems[i])){
			// 			    if (window.getComputedStyle(elems[i],null).getPropertyValue('position') === 'fixed') {
			// 			        console.log(elems[i]);
			// 			        elems[i].style.position = 'absolute';
			// 			    }
			// 			}
			// 		}
			// 	}
			// 	//this causes recursion..
			// 	//this.props.setLoading(false);
			// },750);
			//setTimeout(() => {
				//console.log("selectorsToHighlight: ", this.state.selectorsToHighlight)
				this.state.selectorsToHighlight.map((selector) => (
					//console.log("selector in document: ", document.querySelector(selector))
					document.querySelector(selector).className += ' readdata-highlighted '
				));
			//}, 1000)
			//console.log('element: ', document.getElementById('rn'));
			//console.log("rendering: ", this.state.rawContent);
		return(
			<div className="g-relative dangerous">	
				<iframe src="http://127.0.0.1:5000" style={{width: '100%', height:'450px'}}></iframe>
				<div onMouseMove={this.onMouseMove.bind(this)} 
				 	 dangerouslySetInnerHTML={{__html: this.state.rawContent}} />
			 	{
			 		!this.props.showingInput && <WNDPC />
			 	}
			</div>
		);
	}

}
//http://0.0.0.0:8000/
//<iframe id="renderer" is="x-frame-bypass" src="https://www.youtube.com" height="100%" width="100%"></iframe>
//https://news.ycombinator.com/
//https://www.youtube.com/watch?v=XNJTVLFotr0&t=297s
/*
<div onMouseMove={this.onMouseMove.bind(this)} 
					 	dangerouslySetInnerHTML={{__html: this.state.rawContent}} />
*/
///<html> wrapped around dangerous
///<div dangerouslySetInnerHTML={{__html: this.state.rawContent}}>
export default DirectRender;
/*
{ this.state.loading && <Loader /> }
{this.state.rawContent.firstChild.innerHTML}
				<object className="browser-frame" type="text/html" data={this.state.rawContent}>
				</object>
<embed src="http://stackoverflow.com" width="200" height="200" />
					//    console.log('elemt[i].style.zIndex: ', elems[i].style.zIndex);
			//    if(elems[i].style.zIndex){
			//    	if(elems[i].style.zIndex > 10){
			//    		elems[i].style.zIndex = 1;
		 	//    	} 
			// }		
*/
	/// renderDom = () => {
	// 	const doc = new DOMParser().parseFromString(this.props.rawContent, "text/xml");
	// 	const htmlSections = doc.childNodes[0].childNodes;
	// 	const sections = Object.keys(htmlSections).map((key, i) => {
	// 		let el = htmlSections[key];
	// 		let contents = [<p key={i}>{el.innerHTML}</p>];
	// 		return <div key={i}>{contents}</div>
	// 	}); 
	// 	return sections;
	// }


