const DB = require('../DB/firestore');
const userModel = require('../User/userModels');
var scraper = require('../scraper/scraper');
//deserializes and constructs queries from JSON object

//starts a new scraper instance
exports.startPlayback = async function(socket, query, instanceId, stepNo){
	const queryHolder = constructQuery(query);
	console.log("starting playback");
	let steps = Object.keys(queryHolder.queries);	
	const isPlayback = true;
	var results = {};
	if(global.puppeteerPages[instanceId] === undefined){
		results = await scraper.run(queryHolder.queries[stepNo].selector, function(){}, isPlayback, false, false);
		queryHolder.instanceId = results.instanceId;
	}
	else {
		//move existing scraper instance to a new url as a GO TO URL action.
		//first action if instanceId is already defined shoudl be gotoURL
		results = await scraper.goToUrl(queryHolder.queries[stepNo].selector, global.puppeteerPages[instanceId], instanceId, isPlayback);
		queryHolder.instanceId = instanceId;
	}
	//set instanceId of this scraper
	
	socket.emit('playbackStep', { results: results, stepNo: stepNo, instanceId: results.instanceId });
}

//should preload next step beforehand
exports.startStep = async function(socket, query, instanceId, stepNo){
	const isPlayback = true;
	const queryHolder = constructQuery(query);
	console.log("starting step: ", queryHolder);
	var results = await scraper.action({
		selectorFields:  queryHolder.queries[stepNo].selector,
		instanceId: instanceId,
		text: queryHolder.queries[stepNo].inputText || undefined
	}, queryHolder.queries[stepNo].action, isPlayback);
	
	socket.emit('playbackStep', { results: results, stepNo: stepNo, instanceId: instanceId });
}

constructQuery = function(query){
	//unwrap object
	/*
	{ 
		steps:
   		{ 
   			'1': 
   			{ 
   				action: 'url', selector: 'https://www.google.com' 
   			} 
   		},
  		queryId: '17b30ae0-3728-11e9-82d3-aba65efb0749' 
  	}
    */
    const queryHolder = new userModel.QueryHolder(query.queryId);
    //console.log("query is: ", query); 
    for(step in query.steps){
    	console.log('query step: ', query.steps[step]);
    	var newQuery = new userModel.Query(
    		0,
    		step,
    		query.steps[step].action,
    		query.steps[step].selector,
    		query.steps[step].input || ''
    	);
    	//key is step number
    	queryHolder.queries[step] = newQuery;
    }
    return queryHolder;
}

