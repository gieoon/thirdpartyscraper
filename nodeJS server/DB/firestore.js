
const admin = require('firebase-admin');

const USER_COLLECTION = 'Users';
const QUERY_COLLECTION = "Queries";
const DATA_COLLECTION = "Data";
const QUERY_ID_FIELD = "QueryId";

let serviceAccount = require("../kn1oc-e779ecbeef.json");
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://thirdpartyscraper.firebaseio.com"
});

const db = admin.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
db.settings(settings);
//by removing this link, can call other functions..
module.exports.db = db;

var rootRef = db.collection(USER_COLLECTION);

exports.writeUserToDB = function(user){
	var user = rootRef.doc(user.userId).set({
		username: user.username,
		password: user.password,
		settings: user.settings
	}).then(() => {
		console.log('WROTE USER TO DB SUCCESSFULLY');
	}).catch(err => {
		console.log("ERROR WRITING USER TO DB: ", err);
	});
}

exports.writeQueryToDB = function(userId, query){
}

exports.writeQueryStepToDB = function(userId, query){
	console.log("writing to db: " + userId);
	console.dir('query: ' + query);
	var newQuery = {
		action: query.action,
		selector: query.selector,
	}
	if(query.action === 'type'){
		newQuery.input = query.inputText;
	}
	else if(query.action === 'readdata'){
		newQuery.fieldName = query.fieldName;
		newQuery.fieldType = query.fieldType;
	}
	var queryIdRef = rootRef.doc(userId)
					   .collection(QUERY_COLLECTION)
					   .doc(query.id.toString());
   	queryIdRef.collection('Steps')
					   .doc(query.step.toString())
					   .set(newQuery)
					   .then(function(){
							console.log('WROTE STEP TO DB SUCCESSFULLY');
						}).catch(err => {
							console.log("ERROR WRITING STEP TO DB: ", err);
						});
	//write this to a field, otherwise document will not appear...lol
	queryIdRef.set({
		DELETED_FLAG: 0,
		createdOn: Date.now().toString(),
		//PARTIAL_FLAG: 1
	}, {merge: true});
}

//playbackid = Date.now();
//runs the query and stores the results for each step separately.
exports.savePlaybackQueryStep = function(userId, playbackId, query){
	var step = rootRef.doc(userId)
					  .collection(QUERY_COLLECTION)
					  .doc(query.id.toString())
					  .collection('Data')
					  .doc(playbackId)
					  .collection('Step Data')
					  .doc(query.step.toString())
					  .set({
		timeTaken: query.timeTaken,
		data: query.data			  	
	}).then(function(){
		console.log('SAVED QUERY DATA SUCCESSFULLY');
	}).catch(err => {
		console.log("ERROR WRITING STEP DATA TO DB: ", err);
	});
}

exports.getPlaybackQuery = function(userId, query){

}

exports.getQueryFromDB = function(userId, queryId, ){
	console.log("getting data from DB");
	var query = {};
	var steps = rootRef.doc(userId.toString())
					   .collection(QUERY_COLLECTION)
					   .doc(queryId.toString())
					   .collection('Steps')
					   .get()
   					   .then(snapshot => {
					   		
					   		snapshot.forEach(doc => {
					   			console.log('doc.id: ', doc.id, ' => ', doc.data());
					   			query[doc.id] = doc.data();
					   		});
		console.log("READ FROM DATABASE SUCCESSFULLY");
		return query;
   	}).catch(err => {
   		console.log("ERROR READING QUERY FROM DATABASE");
   	});
}

exports.readQueriesFromDB = function(){

}


exports.saveDataToDB = function(userId, queryId, dataTimestamp, stepNo, value){
	console.log("saved queried value");
	var query = {};
	var steps = rootRef.doc(userId.toString())
					   .collection(DATA_COLLECTION)
					   .doc(queryId.toString())
					   .collection(dataTimestamp.toString())
					   .doc(stepNo.toString())
					   .set({
					   		Data: value
					   });
}


// var rootRef = db.collection('UoA').doc('data');

// var user = rootRef.set({
// 	title: 'where',
// 	last: 'lovelace',
// 	born: 2016
// });

// const firestore = new Firestore();
// const settings = {timestampsInSnapshots: true};
// firestore.settings(settings);


  // // Old:
  // const date = snapshot.get('created_at');
  // // New:
  // const timestamp = snapshot.get('created_at');
  // const date = timestamp.toDate();

