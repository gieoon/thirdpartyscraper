

//CONSTANTS & GLOBAL VARIABLES
global.puppeteerPages = {}; //mapping unique instanceId to puppeteer session.
global.instanceToSocket = {}; //key = socketId value = socket

global.getPageFromInstanceId = function(instanceId){
	//console.log('found: ', global.instanceToSocket);
	return global.puppeteerPages[global.instanceToSocket[instanceId]];
}

global.DELETE_FLAG = '{}TO-DELETE{}';

exports.MAX_PUPPETEER_INSTANCES = 5; //number of parallel instances to run concurrently 

//Express Endpoints
exports.SEARCH_URL = 'searchUrl';


//essential tag types with navigation or data
exports.TAG_TYPES = [
	'title', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
	'p', 'input', 'textarea', 'center',
	'button', 'select', 'option', 'label', 
	'iframe',
	'a', 'link', 'nav', 'ul', 'ol', 'li', 'dt', 'dl', 'dd',
	'table', 'caption', 'th', 'tr', 'td', 'thead'
];

//maybe the user needs more? provide extra for advanced usage
exports.EXTRA_TAG_TYPES = [
	'div', 'tbody', 'tfoot', 'col', 'colgroup', 'form', 'canvas', 'img', 'map', 'area',
	'fieldset', 'legend', 'datalist', 'output', 'figcaption', 'picture', 'svg', 'audio', 'video',
	'address', 'blockquote', 'cite', 'code', 'em', 'i', 'q', 'small', 'sub', 'sup', 'time'
];

//returns a string on how to query
//create priority queries, if exact text lookup fails, then fall back on containsText lookup, 
//if text lookup fails, lookup via tag, or via id and class.
//lookup via anything possible.
//last resort is to lookup via children. Getting the html structure and converting it to JSON, then analysing which child, and programmatically go to that child.
//very difficult to program, but will be very useful.
//each object is executed in order of priority
exports.getXPath = {
	textSearch: [
		["//", "[text()='", "']"], //isText
		["//", "[contains(text(),'", "')]"], //containsText
		["//", "[text()=", "]"] //isTextAny
	],
	idSearch: [
		//isId: ['#', ]
		["//", "[@id='", "']"] //isId: 
	],
	dataSearch: [
		["//", "[@", "='", "']"]
	]
}

//if no tags or selectors are present identify the element, use hierarchy.
//in this case, client has passed over a string of data from parent to the exact child
//take this as an array and parse through it to get to the element
exports.hierarchicalSearch = function(pathArray){
	//each element in pathArray may have an attribute to query.
	string = ["//", ""];
	//how to xpath by hierarchy?
}

//when the only way to identify the element is by its child number
exports.childNumber = function(selector, childNumber){

}
//javascript implementation of an enum
//prioritization of selectors for search
//the smaller the value, the earlier it is checked
exports.getSelectorPriority = Object.freeze({
	"id": 1,
	"text": 2,
	"data": 3, //data-target
	"ariaLabel": 4, //aria-label
	"label": 5,
	"class": 6,
	"href": 7,
	"hierarchy": 8,
	"tag": 9
});


exports.SCROLL_AMT = 40
