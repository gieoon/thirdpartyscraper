// organizes users into categories

//Instance Id is for the running instance of Puppeteer.

exports.User = function(username){
	this.userId = userId;
	this.username = username;
	this.password = 'p';
	//object lookup with 
	this.queries = {};
	this.settings = {};
}

//each query has steps.
//their parent is the collection, identified by instanceId
//each user can havemany queries with multiple instanceId's.

exports.QueryHolder = function(queryId){
	this.id = queryId;
	this.instanceId = 0;
	this.queries = {};
}

//the query to run through when repeating steps
exports.Query = function(id, step, action, cssSelector, inputText, fieldName, fieldType){
	this.id = id;
	this.step = step;
	this.action = action;
	this.inputText = inputText;
	this.fieldName = fieldName;
	this.fieldType = fieldType;
	this.selector = cssSelector;
	this.timeTaken = 0; //for time elapsed when playing back
	this.data = ''; //for data obtained at this step
}


//creates a singular instance of this function in memory without instantiating a new one each time this object is created
exports.Query.prototype.createQuery = function(){

}




