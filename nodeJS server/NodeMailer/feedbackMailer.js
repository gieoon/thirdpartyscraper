"use strict";

const nodemailer = require('nodemailer');
const config = require('../config.json');
//console.log("config is: ", config);
exports.send = async function(request){
	console.log('request: ', request.params);
	// Generate test SMTP service from ethereal.email
	// Only needed if you don't have a real mail account for testing
	let testAccount = await nodemailer.createTestAccount();

	//create reusable transporter object using the default SMTP transport
	//for testing with ethereal
	let transporter = nodemailer.createTransport({
		host: "smtp.ethereal.email",
		port: 587,
		secure: false, //true for 465, false for other ports
		auth: {
			user: testAccount.user, //generate ethereal user
			pass: testAccount.pass //generate ethereal password
		}
	});
	//actual gmail
	let gmailTransporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: config.email.address,
			pass: config.email.password
		}
	})

	//const defaultEmails = "jun.a.kagaya@gmail.com"; //"jun.a.kagaya@gmail.com, jun.a.kagaya@gmail.com"
	var date = new Date(Date.now()).toString()
	//send mail with defined transport object
	let info = await gmailTransporter.sendMail({
		//from: 'feedbackemail@AP',
		to: config.email.address,//defaultEmails,
		subject: 'Feedback for AB received',
		text: 'Received on: ' + date,
		html: '<p>Received on: ' + date + '</p>' +
			'<p>Name: ' + request.params.name + '</p>'+
			'<p>Email: ' + request.params.email + '</p>'+
			'<p>Message: ' + request.params.msg + '</p>'+
			'<h4>Make sure to keep your users happy and the trolls quiet!!</h4>'
	});

	console.log("Message Sent: %s", info.messageId);
	console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
	//real  IMAP/POP3 
	return;
}


