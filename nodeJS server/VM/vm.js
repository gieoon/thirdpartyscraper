"use strict";

const fs = require('fs');
const v86Starter = require('v86').V86Starter;

function readFile(path){
	return new Uint8Array(fs.readFileSync(path)).buffer;
}

const bios = readFile(__dirname + "/files/seabios.bin");
const linux = readFile(__dirname + "/files/linux.iso");
const clear = readFile(__dirname + "/files/clear-30210-kvm.img");

console.log('bios: ', bios);
console.log('linux: ', linux);
console.log('clear: ', clear);

process.stdin.setRawMode(true);
process.stdin.resume();
process.stdin.setEncoding("utf8");

const boot_start = Date.now();
var booted = false;

console.log('Now booting, stand by...');

var emulator = new v86Starter({
	bios: { buffer: bios},
	//cdrom: { buffer: linux },
	cdrom: { buffer: clear },
    autostart: true
});


emulator.add_listener("serial0-output-char", function(chr)
{
    if(!booted)
    {
        var now = Date.now();
        console.log("Took %dms to boot", now - boot_start);
        booted = true;
    }

    process.stdout.write(chr);
});

process.stdin.on("data", function(c)
{
    if(c === "\u0003")
    {
        // ctrl c
        emulator.stop();
        process.stdin.pause();
    }
    else
    {
        emulator.serial0_send(c);
    }
});
