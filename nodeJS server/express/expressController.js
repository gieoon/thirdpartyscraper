var Constants = require('../config/constants');
var Query = require('.././models/query');
var db = require('.././DB/firestore');
var scraper = require('.././scraper/scraper');
var testScraper = require('.././scraper/testScraper');
var mailer = require('../NodeMailer/feedbackMailer.js');
//var rooms = require('../server').rooms;
//https://stackoverflow.com/questions/35749288/separate-file-for-routes-in-express/35749744
//-----------------------------------------------------------------------------
//EXPRESS ENDPOINTS
//for request and response communication model
//Express http server gives request response model from client to server.


module.exports = {
	searchUrl : async (req, res) => {
		console.log("============= Received Request for searchUrl =======================");
		this.addHeaders(res);
		//const randomAdInt = Math.floor(Math.random() * (Constants.MAX_ROOM_SIZE));
		//let newQuery = Query.createQuery();
		var results = await scraper.run (req.params.url, function() {console.log('close called');}, req.params.instanceId, req.params.isDebug, req.params.isPlayback);
		res.send({ results: results });
		console.log("=========== RESPONDED WITH SEARCHURL DATA ===============");
	},
	nextUrl : async (req, res) => {
		console.log("============= Received Request for nextUrl =======================");
		this.addHeaders(res);
		var results = await scraper.nextUrl (req.params.url, function() { console.log('close called');}, req.params.instanceId);
		res.send({ results: results });
		console.log("============= RESPONDED WITH NEXTURL =======================");
	},
	action: async (req, res)=> {
		console.log("================== Received Request for Action =========================");
		//console.log('req.params: ', req.params);
		//console.log('req.baseUrl: ', req.baseUrl);
		
		var path = req.path.replace(/\//,''); //remove first slash from URL
		//console.log('req.path: ', path);
		var actionTypeIndex = path.match(/\//).index;  //first slash without space (?<![ ])\/
		//console.log('actionTypeIndex: ', actionTypeIndex);
		path = path.substring(0, actionTypeIndex);
		path = path.trim();
		//console.log('action type: ', path);
		this.addHeaders(res);
		//let newQuery = Query.createQuery();
		//console.log('req: ', req);
		const actionResults = await scraper.action(req.params, path, false);
		res.send({ results: actionResults });
		
		console.log("=========== RESPONDED WITH ACTION ===============");
	},

	//testing API endpoint
	helloWorld: async (req, res) => {
		console.log('hello world called!!');
		res.send('hello world');
	},

	// directRender: async (req, res) => {
	// 	console.log("==================== RENDER TEST =======================")
	// 	const allHTML = await testScraper.directRender();
	// 	res.send({
	// 		results: allHTML
	// 	});
	// }

	sendFeedbackMail: async (req, res) => {
		console.log("Sending Nodemailer mail");
		mailer.send(req).then(function(){
			res.send('Sent Successfully');
		}).catch(function(err){
			res.send('Sending Error: ' + err);
		})
		
	}
}

exports.addHeaders = function(res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");	
}



/*
WHEN IN SERVER.JS JUST USE THIS
express.get('/getNewTable', (req, res) => {
	// newRoom.size = Constants.MAX_ROOM_SIZE; //default to this size.
	
	const randomAdInt = Math.floor(Math.random() * (Constants.MAX_ROOM_SIZE));
	let newRoom = Room.createRoom();
	newRoom.adCount = Constants.AD_COUNT;
	newRoom.funTitle = req.query.title;
	newRoom.adVideoNo = randomAdInt;
	newRoom.size = Constants.MAX_ROOM_SIZE;
	newRoom.response = res;
	//console.log("req.query.title: ", req.query.title);
	rooms[newRoom.uniqueId] = newRoom;
	//send this table to client.
	res.send({ table: newRoom });

});

express.get('/getCurrentTables', (req, res) => {
	//give 27 tables, 9 rows of 3.
	console.log("sending tables: ", rooms);
	res.send({ rooms: rooms });
});

express.get('/', function(req, res){
	//tell connection to render this on the page.
	res.send('Hello World!');
	db.start();
	//TODO temporarily disabled
	//db.queryTest();
});
*/
