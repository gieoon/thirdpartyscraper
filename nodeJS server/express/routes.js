
var express = require('express');
var expressController = require('./expressController');

var router = express.Router();

router.route('/searchUrl/:url/:isDebug?/:isMobile?').get(expressController.searchUrl);

//selector fields with delimiters
router.route('/click/:instanceId/:selectorFields/').get(expressController.action);
router.route('/nexturl/:instanceId/:url').get(expressController.nextUrl);
router.route('/readdata/:instanceId/:selectorFields/:fieldName/:fieldType').get(expressController.action);
router.route('/type/:instanceId/:selectorFields/:text').get(expressController.action);
//router.route('/back/:instanceId/:selectorFields').get(expressController.action);
router.route('/').get(expressController.helloWorld);
router.route('/directRender/:instanceId/:url/:isDebug?/:isPlayback?').get(expressController.searchUrl);
router.route('/mailfeedback/:name/:email/:msg').get(expressController.sendFeedbackMail);

module.exports = router;


