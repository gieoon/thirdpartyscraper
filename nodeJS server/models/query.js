//holds query formats and saves them for the user. SO the user can come back next time and resume the same query.
exports.createQuery = function() {
	return {
		uuid: generateUUID(),
		title: '',
		//arbitrary number of steps each holding an action and results.
		steps: []
	}
}

exports.createClass = () => {
	return {
		className: '',
		target: '',
		innerHTML: ''	
	}
};

function generateUUID(){
	var d = Date.now();
	if(typeof performance !== 'undefined' && typeof performance.now === 'function'){
		d += performance.now(); //use high precision timer if available
	}
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c === 'x' ? r : ( r & 0x3 | 0x8)).toString(16);
	});
}


