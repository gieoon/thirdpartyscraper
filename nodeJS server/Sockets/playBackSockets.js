const playback = require('../Playback/playback');

exports = module.exports = function(io){
	io.sockets.on('connection', function(socket){
		
		//if(!socket.connected){
			socket.emit('connectedPlayback');
		//}
		
		socket.on('disconnect', function(){
			//console.log('user disconnected');
		});
		socket.on('requestPlaybackConnection', function(){
			console.log('a user connected for playback');
			socket.on('playbackStart', function(obj){
				console.log('received request for playback: ', obj.query);
				//check if instanceId exists, if it exists, use the existing one.
				//otherwise start new one. 
				//conserve browser pages
				if(global.puppeteerPages[obj.instanceId] !== undefined){
					console.log("duplicate instanceId found");
				}
				playback.startPlayback(socket, obj.query, obj.instanceId, 1); //start from 1st step by default
			});

			socket.on('getPlaybackStep', function(obj){
				console.log('received request for query: ', obj);
				playback.startStep(socket, obj.query, obj.instanceId, obj.stepNo);
			});
		})
	});
}
