const scraper = require('../scraper/scraper.js');
const uuidv1 = require('uuid/v1');

exports = module.exports = function(io){
	io.sockets.on('connection', function(socket){

		socket.on('requestDirectRenderConnection', function(){
			console.log('a user connected for direct render updates: ', socket.id);
			const instanceId = uuidv1(); // '45745c60-7b1a-11e8-9c9c-2d42b21b1a3e'
			scraper.init(socket, instanceId, false);
			
			socket.emit('connectedDirectRender', instanceId);
			//socket.on('instanceId', function(instanceId){
				//scraper.addSocket(socket, instanceId);
				
				//TODO make another object lookup from socket to instance, and delete from both simultaneously
			//})
			socket.on('disconnect', function(){
				console.log('user disconnected from direct updates: ', socket.id);
				scraper.close(socket.id);
				
			});

			socket.on('scrollPls', function(instanceId) {
				console.log("scrolling a little...", instanceId);
				scraper.scrollPage(instanceId);
			});
		});

	});
}

//new information was recieved from XHR requests and responses, so send the new HTML to the client
exports.updateClientDirectRender = function(socket, html){
	//console.log('emmitting new html');
	socket.emit('updateDirectRender', html);
}

exports.updateClientJavascript = function(socket, script){
	socket.emit('updateDirectRenderJavascript', script);
}

//TODO check how to add polymer objects from Polymer Project to HTML
exports.appendClientDirectRender = function(socket, document){
	socket.emit('appendClientDirectRender', document);
}

// Web Sockets passing CSS stylesheets
exports.updateStyleSheet = function(socket, stylesheet){
	console.log('SENT STYLESHEET');
	socket.emit('updateStyleSheet', stylesheet);
}