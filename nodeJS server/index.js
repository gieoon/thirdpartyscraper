//libraries
const express = require('express')();
var path = require("path");
var cors = require('cors');
const fs = require('fs');
var server = require('http').Server(express);
var io = require('socket.io')(server); //initialize with express server
var routes = require('./express/routes');
const socketAuth = require('socketio-auth');//https://hackernoon.com/enforcing-a-single-web-socket-connection-per-user-with-node-js-socket-io-and-redis-65f9eb57f66a

//const vm = require('./VM/vm.js');

//require external socket files
require('./Sockets/playBackSockets.js')(io);
require('./Sockets/directRenderSockets.js')(io);
//local modules
var Constants = require('./config/constants');
const db = require('./DB/firestore');
//local constants
const port = process.env.PORT || 5000;

//local variables
const scrapers = {}; //each scraper is mapped key: user uuid, value: scraper object. 

exports.io = io;

//========================================================================================================
server.listen(port, () => console.log('Listening on port ', { port }));
express.use(cors()); //to get past CORS issues.
express.use('/api', routes);

//express.get('/:id', (req, res) => {
express.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, './vm-build/v86/index.html'))
  //res.sendFile(path.join(__dirname, './v86.css'));
});
express.get('/vgabios', (req, res) => {
	res.sendFile(path.join(__dirname, './vm-build/v86/bios/vgabios.bin'));
});
express.get('/seabios', (req, res) => {
	res.sendFile(path.join(__dirname, './vm-build/v86/bios/seabios.bin'));
});
express.get('/freedos', (req, res) => {
	//res.sendFile(path.join(__dirname, './vm-build/v86/images/freedos722.img'));
	//res.sendFile(path.join(__dirname, './vm-build/v86/images/clear-30210-kvm.img'));
	res.sendFile(path.join(__dirname, './vm-build/v86/images/winXP.iso'));
	//res.sendFile(path.join(__dirname, './vm-build/v86/images/winXP_Disk1.img'));
	//res.sendFile(path.join(__dirname, './vm-build/v86/images/Win8.1.iso'));
	//res.sendFile(path.join(__dirname, './vm-build/v86/images/Win10.iso'));
});

express.get('/1', (req, res) => {
	res.sendFile(path.join(__dirname, './vm-build/v86/images/winXP_Disk2.img'));
});

express.get('/freedos2', (req, res) => {
	res.sendFile
})

express.get('/dashboard', (req, res) => {
	res.sendFile(path.join(__dirname, './Dashboard/index.html'));
});

express.get('/inituser', (req, res) => {
	console.log('initializing guest user');
	db.writeUserToDB({
		userId: 'guest',
		username: 'sample',
		password: 'p',
		settings: {}
	});
});

// io.on('connection', function(socket){
// 	console.log('a user connected');
	
// });


// http.listen(3000, function(){
// 	console.log('listening on *:3000');
// });
