
const puppeteer = require('puppeteer');

(async function run(){
	console.log("LAUNCHING TEST");
	const browser = await puppeteer.launch({
		headless: false
	});
	const page = await browser.newPage();
	await page.goto('https://www.youtube.com/watch?v=XNJTVLFotr0&t=297s'); 

	var element = await page.waitForSelector('#SearchBar__Keywords');
	await element.type('machine learning');

	seekBtn = await page.waitForSelector('.\_2lGAHdH');
	await Promise.all([
		page.waitForNavigation({ timeout: 10000, waitUntil: ['domcontentloaded','load','networkidle0','networkidle2'] }),				
		seekBtn.click()
	]);
	console.log("done");
})();
