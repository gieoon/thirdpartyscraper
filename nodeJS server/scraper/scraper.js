/*
    Starts a new asynchronous scraper instance.
    Each time a user presses UI submit, a new instance will be created and an old one destroyed.
*/
//http://testing-ground.scraping.pro/

//local imports
const db = require('../DB/firestore');
const dbInstance = require('../DB/firestore').db;
const constants = require('../config/constants.js');
const search = require('./search');
const filter = require('./filter');
const testScraper = require('./testScraper');
const cssSelectorSearch = require('./cssSelectorSearch');
const userModels = require('../User/userModels');
const directRenderSocket = require('../Sockets/directRenderSockets.js');
//library imports
const puppeteer = require('puppeteer');
const jsdom = require('jsdom'); //for virtual DOM creation for manipulating HTML strings.
const { JSDOM } = jsdom;
var util = require('util');
//const { PendingXHR } = require('pending-xhr-puppeteer');

//const html2json = require('html2json').html2json;

const startTime = Date.now();
var currentStep = 0;

//add a socket associated with puppeteer scraper instanceId
exports.addSocket = function(socket, instanceId){
     
}

//intializes and runs the scraper
exports.init = async function(socket, instanceId, isMobile) {

    global.puppeteerPages[socket.id] = {
        page: {},
        browser: {},
        socket: socket,
        scrollTrigger: false,
        promises: []
    };
    global.puppeteerPages[socket.id].promises.push(new Promise(async (resolve, reject) => {
    
        global.instanceToSocket[instanceId] = socket.id;

        //optional parameter for headlesss
        var isMobile = isMobile || false;
        //console.log('debug: ', debug);
        //console.log('debug == false: ', debug === false);
    	console.log("================== LAUNCHING QUERY ==================="); 

        const width = isMobile ? 500 : 1000; //1400
        const height = 1600;
        
        // Prevent Puppeteer from showing the "Chrome is being controlled by automated test
        // software" prompt, but otherwise use Puppeteer's default args.
        const args = await puppeteer.defaultArgs().filter(flag => flag !== '--enable-automation');
        const browser = await puppeteer.launch({
            headless: false,//true,//debug === false, //need to convert string to boolean...through comparison
            args: [
                `--window-size=${ width },${ height }`, 
                `--mute-audio=true`,
                //'--no-sandbox',
                //'--disable-setuid-sandbox',
            ],
            isMobile: isMobile,
        });

        const page = await browser.newPage();
        //const pendingXHR = new PendingXHR(page);
        //DEBUG set this here.
        currentStep = 0;

        global.puppeteerPages[socket.id].page = page;
        global.puppeteerPages[socket.id].browser = browser;

        // Adaptive?
        // await page.setUserAgent('Mozilla (Android) Mobile')
        await page.setViewport({ width, height });
        
        // global.puppeteerPages[socket.id].promises.push(function(){
        //     return Promise.resolve('done');
        // });
        resolve();
    }));

}

exports.run = async function(url, close, instanceId, isDebug, isPlayback){
    //console.log("promises: ", global.getPageFromInstanceId(instanceId).promises);
    await Promise.all(global.getPageFromInstanceId(instanceId).promises); //wait for browser page to load if it hasn't loaded yet
    
    var debug = isDebug || false;
    const page = global.getPageFromInstanceId(instanceId).page;
    
    const stylesheets = exports.startXHRListener(page, instanceId);
    //wait for init to have finished if it has not

    //global.getPageFromInstanceId(instanceId.promises.push(new Promise((resolve, reject) => resolve("run")));

    //close();
    return {
        styles: stylesheets, 
        instanceId: instanceId, 
        siteList: debug ? await testScraper.testGoToUrl(url, page) : await exports.goToUrl(url, page, instanceId, isPlayback, 'url')
    }
}

exports.nextUrl = async function(url, close, instanceId){
    const page = global.getPageFromInstanceId(instanceId).page;
    const stylesheets = exports.startXHRListener(page, instanceId);
    url = stripUrl(url);
     

    return {
        styles: stylesheets,
        instanceId: instanceId,
        siteList: await exports.goToUrl(url, page, instanceId, false, 'nexturl')  
    }
}


exports.startXHRListener = async function(page, instanceId){
    var styles = [];
    const MAX_XHR = 15, MAX_IMG = 15;
    
    const scripts = []
    const stackObj = 'stack';
    var xhrStack = [];
    var xhrCount = 0;
    var prevXhrCount = 5; //as long as this is not 0, to stop multiple repeated 0's from triggering.

    var imageStack = [];
    var imageCount = 0;
    var prevImageCount = 5;

    page.on('response', async response => {
        var res = response.request().resourceType();
        //console.log('response type: ', res);
        if(res !== 'image'){
            //console.log('resource detected: ', res);
        }

        const status = response.status();
        if ((status >= 300) && (status <= 399)) {
            console.log('Redirect from', response.url(), 'to', response.headers()['location'])
        }
        
        //if(response.request().resourceType === 'stylesheet' )console.log('resource is: ', response.request());
        if(response.request().resourceType() === 'stylesheet') {
            var styleString = await response.text();
            console.log('pushed stylestring');
            //styles.push(styleString);
            const socket = global.getPageFromInstanceId(instanceId).socket;
            directRenderSocket.updateStyleSheet(socket, styleString);
        }
        else if (response.request().resourceType() === 'document'){
            const doc = await response.text();
            //console.log("document found: ", doc);
            
            const socket = global.getPageFromInstanceId(instanceId).socket;
            directRenderSocket.appendClientDirectRender(socket, doc)
        }
        else if(response.request().resourceType() === 'image'){
            var image = await response.request();
            //console.log("resolved image: ", imageStack.length);
            imageStack.pop();
            if(imageStack.length <= 1 && imageStack.length !== prevImageCount){
                prevImageCount = imageStack.length;
                const allHTML = await getUpdatedPage(page);

                const socket = global.getPageFromInstanceId(instanceId).socket;
                //console.log("going to send with socket: ", socket);
                if(socket !== undefined){
                    //console.log("UPDATING SOCKET DIRECTRENDER FOR CLIENT");
                    //TODO directRenderSocket.updateClientDirectRender(socket, allHTML);
                }
                return {
                    styles: styles
                };
            }

        }
        // else if (response.request().resourceType() === 'script'){
        //     var script = await response;
        //     console.log('got script: ', script);
        //     //scripts.push(script);
        //     const socket = global.puppeteerPages[instanceId].directRenderSocket;
        //     directRenderSocket.updateClientJavascript(socket, script);
        // }

        // else if(response.request().resourceType() === 'document'){

        //     await response.request();
        //     console.log("response document: ");
        //     //console.log('document response: ', response);
        // }

        if(response.request().resourceType() === 'xhr'){
            await response.request();
            //console.log("xhr resolved");
            xhrStack.pop();

            //console.log("removing xhr requests length: ", xhrStack.length);
            //console.log('prevXhrCount: ', prevXhrCount);
            //if(xhrStack.length === 0 && xhrStack.length !== prevXhrCount){
                prevXhrCount = 0;
                
                const allHTML = await getUpdatedPage(page)//instead of just updating the XHR, am updating the whole HTML of the page...
                //console.log("socket to send to: ", global.puppeteerPages[instanceId].directRenderSocket);
                const socket = global.getPageFromInstanceId(instanceId).socket;
                //console.log("sending update to socket: ", socket);
                if(socket !== undefined){
                    directRenderSocket.updateClientDirectRender(socket, allHTML);
                }
                // return {
                //     styles: styles,
                //     //scripts: scripts
                // };
            //}
        }
    });

    //await page.setRequestInterceptionEnabled(true);

    page.on('request', async request => {   


        //some code here that adds this request to ...
        //a list and checks whether all list items have ...
        //been successfully completed!
        //console.log("interceptedRequest: ", interceptedRequest);
        //don't push anymore and cease updating if too many xhr requests
        if(request.resourceType() === 'xhr' && xhrCount <= MAX_XHR){
            //console.log("waiting xhr request");
            xhrStack.push(stackObj);
            prevXhrCount = xhrStack.length;
            xhrCount++;
            //console.log("adding xhr requests length: ", xhrStack.length);
            await request;
            //console.log("xhr request complete");
        }
        if(request.resourceType() === 'image'){// && imageCount <= MAX_IMG){
            //await page.setRequestInterception(true);
            //request.abort();
            // else {
            //     request.continue();
            // }
            // imageStack.push(stackObj);
            // //console.log("requested image: ", imageStack.length);
            // prevImageCount = imageStack.length;
            // imageCount++;
            // await request;
        }
        if(request.resourceType() === 'video'){
            //await page.setRequestInterception(true);
            //request.abort();
        }
    });

    // console.log("waiting all XHR to finish");
    // await pendingXHR.waitForAllXhrFinished();
    // console.log("All XHR finished!!");
    //console.log("styles.length: ", styles.length);
    if(xhrStack.length === 0){
        //console.log("returning, xhr complete");
        return {
            styles: styles,
            //scripts: scripts
            //images: images
        };
    }
}

exports.close = async function(socketId){
	console.log('------------------------------------------------------');
	console.log("CLOSING BROWSER INSTANCE: ", socketId);
	console.log("Duration elapsed: ", (Date.now() - startTime) % 1000 % 60);
	console.log('------------------------------------------------------');
    //console.log('global.puppeteerPages: ', global.puppeteerPages);
    //console.log('global.instanceToSocket: ', global.instanceToSocket);
    try{
        await global.puppeteerPages[socketId].browser.close();
        delete global.puppeteerPages[socketId];
    } catch(err){
        console.log("browser close error caught: ", err);
    };

    for(var instanceId in Object.keys(global.instanceToSocket)){
        if(global.instanceToSocket[instanceId] == socketId){
            delete global.instanceToSocket[instanceId];
            break;
        }
    }
}

exports.scrollPage = async function(instanceId){
    const page = global.getPageFromInstanceId(instanceId).page;
    console.log("scrolling whole page");
    await page.evaluate(() => {
        window.scrollBy(0,window.innerHeight); //window.innerHeight/ 10
    });
    global.getPageFromInstanceId(instanceId).scrollTrigger = true;

    
    let query = new userModels.Query(instanceId, ++currentStep, "scroll", '', null, null, null);
    db.writeQueryStepToDB('guest', query);  
}



exports.goToUrl = async function (url, page, instanceId, isPlayback, actionType) {
    //return after finding the url, so can prompt the user onto the next step
    //var url = 'http://www.' + url;
    url = stripUrl(url);
    try{
        try{
            url = 'https://www.' + url;
            console.log('url: ', url);
            await page.goto(url);  
        }catch(err){
            console.log('error going to url, redoing with "http:" ');
            url = stripUrl(url);
            url = 'http://www.' + url;
            console.log('url: ', url);
            await page.goto(url);
        }
        //await page.waitFor(5000); WOW
        return await collectPageDataToReturn(instanceId, page, url, actionType, url, undefined, isPlayback);
        //console.log('tagtypes: ', tagTypes);

    } 
    catch(err) {
        console.log('err: ', err);
        return { allHTML: '' }
    }
}

function stripUrl(url){
    url = url.replace(/{}/g,'/'); //re-translate delimiters
    url = url.replace(/\[]/g,'?');
    url = url.replace(/<>/g,'#');
    url = url.replace(/>>/g,'.')
    url = url.replace(/<</g,'\\')
    //cut out the front of the url to normalize it
    url = url.replace(/^https?:\/\//g, '');
    url = url.replace(/^www\./g, '');
    //console.log("URL: ", url);
    if(url.match(/\.co?m^|\.net^/gi) === null){
        //doesn't work on some websites that don't have an ending...
        //url = url + '.com' //TODO for now just add .com for easier testing
    }
    return url;
}

exports.action = async function(params, actionType, isPlayback){
    //TODO may need to decode hashes as well, as these are reserved over URL links.
    if(params.selectorFields){
        var cssSelector = params.selectorFields 
            .replace(/{}/gi,'/')
            .replace(/\[]/gi,'?')
            .replace(/<>/g,'#')
            .replace(/>>/g,'.')
            .replace(/<</g,'\\')
    }
    console.log('params: ',params);
    var selectorToHighlight = '';
    var page = getPageFromInstanceId(params.instanceId).page;
    cssSelector = await stripFrontUntilFound(cssSelector, page);

    //if scroll exists, then scroll
    // if(!isPlayback){
    //     const scrollTrigger = global.puppeteerPages[params.instanceId].scrollTrigger;
    //     if(scrollAmt > 0){
    //         console.log("saving scroll amt to DB: ", scrollAmt);
    //         ++currentStep;
    //         let query = new userModels.Query(params.instanceId, currentStep, 'scroll', scrollAmt, null, null, null);
    //         db.writeQueryStepToDB('guest', query);  
    //         global.puppeteerPages[params.instanceId].scrollAmt = 0;
    //     }
    // }
    // else if(isPlayback){
    //     //slowly scroll down
    //     var remainingScrollDist = max(remainingScrollDist - window.innerHeight, 0);//TODO get the scroll amount stored in DB
    //     while(remainingScrollDist !== 0){
    //         await page.evaluate(_ => {
    //             window.scrollBy(0, min(remainingScrollDist, window.innerHeight)); //window.innerHeight/ 10
    //         });
    //         await page.waitFor(3000); //give the page time to load
    //     }
    // }

    if(actionType === 'click' || actionType === 'type'){
        var text = '';
        if(params.text !== undefined){
            text = params.text;
        }
        page = await cssSelectorSearch.executeAction(page, params.instanceId, cssSelector, actionType, text);
        console.log('action complete');
    }
    else if(actionType === 'readdata'){
        page = await cssSelectorSearch.executeAction(page, params.instanceId, cssSelector, actionType);
        if(isPlayback) {
            console.log("adding cssselector");
            selectorToHighlight = cssSelector;
        }
        console.log('readdata complete');    
    }
    else if(actionType === 'back'){
        page = await cssSelectorSearch.executeAction(page, params.instanceId, cssSelector, actionType);
    }
    else if(actionType === 'scroll'){
        console.log("scrolling...");
        await page.evaluate(() => {
            window.scrollBy(0,window.innerHeight); //window.innerHeight/ 10
        });
        //wait for the page to load
        await page.waitFor(5000);
    }
    var url = await getCurrentUrl(page); 
    var siteList = await collectPageDataToReturn(params.instanceId, page, url, actionType, cssSelector, params.text, isPlayback, params.fieldName, params.fieldType);
    return {
        instanceId: params.instanceId,
        siteList: siteList,
        selectorToHighlight: selectorToHighlight
    }
}

async function stripFrontUntilFound(cssSelector, page){
    console.log('cssSelector: ', cssSelector);
    var s = await page.evaluate((selector) => {
        const elements = Array.from(document.querySelectorAll(selector));
        const links = elements.map(element => {
            return element.href;
        })
        return links;
    }, cssSelector)
    if(s.length){
        return cssSelector;
    }
    if(!s.length){
        //strip front and repeat
        cssSelector = cssSelector.replace(/^\S+\s?>?\s?/,''); //[a-zA-Z]+\s>?\s?
        return await stripFrontUntilFound(cssSelector, page);
    }
}

//siteList
async function collectPageDataToReturn(instanceId, page, url, actionType, cssSelector, inputText, isPlayback, fieldName, fieldType){
    const title = await page.title();
    console.log("title: " + title);
    ++currentStep;
    //const screenshot = await page.screenshot({ path: './test-data/' + Date.now().toString() + '-' + currentStep + '.png' });
    //console.log("=================== SCREENSHOT SAVED SUCCESSFULLY ======================");
    //let allHTML = await page.evaluate(() => document.body.innerHTML);
    //await page.waitForNavigation({waitUntil: 'networkidle2'});
    const allHTML = await page.evaluate(() => document.documentElement.outerHTML);
    //let allHTML = await page.$$('*');
    //let sanitizedContent = filter.sanitizePageContent(allHTML);
    //var tagTypes = await filter.findAllTagTypesInString(sanitizedContent);
    //tagTypes = tagTypes.filter(item => item.text !== global.DELETE_FLAG);
    
    //console.log('isPlayback: ', isPlayback);
    if(!isPlayback){
        //store in DB
        let query = new userModels.Query(instanceId, currentStep, actionType, cssSelector, inputText, fieldName, fieldType);
        db.writeQueryStepToDB('guest', query);    
    }
    
    return {
        url: url,
        allHTML: allHTML,
        step: currentStep,
        title: title,
        fieldName: fieldName,
        fieldType: fieldType
        //screenshot: screenshot,
        //tags: tagTypes,
        //sanitizedContent: sanitizedContent
    }
}

async function getUpdatedPage(page){
    const allHTML = await page.evaluate(() => document.documentElement.outerHTML);
    return allHTML;
}

async function getCurrentUrl(page){
    var url;
    try{
       url = await page.evaluate('location.href'); 
   } catch(err){
        console.log('ERROR execution context destroyed, check network loading: ');
        await page.waitForNavigation({ waitUntil: 'networkidle2' });
        url = await page.evaluate('location.href'); 
   }
    return await url;
}

//searches through selectors for the best one, then returns this.
async function searchForBest(page, selectors){
    for(var i = 0; i < selectors.length; i++){
        try{
            await page.selectors[i]
        }
        catch(err){}
    }
}

//performs an action as requested from client
//DEPRECATED IN FAVOUR OF CSS SELECTOR
// exports.action = async function(params, actionType){
//     var fields = params.selectorFields
//         .replace(/{}/gi,'/')
//         .replace(/\[]/gi,'?')
//         .split("|");
//     //console.log('params: ', fields);
//     var page = global.puppeteerPages[params.instanceId];
//     var selectors = await search.readRequestData(fields);
//     //console.log('selectors: ', selectors);
//     //call puppeteer to search based on these selectors
//     if(actionType === 'click'){
//         const stylesheets = startCSSListener(page);
//         page = await search.executeAction(page, selectors, actionType);
//         var url = await getCurrentUrl(page); 
//         var siteList = await collectPageDataToReturn(page, url);
//         return {
//             styles: stylesheets, 
//             instanceId: params.instanceId, 
//             siteList: siteList
//         }  
//     }
//     else if(actionType === 'readdata'){
//         console.log('reading data');
//         var innerData = await search.executeAction(page, selectors, actionType);
//         //assume user knows what they are doing, and url is not updated.
//         ++currentStep;
//         return {
//             instanceId: params.instanceId,
//             innerData: innerData,
//             step: currentStep
//         }
//     }
// }

            //if($item[0].name === 'input' && $item.attr('aria-label') !== undefined){
                //console.log('item: ', util.inspect($item));
                // console.log('item name: ', util.inspect($item[0].name));
                // console.log('class: ', $item.attr('class'));
                // console.log('label: ',  $item.attr('value'));
                // console.log('children: ', $item.children().length);
                // console.log('children().text(): ', $item.children().text());
                //console.log('=============================================='); 
            //}
            //text = getRelevantText($item);
            //var text = filterIrrelevantTags($item);
            //TODO, if child contains parent's text, then forfeit this, makeshift fix.

// function findAllTagTypesInString(content){
//     // console.log(html2json(content));
//     // return json;
// }



        /*this.filter(($item) => {
            if($item.text() === ''){
                return false;
            }
            return true;
        }).*/
// findAllTagTypesInString = (content) => {
//     var promise = htmlToJson.parse(content, {
//         'text': function($doc) {
//             return $doc.find('*').name();//.text();
//         }
//     }, (err) => {
//         console.log('err: ', err);
//     });

//     await promise.done(function(result) {
//         console.log("returning results: ", result);
//         return result;
//     });
// }

// function findAllTagTypesInString(content){
//     const dom = new JSDOM('<!DOCTYPE html><p>Hello world</p>');
//     var tag = dom.window.document.createElement("div");
//     tag.innerHTML = content;

//     var t = tag.getElementsByTagName("*");
//     var elements = [];
//     for (var i = 0; i < t.length; i++) {
//         var tName = t[i].tagName.toLowerCase();
//         elements.push({ tagType: tName, inner: t[i].innerHTML });
//     }
//     return elements;
// }

        //this works but duplicates text
        // let tags =  await page.$$('*');
        // const tagTypes = [];
        // for(let i = 0; i < tags.length; i++ ){
        //     const jsonText = await tags[i].getProperty('textContent'); //'innerText'
        //     const text = await jsonText.jsonValue();
        //     if(text.length > 0 )
        //     tagTypes.push(tags[i]); 
        // }

/*


    //console.log('allHTML: ', allHTML);
    //const allHTML = await page.evaluate(() => document.documentElement.outerHTML);
    //let allHTML = await page.$$('*');
    //let sanitizedHTML = sanitizeHTML
    //let strippedHTML = await sanitizeHTML2(page);
    //TODO, write this to the database as UUID, stepNo, data

function findTagInContent(tag, content){
    var result =  content.match(/<title\b[^<]*(?:(?!<\/title>)<[^<]*)*<\/title>/gi, '');
    console.log('result; ', result);

    var regexp = new RegExp("/<" + tag + "\b[^<]*(?:(?!<\/" + tag + ">)<[^<]*)*<\/" + tag + ">", "gi");
    return content.match(regexp);
}
//<input\b[^<]*(?:(?!<\/input>)<[^<]*)*<\/input>

function noscript(strCode){
    var html = $(strCode.bold()); 
    html.find('script').remove();
    return html.html();
}

 function stripScripts(s) {
    var div = document.createElement('div');
    div.innerHTML = s;
    var scripts = div.getElementsByTagName('script');
    var i = scripts.length;
    while (i--) {
      scripts[i].parentNode.removeChild(scripts[i]);
    }
    return div.innerHTML;
  }
*/