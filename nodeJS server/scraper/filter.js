//filters out useful content from raw HTML data
//this is using cheerio functionality, so for specific API calls, look at cheeriojs
//https://github.com/cheeriojs/cheerio
const htmlToJson = require('html-to-json');

module.exports = {
	sanitizePageContent: (content) => {
	    //remove script/style tags
	    content = module.exports.stripScriptTags(content);
	    content = module.exports.stripStyleTags(content);
	    content = module.exports.stripSVGTags(content);
	    content = module.exports.stripHeadTags(content);
	    return content;
	},

	stripScriptTags: (str) => {
	    return str.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '');
	},

	stripStyleTags: (str) => {
	    return str.replace(/<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gi, '');
	},

	stripSVGTags: (str) => {
	    return str.replace(/<svg\b[^<]*(?:(?!<\/svg>)<[^<]*)*<\/svg>/gi, '');
	},

	stripHeadTags: (str) => {
	    return str.replace(/<head\b[^<]*(?:(?!<\/head>)<[^<]*)*<\/head>/gi, '');
	},

	findAllTagsWithInnerHTML: async(content) =>{
	    //return 

	},

	findAllTagTypesInString: async(content) =>{
	    return await htmlToJson.parse(content, function(){
	        return this.map('*', function($item) {
	            var text = $item.text().trim();
	            
	            if($item.children().length > 0){
	            	//TODO check if parent has the same text as child
	            	//or, just get rid of divs, they seem irrelevant
	            	if($item[0].name === 'div'){
		            	//and if no link or other attributes are present
		            	if($item.attr('href') === undefined && $item.attr('aria-label') === undefined){
		                	text = DELETE_FLAG;
		            	}
		            }
	            }

	            if($item.children().length > 0)
	            	text = module.exports.filterRepetitiveTextInParent($item);
	            //remove empty divs that are not inputs
	            else if($item.text() === ''){
	                if($item[0].name !== 'input'){
	                    text = DELETE_FLAG;
	                }
	            }
	            //TODO check $item.data() //for cheerio data- property
	            return {
	                tag: $item[0].name,
	                class: $item.attr('class'),
	                dataToggle: $item.attr('data-toggle'),
	                dataTarget: $item.attr('data-toggle'),
	                label: $item.attr('label'),
	                href: $item.attr('href'),
	                arialabel: $item.attr('aria-label'),
	                id: $item.attr('id'), 
	                text: text
	                //item: JSON.parse(JSON.stringify($item))
	            };
	        });
	    });
	    await promise.done(function(items){
	        return items;
	    }, (err) => {
	        console.log('err: ', err);
	    });
	},

	filterRepetitiveTextInParent: ($item) =>{
		var parentText = $item.text().trim();
		var childTexts = $item.find('a').eq(0);
		//console.log('childTexts: ', childTexts);
		//check if children have the same text
		//console.log('$item.children(): ', $item.children());
		for(var i = 0; i < childTexts.length; i++){

			var childText = childTexts.eq(i).text().trim();//.get(i)//.text().trim();
			//console.log('childrText: ', childText);
			if(childText.length > 0){
				if(parentText.includes(childText)){
					//console.log('removingparneText: ' + parentText + ', childText: ' + childText)
					return DELETE_FLAG;
				}
			}
		}
		return parentText;
	},

	getRelevantText: (item) => {
	    //if has children
	    if(item.children().length > 0){
	        //and children have the same text, but shortened.
	        //then most likely is a duplicate, and there's no need to shwo this.
	        // if(item.text().includes(item.children().text())){
	        //     return ''//'{}TO-DELETE{}';
	        // }
	        
	        //TODO, eventually find a way to isolate only the relevant text, by finding text of direct element.
	    }
	    return item.text();
	},

	//what to exclude from the tags
	//seocndary filter to exclude the tags that have been marked as unnecessary
	filterIrrelevantTags: ($item) => {

	    if($item.children().length > 0){
	        return DELETE_FLAG;
	    }
	    else if($item[0].name === 'input'){
	        return '';
	    }
	    else return $item.text().trim();
	}
}

