const constants = require('../config/constants');
const puppeteer = require('puppeteer');

//for quick testing
//is called via API calls with optional parameters
//e.g. http://localhost:5000/api/searchUrl/https:%7B%7D%7B%7Dwww.webscraper.io%7B%7Dtest-sites/false/true

//to test longer processes, kind of like API Harness
exports.testGoToUrl =  async function (url, page) {
    console.log("========= DEBUG MODE =============")
    url = url || 'https://www.webscraper.io/test-sites';
    url = url.replace(/{}/g,'/');
    try{
        await page.goto(url);    

        const title = await page.title();
        console.log("title: " + title);
        
        //const elements = await page.$x("//a[text()='E-commerce site]']");
        var stringArray = constants.getString.textSearch[1];
        var string = stringArray[0] + 'a' + stringArray[1] + 'E-commerce site' + stringArray[2];
        console.log('looking up string: ', string);
        const elements = await page.$x(string);

        //const elements = await page.$x("//*[contains(text(), 'E-commerce site')]");
        //const elements = await page.$x("//*[text()= 'E-commerce site']");
        console.log('elements: ', elements);
        await elements[0].click();
        await page.waitForNavigation({ waitUntil: 'networkidle0' });
        let allHTML = await page.evaluate(() => document.body.innerHTML);

        //let sanitizedContent = sanitizePageContent(allHTML);
        //var tagTypes = await findAllTagTypesInString(sanitizedContent);
        
        //tagTypes = tagTypes.filter(item => item.text !== DELETE_FLAG);
        return {
            url: url,
            step: currentStep,
            title: title,
            screenshot: screenshot,
            //tags: tagTypes,
            allHTML: allHTML,
            //sanitizedContent: sanitizedContent
        }
    } 
    catch(err) {
        console.log('err: ', err);
        return { allHTML: '' }
    }
}

exports.renderTest = async function(){
    //console.log("Testing directly rendering pages");
    const browser = await puppeteer.launch();
    //console.log('browser launched');
    const page = await browser.newPage();
    //console.log('newpage launched');
    //var count = 0;
    var styles = [];
    var scripts = [];
    var fonts = [];
    page.on('response',async response => {
        console.log('resource detected: ', response.request().resourceType());
        if(response.request().resourceType === 'stylesheet' )console.log('resource is: ', response.request());
        if(response.request().resourceType() === 'stylesheet') {
            //count++;
            var styleString = await response.text();
            console.log('pushed stylestring');
            styles.push(styleString)
        }
        if(response.request().resourceType() === 'script'){
            var scriptString = '<script>' + await response.text() + '</script>'
            scripts.push(scriptString);
        }
        if(response.request().resourceType() === 'font'){
            //var fontString = 
            //console.log('font: ', await response.text());
        }
        if(response.request().resourceType() === 'image'){
            //console.log('image: ', await response.text());
        }
    });
    await page.goto('https://cloud.webscraper.io/login', {waitUntil: 'networkidle2'});
    const allHTML = await page.evaluate(() => document.documentElement.outerHTML);
    console.log("styles.length: ", styles.length);

    // await page._client.send('DOM.enable');
    // await page._client.send('CSS.enable');
    // const doc = await page._client.send('DOM.getDocument');
    // const nodes = await page._client.send('DOM.querySelectorAll', {
    //     nodeId: doc.root.nodeId,
    //     selector: '*'
    // });
    // const styleForSingleNode = await page._client.send('CSS.getMatchedStylesForNode', {nodeId: 3});
    // const stylesForNodes = nodes.nodeIds.map(async (id) => {
    //     return await page._client.send('CSS.getMatchedStylesForNode', {nodeId: id});
    // });

    // console.log(JSON.stringify(stylesForNodes));
    // console.log(JSON.stringify(styleForSingleNode));

    //styles = styles.splice(1, 2);
    return { 
        allHTML: allHTML,
        styles: styles,
        scripts: scripts,
        fonts: fonts
    }
    //await browser.close();
}


//https://stackoverflow.com/questions/53746748/extract-all-css-with-puppeteer
//https://github.com/GoogleChrome/puppeteer/issues/1619