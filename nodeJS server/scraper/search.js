//allocates hard coded dynamic search queries based on selector properties
const constants = require('../config/constants');

exports.readRequestData = async function(properties){
	var selectors = [];
	let tag = '';
	//order of queries to prioritize
	//ID => TEXT, CLASS, data-type => etc. 
	console.log('properties: ', properties);
	//set tag first as it always exists, and is necessary to query elements
	for(var c = 0; c < properties.length; c++){
		if(properties[c].match(/^tag:/gi) !== null){
			tag = properties[c].split(":")[1];
			break;
		}
	}

	//console.log('selector.tag: ', selector.tag);

	for(var i = 0; i < properties.length; i++){
		if(properties[i].match(/^text:/gi) !== null){
			//search based on string
			var selector = new SelectorModel();
			selector.tag = tag;
			selector.type = 'text';
			selector.content = properties[i].split(":")[1];
			selector.priority = constants.getSelectorPriority["text"];
			selector.queryString = injectTextData(
				selector.content, 
				constants.getXPath.textSearch[0], 
				tag);
			selectors.push(selector);
			console.log('added selector: ', selector);
		}
		else if(properties[i].match(/^data/gi) !== null){
			var selector = new SelectorModel();
			selector.tag = tag;
			selector.type = 'data';
			selector.content = properties[i].split(":")[1];
			selector.priority = constants.getSelectorPriority["data"];
			selector.queryString = injectDataData(
				selector.content,
				constants.getXPath.dataSearch[0],
				properties[i].split(":")[0], //the first part fo the string as well
				tag);
			selectors.push(selector);
			console.log('added selector: ', selector);
		}
		/* TODO temporarily commented out
		else if(properties[i].match(/^href:/gi) !== null){
			selector.href = properties[i].split(":")[1];
		}
		else if(properties[i].match(/^id:/gi) !== null){
			selector.id = properties[i].split(":")[1];
			//selector.idQuery = injectData(selector.id, constants.getString.xPath.isId, selector.tag);
		}
		else if(properties[i].match(/^class:/gi) !== null){
			selector.class = properties[i].split(":")[1];
			selector.class = "." + selector.class;
		}
		else if(properties[i].match(/^aria-label:/gi) !== null){
			selector.ariaLabel = properties[i].split(":")[1];
			selector.ariaLabel = '[aria-label:' + selector.ariaLabel + ']';
		}
		else if(properties[i].match(/^label:/gi) !== null){
			selector.label = properties[i].split(":")[1];
		}
		*/
	}
	return selectors;
}

function injectTextData(text, stringArray, tag){
	tag = tag || '*';
	return stringArray[0] + tag + stringArray[1] + text + stringArray[2];
}
//for data tags like data-toggle, data-target
//these are dynamic, and are sent in as string parameters
function injectDataData(content, xPathArray, dataString, tag){
	tag = tag || '*';
	return xPathArray[0] + tag + xPathArray[1] + dataString + xPathArray[2] + content + xPathArray[3];
}
function injectEvaluateData(selectorType, identifier, stringArray){
	return;  
}

//executes based on selector priority
exports.executeAction = async (page, selectors, actionType) => {
	//[constants.getString.xPath.containsText[2]](page, selector);
	//console.log("received selectors: ", selectors);
	const searchSelector = await getSearchSelector(selectors);
	//console.log("found selector: ", searchSelector);
	const element = await getSearchFunction(page, searchSelector);
	//console.log("found searchFunctionElement: ", element);

	if(actionType === 'click'){
		console.log("clicking element: ");
		//console.log(element);
		try{
			const clicked = await element[0].click();
			//console.log('clicked element: ', clicked);
			await page.waitForNavigation({ waitUntil: 'networkidle2' }); //'networkidle0' 
			console.log("network idle");
			//const screenshot = await page.screenshot({ path: './test-data/' + Date.now().toString() + '-' + currentStep + '.png' });
    		//console.log("=================== SCREENSHOT SAVED SUCCESSFULLY ======================");
     		return page;
		}catch(err){
			console.log('click error caught successfully: ', err);
			if(page === null || typeof page === undefined){
				//request more information
				return {
					failure: 'need more info'
				}
			}
		}
	}
	else if(actionType === 'readdata'){
		console.log('reading data from element');
		try {
			await element[0].getProperty('innerHTML'); //'value'
			const innerHTML =  await element[0].jsonValue();
			// var content = Array.prototype.filter.call(element.childNodex, function(element) {
			// 	return element.nodeType === Node.TEXT_NODE;
			// }).map(function(element){
			// 	return element.textContent;
			// }).join("");
			return innerHTML;

		} catch(err){
			console.log('reading data error caught successfully: ', err);
			if(innerHTML === null || typeof innerHTML === undefined){
				//request more information
				return {
					failure: 'need more info'
				}
			}
		}
	}
}

 function SelectorModel() {
	this.tag= '',
	this.type= '',
	this.priority= 100,
	this.queryString= '',
	this.content= ''	,
	this.searchIndex= 0 
}

//loops through all selectors for an element and returns the one with the highest priority	
async function getSearchSelector(selectors){
	var minIndex = selectors.length - 1;
	//console.log('minIndex: ', minIndex);
	var type = '';
	//loop through all selectors and find the minimum, then perform action on the minimum
	for(var i= 0; i < selectors.length; i++){
		var selector = selectors[i];
		if(selector.priority < minIndex){
			minIndex = i;
			type = selector.type;	
		} 
	}
	//console.log('minIndex: ', minIndex);
	console.log('getting type: ', type);
	console.log('getting selector: ', selectors[minIndex]);
	if(selectors[minIndex] === undefined){
		return {
			failure: 'need more info'
		}
	}
	else {
		return {
			selector: selectors[minIndex],
			type: type
		};	
	}
}

/*
how to put variables into strings for xPath queries
function formatDataSelector (element, attribute) {
  return `[${attribute}="${element.getAttribute(attribute)}"]`
}

also, dynamically assigning functions, the way I was trying to do it.
 `await ${this._frame}.type('${selector}', '${value}')
 
 then executing this string as a function
 
 https://github.com/checkly/puppeteer-recorder/blob/master/src/code-generator/CodeGenerator.js
*/

async function getSearchFunction(page, selector){
	console.log('searching via: ', selector.selector.type);
	switch(selector.selector.type){
		case "text":
			const element = await searchViaText(page, selector.selector);
			//console.log('element: ', element);
			return await element;
		// default:
		// 	return null;
	}
}

//sequence to search via Text
//1. xPath: search for exact string using 
	//if more than one exists, check for id or class recursively
//2. xPath: search for element containing string
	//if more than one exists, check for id or class recursively
async function searchViaText(page, selector){
	//console.log("searching for selector: ", selector);
	try{
		const element = await page.$x(selector.queryString);
		//element = await page.$x("//a[text()='E-commerce site']");
		//console.log("found element: ");//, element);
		if(element == null || typeof element == undefined || element.length == 0){
			//console.log('element does not exist, re-executing');
			return await reexecute(page, selector);
		}
		else {
			//console.log("returning element: ", element[0]);
			return await element;
		}
	}catch(err){
		console.log("err going to element re-executing: ", err);
		return await reexecute(page, selector);
	}
}

async function reexecute(page, selector){

	if(selector.searchIndex < constants.getXPath.textSearch.length){
		//console.log("trying again: ");
		//increment index of query and try again
		++selector.searchIndex;
		//console.log("new search index: ", selector.searchIndex);
		//if index is larger than 1, no tag
		selector.queryString = injectTextData(
			selector.content, 
			constants.getXPath.textSearch[selector.searchIndex], 
			selector.searchIndex > 1 ? '' : selector.tag);
		const element = await searchViaText(page, selector);
		//console.log('4. found reexecute element: ', element);
		return element;
	}
}
	



//different execution strategies
// async function executeByXPath(page, selector){
// 	console.log("executing by xpath: ", selector.text);
// 	return await page.$x("//a[text()='E-commerce site]']");
// 	//return await page.$x(selector.text);
// }

// async function executeByEval1(page, identifier){
// 	console.log("executing by eval1: ", identifier);
// 	return await page.$eval(identifier, el => el.innerText);

// }






/*
//var stack = []; in JS, arrays are treated the same as stacks when using simple array modifiers
	
	//stack.push()
	stack.pop()
console.log("querying elements: ", selector);
	const fn = 'xPath';
	console.log("typeof selector.texts[xPath]: ", typeof [selector.texts]);
	console.log('selector.texts[xPath]: ', selector.texts('E-commerce site'));
	

function initTagSelectors(text){

}
function initHrefSelectors(text){

}
function initIdSelectors(text){

}
function initClassSelectors(text){
}
function initAriaLabelSelectors(text){

}
function initLabelSelectors(text){

}
*/