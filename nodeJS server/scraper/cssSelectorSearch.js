const constants = require('../config/constants');
const scraper = require('./scraper.js');
//const { PendingXHR } = require('pending-xhr-puppeteer');

exports.executeAction = async function (page, instanceId, cssSelector, actionType, text){
	try{
		console.log('executing action on: ', cssSelector);
		const element = await page.waitForSelector(cssSelector);
		console.log('found element');
		if(actionType === 'click'){
			console.log("clicked: ", actionType);
			//Promise.race
			//const [response] = 
			//try{
				await Promise.all([
					page.waitForNavigation({ timeout: 10000, waitUntil: ['domcontentloaded','load','networkidle0','networkidle2'] }),
					//page.waitForNavigation({ timeout: 10000, waitUntil: ['networkidle0'] }),
					element.click(),
					page.frames(),
					page.evaluate(async () => {
					  const selectors = Array.from(document.querySelectorAll("img"));
					  await Promise.all(selectors.map(img => {
					    if (img.complete) return;
					    return new Promise((resolve, reject) => {
					      img.addEventListener('load', resolve);
					      img.addEventListener('error', reject);
					    });
					  }));
					})
				]);
			//} catch(err){
			//	console.log("caught navigation timeout error");
			//}
			console.log("loading frames");
			//const frames = await page.frames();
			//await page.waitFor(1000);
			await page.waitForFunction(imagesHaveLoaded, { timeout: 10000 });
			
			console.log("page done: ");
			// await element.click();
			// //wait for network to stop loading page, if a page change has occurred
			// await page.waitForNavigation({ waitUntil: 'networkidle2' }); //depending on the number of network requests allowed to be remaining.
		}
		else if(actionType === 'type'){
			console.log('typing text: ', text);
			await element.type(text);
		}
		else if(actionType === 'readdata'){
			console.log('reading data...');
			let el = await page.$(cssSelector);
			let text = await el.getProperty('innerText'); //'value' //innerHTML //textContent //innerText
			const innerHTML =  await text.jsonValue();
			//TODO get this element's innerHTML/textContent/innerText
			//console.log('got innerHTML: ', innerHTML);

		}
		else if(actionType === 'back'){
			console.log("going back");
			//Alt+Left is back in chrome, so see if that works
			await page.keyboard.down('Alt');
			console.log("alt pressed");
			await page.waitFor(1000);
			//await page.keyboard.down(String.fromCharCode(37));
			//await page.waitFor(100);
			await page.keyboard.press('ArrowLeft');
			console.log("left pressed");
			await page.waitFor(1000);
			//await page.keyboard.up(String.fromCharCode(37));
			await page.keyboard.up('Alt');
			console.log('alt up');

			await page.keyboard.down('ControlLeft');
			await page.waitFor(1000);
			await page.keyboard.press('f');
			await page.waitFor(1000);
			await page.keyboard.up('ControlLeft');

			
		}

		// console.log("waiting all XHR to finish");
		// const pendingXHR = new PendingXHR(page);
	 //    await pendingXHR.waitForAllXhrFinished();
	 //    console.log("All XHR finished!!");
	    //await page.waitFor(3000);
	    await scraper.startXHRListener(page, instanceId);
		return await page;
	} catch(err){
		console.log('ERROR executing action: ', err);
		//await page.waitForNavigation({ waitUntil: 'domcontentloaded' }); //'load' //'networkidle0' //'networkidle2' 
		//await element.click();
		return page;
	}
}

function imagesHaveLoaded() { 
	return Array.from(document.images).every((i) => i.complete); 
}
	