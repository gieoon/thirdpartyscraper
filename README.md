# DATA2U

### Running on Local

`npm start` for the client

`cd server && node server.js` for the server

server is running on localhost:5000
client is on localhost:8080

### Deployments

Modify version number in root App.js
`global.VERSION = /* version number here */;`

```
$ Firebase logout
$ Firebase login
$ Firebase init
$ Firebase deploy
```
Deployment URL:
> https://thirdpartyscraper.firebaseapp.com


npm run build (to create build folder holding static assets)

add to gh-pages
===============================
The build folder is ready to be deployed.
To publish it at http://gieoon.github.io/data2u , run:

  yarn add --dev gh-pages

Add the following script in your package.json.

    // ...
    "scripts": {
      // ...
      "predeploy": "yarn build",
      "deploy": "gh-pages -d build"
    }

Then run:

  yarn run deploy

