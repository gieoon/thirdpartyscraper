//ENTRY POINT
//main server layer to run queries

const runner = require('./Runner/runner.js');
const DB = require('./db.js');
const scheduler = require('./scheduler.js');


(async function(){	
	console.log("Server Initializing At: " + new Date(Date.now()));	
	const usersList = await DB.organizeUsersToQuery();
	console.log("Retrieved Users: ", usersList);
	//await runner.init(usersList);
	//await scheduler.init(usersList);
	await DB.initScheduleListener(usersList);
})();



