const scheduler = require('./scheduler.js');

let serviceAccount = require("./kn1oc-e779ecbeef.json");
const admin = require('firebase-admin');
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://thirdpartyscraper.firebaseio.com"
});

const USER_COLLECTION = 'Users';
const DATA_COLLECTION = 'Data';
const TIMESTAMP_COLLECTION = 'Timestamps';
const STEPS_COLLECTION = 'Steps';
const QUERY_COLLECTION = 'Queries';

const db = admin.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);
//module.exports.db = db;

//UPDATE make this a listener so it will stop schedules as listened as well.
const initScheduleListener = async function(usersList){
	for(userId in usersList){
		let user = usersList[userId];
		db.collection(USER_COLLECTION)
			.doc(user.toString())
			.collection(QUERY_COLLECTION)
			.onSnapshot(function(querySnapshot){
				var queries = [];
				//console.log("CREATING DB LISTENERS");
				querySnapshot.docChanges().forEach(function(change){
					//console.log("found doc: ", change.doc.id)
					//querySnapshot.docChanges().forEach(function(change){
						//ADDING QUERIES
						if(change.type === 'added'){
							//check flag for partial saving is not true
							var partialFlag = change.doc.get('frequency');
							if(partialFlag !== undefined){
								console.log("new data added||initialized: ", change.doc.id);
								let stepsPromise = change.doc.ref.collection('Steps').get();
								var steps = {};
								stepsPromise.then(function(stepsSnapshot){
									stepsSnapshot.forEach(function(step){
										steps[step.id] = step.data();
									});
									scheduler.addSchedule({id: change.doc.id, query: change.doc.data(), steps: steps}, user);
								})
							}
							else {
								console.log("=== Skipping ADDING partial incomplete query: ", change.doc.id, " ===");
							}
						}

						//MODIFYING QUERIES
						if(change.type === 'modified'){
							//console.log("Modified query: ", change.doc.data());

							// change.doc.get().then(function(querySnapshot){
							// 	console.log('querySnapshot: ', querySnapshot);
							// })
							var partialFlag = change.doc.get('frequency');
							if(partialFlag !== undefined){
								change.doc.ref.collection('Steps').get().then(function(stepsSnapshot){
									var steps = {}
									stepsSnapshot.forEach(function(step){
										//console.log("step: ", step.id);
										steps[step.id] = step.data();
									});
									global.currentQueries[change.doc.id] === undefined ? 
										scheduler.addSchedule({id: change.doc.id, query: change.doc.data(), steps: steps}, user)
										:
										scheduler.modifySchedule({id: change.doc.id, query: change.doc.data(), steps: steps});
								})
							}
							else {
								console.log("=== Skipping MODIFYING partial incomplete query: ", change.doc.id, " ===");
							}
						}

						//DELETING QUERIES
						//user can still see partial queries from the front end, this will be dealt with later, but this is a source of bad data, so have to check for that here as well
						if(change.type === 'removed'){
							//console.log("=== Removing Query: ", change.doc.id, " ===");
							var partialFlag = change.doc.get('frequency');
							if(partialFlag !== undefined){
								change.doc.ref.collection('Steps').get().then(function(stepsSnapshot){
									const steps = {}
									stepsSnapshot.forEach(function(step){
										steps[step.id] = step.data();
									});
									scheduler.deleteSchedule({id: change.doc.id, query: change.doc.data(), steps: steps});
								});
							}
							else {
								console.log("=== Skipping REMOVING partial incomplete query as it does not exist: ", change.doc.id, " ===");
							}
						}
					//})
				});
			});
	}
}



//goes through all users and assigns an order for their queries
const organizeUsersToQuery = async function(){
	const usersList = [];
	var usersPromise = db.collection(USER_COLLECTION).get();
	return usersPromise.then(function(users){
		users.forEach(function(user){
			let username = user.id;
			usersList.push(username);
		});
		return usersList;
	});
};

const decodeQueriesForUserFromDB = async function(userId){
	console.log("decoding query for: ", userId);
	const promises = [];
	var queriesPromise = db.collection(USER_COLLECTION)
		.doc(userId.toString())
		.collection(QUERY_COLLECTION)
		.get();

	return queriesPromise.then(function(queries){
		queries.forEach(function(query){
			let queryCreatedOn = query.get('createdOn');
			let queryDay = query.get('day');
			let queryDayOfWeek = query.get('dayOfWeek');
			let queryFrequency = query.get('frequency');
			let queryHour = query.get('hour');
			let queryMinute = query.get('minute');
			let queryName = query.get('name');
			let stepsPromise = query.ref.collection('Steps').get();
			var steps = {};

			promises.push(
				stepsPromise.then(function(stepsSnapshot){
					stepsSnapshot.forEach(function(step){
						steps[step.id] = step.data();
					});
					return { 
						steps: steps, 
						queryId: query.id, 
						name: queryName, 
						createdOn: queryCreatedOn,
						day: queryDay,
						dayOfWeek: queryDayOfWeek,
						frequency: queryFrequency,
						hour: queryHour,
						minute: queryMinute
					};
					console.log('stepsSnapshot: ', stepsSnapshot);
				})
			);
		});
		return Promise.all(promises);
	});
}

const saveScheduleDetailsWithData = function(userId, jobId, runId, queryId,  actualFireDate, scheduledFireDate, fireDisparity, runDuration, data){
	console.log("saving schedule details: ", actualFireDate, '.', scheduledFireDate, '.', fireDisparity, '.', runDuration);
	console.log("queryId: ", queryId);
	console.log("userId: ", userId);
	console.log("jobId: ", jobId);
	var query = {};

	var steps = db.collection(USER_COLLECTION)
					.doc(userId.toString())
					.collection(DATA_COLLECTION)
					.doc(queryId.toString())
					.set({
						[`${runId.toString()}`]: {
							 actualFireDate: actualFireDate,
							 jobId: jobId.toString(),
							 scheduledFireDate: scheduledFireDate,
							 fireDisparity: fireDisparity,
							 executionDuration: runDuration,
							 //values: [{ stepNo: '4', value: '$1311.99', timestamp: 1554213398249 }, { stepNo: '3', value: '$1322.99', timestamp: 1554313398249 }]
							 values: data
						}
					}, {merge: true})
					.then(function(){
				   		console.log("saveScheduleDetailsWithData() data UPDATED successfully");
				    })
				    .catch(function(err){
						console.log('error caught during UPDATE: ', err);
					});
}

const saveDataToDB = function(userId, jobId, queryId, dataTimestamp, stepNo, value){
	console.log("saving queried value");
	var query = {};

	try{
		var steps = db.collection(USER_COLLECTION)  
					   .doc(userId.toString())
					   .collection(DATA_COLLECTION)
					   .doc(queryId.toString())
					   .update({
					   		[`${runId.toString()}`]: {
					   			timestamp: dataTimestamp.toString(),
					   			Data: value,
					   			Step: stepNo.toString()
					   		}
					   })
					   // .collection(TIMESTAMP_COLLECTION)
					   // .doc(dataTimestamp.toString())
					   // .collection(STEPS_COLLECTION)
					   // .doc(stepNo.toString())
					   // .set({
					   // 		Data: value
					   // })
					   .then(function(){
					   		console.log("saveDataToDB() saved successfully: ", value);
					   })
					   .catch(function(err){
					   		console.log("error caught saving value: " + value + ' Error: ' + err);
					   });

/*

data: admin.firestore.FieldValue.arrayUnion({
					   			timestamp: dataTimestamp.toString(),
					   			Data: value,
					   			Step: stepNo.toString()
					   		})
*/
		 // db.collection(USER_COLLECTION)  
			// 		   .doc(userId.toString())
			// 		   .collection(DATA_COLLECTION)
			// 		   .doc(queryId.toString()).set({
			// 		   		default: 0
			// 		   });
			/*
							[`dataTimestamp`]: {
					   			timestamp: dataTimestamp.toString(),
					   			Data: value,
					   			Step: stepNo.toString()
					   		}
			*/

	} catch(err){
		console.log("error caught2: ", err);
	};
}
module.exports = {
	db: db,
	initScheduleListener: initScheduleListener,
	organizeUsersToQuery: organizeUsersToQuery,
	decodeQueriesForUserFromDB: decodeQueriesForUserFromDB,
	saveScheduleDetailsWithData: saveScheduleDetailsWithData,
	saveDataToDB: saveDataToDB
};

