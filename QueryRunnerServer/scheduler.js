//https://www.npmjs.com/package/node-schedule
const schedule = require('node-schedule');

const runner = require('./Runner/runner.js');
const uuidv1 = require('uuid/v1');

global.scheduledJobs = {}; 
global.currentQueries = {};

exports.addSchedule = async function(data, userId){
	let query = constructQueryFromData(data);
	//only add if query is complete
	
	console.log("== Constructing Scheduled Job for: " + (query.name || 'New Query title') + ' ID: ' + query.queryId + ' Steps: ' + Object.keys(query.steps).length);
	let queryRunId = uuidv1();
	//console.dir('query: ', query);
	var constructedDate = constructScheduleDate(query.dayOfWeek, '*', query.day, query.hour, query.minute);
	console.log(constructedDate);
	await constructScheduledJob(constructedDate, userId, query);
}

exports.deleteSchedule = async function(data){
	let query = constructQueryFromData(data);
	console.log("== Deleting Scheduled Job for: " + (query.name || 'New Query title') + ' ID: ' + query.queryId + ' Steps: ' + Object.keys(query.steps).length);
	getJobFromQueryId(query.queryId).cancel();
}

//don't know if the query was modified or is newly added, but query existing id's and if it isn't in there it's safe to assume it's a new query, so create a new one instead.
exports.modifySchedule =async function(data){
	let query = constructQueryFromData(data);
	console.log("== Modifying Scheduled Job for: " + (query.name || 'New Query title') + ' ID: ' + query.queryId + ' Steps: ' + Object.keys(query.steps).length);
	var constructedDate = constructScheduleDate(query.dayOfWeek, '*', query.day, query.hour, query.minute);
	console.log(constructedDate);
	getJobFromQueryId(query.queryId).reschedule(constructedDate);
}

//only one query is running at a time, so can get the jobId from this very easily and translate it into a queryId
const getJobFromQueryId = function(queryId){
	//console.log("querying for queryId: ", currentQueries[queryId]);
	return scheduledJobs[currentQueries[queryId]].job;
}

const constructQueryFromData = function(data){
	let query = data.query;
	query.queryId = data.id;
	query.steps = data.steps;
	return query;
}




//schedule a job to run at an interval
/*

*    *    *    *    *    *
┬    ┬    ┬    ┬    ┬    ┬
│    │    │    │    │    │
│    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
│    │    │    │    └───── month (1 - 12)
│    │    │    └────────── day of month (1 - 31)
│    │    └─────────────── hour (0 - 23)
│    └──────────────────── minute (0 - 59)
└───────────────────────── second (0 - 59, OPTIONAL)

*/
function constructScheduleDate(dayOfWeek, month, dayOfMonth, hour, minute){
	var seconds= '0 ';
	var date = seconds; //start with seconds defined, we won't use it yet.
	//console.log(minute, hour, dayOfMonth, month, dayOfWeek);
	var tempDayOfWeek = '';
	if(dayOfWeek !== '') {
		tempDayOfWeek = convertDayOfWeek(dayOfWeek);
	}
	//validation checks
	if(	   boundsCheck(minute, 0, 59) 
		&& boundsCheck(hour, 0, 23) 
		&& boundsCheck(dayOfMonth, 1, 31) 
		//&& boundsCheck(month, 1, 12)
		&& boundsCheck(tempDayOfWeek, 0, 7)){

		if(minute === ''){
			date += '* ';
		} else {
			date += minute + ' ';
		}

		if(hour === ''){
			date += '* ';
		} else {
			date += hour + ' ';
		}

		if(dayOfMonth === ''){
			date += '* ';
		} else {
			date += dayOfMonth + ' ';
		}

		if(month === ''){
			date += '* ';
		} else {
			date += month + ' ';
		}

		if(tempDayOfWeek === ''){
			date += '* ';
		} else {
			date += tempDayOfWeek + ' ';
		}
		
		//debug
		//return '*/30 * * * * *'; //every 30 seconds
		//return '* */30 * * * *'; //hourly at minute :06, 00 seconds
		//return '0 * * * * *'; //evey min
		return date;
	}
	else {
		console.log('ERROR: Query has illegal bounds, job not scheduled');
		return '';
	}
}

function boundsCheck(input, min, max){
	return ((parseInt(input) <= max) && (parseInt(input) >= min)) || input.toString().length === 0;
}

function convertDayOfWeek(dayOfWeek){
	//console.log("converting: ", dayOfWeek);
	switch(dayOfWeek.trim()){
		case 'monday': return '1 ';
		case 'tuesday': return '2 ';
		case 'wednesday': return '3 ';
		case 'thursday': return '4 ';
		case 'friday': return '5 ';
		case 'saturday': return '6 ';
		case 'sunday': return '7 ';
		default: return '0 ';
	}
}

//read query data from database and create a new job
//run this whenever this script starts
async function constructScheduledJob(constructedDate, userId, query){
	//assign a jobId, if it is assigned, it means that this job exists and has been picked up. 
	//if a jobId is not assigned, no job has been created.
	var jobId = require('uuid/v1')();
	var actualFireDate, fireDisparity, scheduledFireDate, runDuration;
	var j = await schedule.scheduleJob(constructedDate, async function(fireDate){
		console.log(">>>>>>>>>>>>>>>>> JOB EXECUTING >>>>>>>>>>>>>>>>");
		const runId = require('uuid/v1')();
		console.log('runId: ', runId);
		actualFireDate = new Date(); 
		fireDisparity = actualFireDate - fireDate;
		scheduledFireDate = fireDate;
		const dataArray = await runner.init(jobId, userId, query);

		//console.log("dataArray: ", dataArray);
		runDuration = new Date() - actualFireDate;
		scheduledJobs[jobId].actualFireDate = actualFireDate; 
		scheduledJobs[jobId].scheduledFireDate = scheduledFireDate; 
		scheduledJobs[jobId].fireDisparity = fireDisparity;
		scheduledJobs[jobId].runDuration = runDuration;
		scheduledJobs[jobId].runId = runId;
		//console.log('DB: ', DB);
		require('./db.js').saveScheduleDetailsWithData(userId, jobId, runId, query.queryId, actualFireDate, scheduledFireDate, fireDisparity, runDuration, dataArray);
	});
	scheduledJobs[jobId] = {
		jobId: jobId,
		job: j
	};
	currentQueries[query.queryId] = jobId;

	return await jobId;
}
// //remove and stop a job that's already running
// exports.cancelScheduledJob = function(jobId){
// 	scheduledJobs[jobId].job.cancel();
// }

// //edit an existing job
// exports.updateJob = function(jobId, constructedDate){
// 	scheduledJobs[jobId].job.reschedule(constructedDate);
// }

exports.getNextScheduledRun = function(jobId){
	return scheduledJobs[jobId].job.nextInvocation();
}
//save job details to DB to run constantly behind the scenes. If server shuts down, these can be reinitiated.


/*

//deprecated in favour of DB setting listener and creating schedules
exports.init = async function(usersList){
	//load steps
	console.log("Running At: " + new Date(Date.now()));	
	for(user in usersList){
		//console.log('user: ', usersList[user]);
		let queries = await DB.decodeQueriesForUserFromDB(usersList[user]); 
		
		let userRunId = uuidv1();
		//console.dir('query received: ', queries);

		for(queryIndex in queries){
			console.log("Constructing Scheduled Job for: " + (queries[queryIndex].name || 'New Query title') + ' ID: ' + queries[queryIndex].queryId + ' Steps: ' + queries[queryIndex].steps);
			let queryRunId = uuidv1();
			let query = queries[queryIndex];
			//console.dir('query: ', query);
			var constructedDate = constructScheduleDate(query.dayOfWeek, '*', query.day, query.hour, query.minute);
			console.log(constructedDate);
			await constructScheduledJob(constructedDate, usersList[user], query);
		}
	}
}

*/
