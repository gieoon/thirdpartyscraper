//runs the queries via puppeteer

const puppeteer = require('puppeteer');
const DB = require('../db.js');
const uuidv1 = require('uuid/v1');

//initializes one instance for one query
//after finishing this query, will close it and move to the next query
exports.init = async function(jobId, user, query){
	console.log(">>>>>>>>>>>>>>>>>>>>>>>>>> TASK RUNNER SERVER INITIALIZING >>>>>>>>>>>>>>>>>>>>");
	console.log((new Date()).toString());
	const width = 1000;
	const height = 1600;
	const args = await puppeteer.defaultArgs().filter(flag => flag !== '--enable-automation');
	const browser = await puppeteer.launch({
		headless: true,
		args: [
			`--window-size=${ width },${ height }`,
			`--mute-audio=true`,
			'--no-sandbox',
            '--disable-setuid-sandbox',
		],
		isMobile: false
	});

	const page = await browser.newPage();

	currentStep = 0;
	retryCount = 0;

	await page.setViewport({
		width, height
	});

	dataArray = [];
	
	//load steps
	// for(user in usersList){
	// 	let queries = await DB.decodeQueriesForUserFromDB(usersList[user]); 
	// 	let userRunId = uuidv1();
	// 	console.log('query received: ', queries);

		// for(query in queries){
		// 	console.log("query: ", queries[query].queryId);
		// 	console.log("Running: " + (queries[query].name || 'New Query title') + ' ' + queries[query].queryId + ' with steps: ' + queries[query].steps);
		// 	let queryRunId = uuidv1();
	for(stepNo in query.steps){
		console.log("handling step: ", query.steps[stepNo]);
		let step = query.steps[stepNo];
		retryCount = 0;
		switch(step.action){
			case 'url':
				await goToUrl(page, step.selector);
				break;
			case 'readdata':
				dataArray.push(
					await action(page, jobId, user, query.queryId, stepNo, step.action, step.selector, retryCount)
				);
				break;
			case 'click':
				await action(page, jobId, user, query.queryId, stepNo, step.action, step.selector, retryCount);
				break;
			case 'type':
				await action(page, jobId, user, query.queryId, stepNo, step.action, step.selector, retryCount, step.input);
				break;
			case 'nexturl':
				await nextUrl(page, url);
				break;
			default: 
				break;
		}
	}
	browser.close();
	console.log("<<<<<<<<<<<<<<<<<< Closed TASKRUNNER Browser <<<<<<<<<<<<<<<<<<<<<<<");

	return dataArray;
}

async function goToUrl(page, url){
	await page.goto(url);
}

async function nextUrl(page, url){
	console.log("next Url: ", url);
	await goToUrl(page, url);
}

async function action(page, jobId, userId, queryId, stepNo, actionType, selector, retryCount, input){
	try{
		if(retryCount < 3){
			console.log('executing action on: ', selector);
			const element = await page.waitForSelector(selector);
			console.log("found element");
			switch(actionType){
				case 'click':
					const [response] = await Promise.all([
						page.waitForNavigation({ timeout: 10000, waitUntil: ['domcontentloaded','load','networkidle0','networkidle2'] }),
						element.click()
						//page.click(element)
					]);
					break;
				case 'type':
					await element.type(input);
					break;
				case 'readdata':
					console.log("reading data... ", selector);
					let el = await page.$(selector);
					let text = await el.getProperty('innerText'); //'value' //innerHTML //textContent //innerText
					var innerHTML =  await text.jsonValue();
					//TODO get this element's innerHTML/textContent/innerText
					console.log('got innerHTML: ', innerHTML);
					//write to DB
					//DB.saveDataToDB(userId, jobId, queryId, Date.now(), stepNo, innerHTML);
					//write to DB at save time as schedule details. This makes updates less frequent
					if(innerHTML === ''){
						//retry after wait
						//console.log("retrying action");
						//action(page, jobId, userId, queryId, stepNo, actionType, selector, ++retryCount, input);
					}
					return {
						stepNo: stepNo, 
						value: innerHTML, 
						timestamp: Date.now()
					};
				case 'scroll':
					console.log("scrolling...");
					await page.evaluate(() => {
				    	window.scrollBy(0,window.innerHeight); //window.innerHeight/ 10
				    });
				    //wait for the page to load
					await page.waitFor(5000);
					break;
				default:
					break;
			}
		}
	}
	catch(err){
		++retryCount;
		if(retryCount < 3){
			console.log("Error executing action, retrying: ", err);
		}
		console.log("RetryCount exceeded, Moving on");
		//TODO log issues to ExceptionLog
		console.log("log issues to DB");
	}		
}