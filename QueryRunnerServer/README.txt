This server is a compartmentalized, abstracted modulated encapsulated query running server that will run and collect the data on a regular basis.

This is done purely through communication with the database and without any socket connection with the server.

This is so that if this server fails, the client side app is still running, and vice versa.
Also, for future scalability, if more queries need to be run simultaneously, or if queries are to be run inside Docker containers, it will be much easier to simply port all of this code without touching anything on the client server layer.

Database triggers will be put in place to listen to updates in queries and documents, so that puppeteer scripts can be generated.
Puppeteer scripts will be cached here to be performed and run on a regular basis.

MVP for this is to simply be able to read and decode queries from the DB and save data back to the DB.

