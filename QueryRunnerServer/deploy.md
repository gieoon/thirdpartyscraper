Deploy by using git subtree to only target "NodeJs server" subfolder directories

puppeteer browser has to be headless for this to work.

Have to add a config.json to make it work, and removed my own email address and password

Procfile with 
`worker: node index.js`

Swap remotes
`heroku git:remote -a "data2u-taskrunner-server"`

```
$ heroku buildpacks:clear
$ heroku buildpacks:add --index 1 https://github.com/jontewks/puppeteer-heroku-buildpack
$ heroku buildpacks:add --index 1 heroku/nodejs
```

`$ heroku scale worker=1`


Deployment URL on Heroku:
https://data2u-taskrunner-server.herokuapp.com/

add to local and push to local.

have to push from master branch
`git subtree push --prefix "QueryRunnerServer" heroku master`

To run logs for the site
`heroku logs --tail`

Deployment URL on AWS:
